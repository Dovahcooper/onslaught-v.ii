#pragma once
//Atiya  Nova
//This was taken from our lecture slides in Algorithms and Data Structures
#include <iostream>

template <class elemType>
class arrayListType
{
public:
	void arrayListType<elemType>::ArrayListType();
//	void arrayListType<elemType>::~ArrayListType();

	bool arrayListType<elemType>::isEmpty();
	bool arrayListType<elemType>::isFull();

	int arrayListType<elemType>::listSize();
	int arrayListType<elemType>::maxListSize();
	int arrayListType<elemType>::seqSearch(const elemType& item);

	void arrayListType<elemType>::insertAt(int location, const elemType & insertItem);
	void arrayListType<elemType>::insertEnd(const elemType& insertItem);
	void arrayListType<elemType>::retrieveAt(int location, elemType& retItem);
	void arrayListType<elemType>::removeAt(int location);
	void arrayListType<elemType>::remove(const elemType& removeItem);

protected:
	elemType list[100]; //this is the size of the array
	int length; //the length of the array
	int maxSize; //the maximum size of the array
};


