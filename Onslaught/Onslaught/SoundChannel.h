//Designed by Matthew Holt 100622906
#pragma once

#include "SoundEngine.h"
#include <cstring>

class SoundSystem
{
public:
	SoundSystem();
	~SoundSystem();

	//updates the playing status as well as resets the 3D attributes every frame
	//takes in deltaTime, *se->system, and a bool for playing 
	void update(float dt, FMOD::System &sys, bool _playing = false);

	//sets the position using a glm vector and translates it into an
	//FMOD vector for use in the attributes of the sound
	void setPosition(glm::vec3 &position);

	//gets the position
	glm::vec3 getPosition();

	//sets the velocity for a little bit of doppler
	//during runtime. Panning does not create a doppler
	//effect
	void setVelocity(glm::vec3 &vel);

	//gets the velocity
	glm::vec3 getVelocity();

	//sets the channel volume so as to be heard without
	//requiring an excessive computer volume
	void setVolume(float volume);

	//takes in an FMOD_MODE enum for rolloff
	//do this before starting the loop
	//use FMOD_3D_INVERSEROLLOFF for
	//best results
	void setRolloff(FMOD_MODE _rolloff);

	//takes in an FMOD_MODE enum
	//just send through FMOD_3D for now
	void setMode(FMOD_MODE _mode);

	void setFMODMode(FMOD_MODE c);

	//loads the sound from a user specified file
	//takes in a string and the *se->system
	bool createSound(std::string soundFile, FMOD::System &system);



	void setPlaying(bool temp);
	bool getPlaying();

	FMOD::Channel *channel;
	FMOD::Sound *sound;
protected:
	FMOD_RESULT result;

	bool playing = false;
	bool linear = false;

	FMOD_VECTOR channelPos;
	glm::vec3 channelPosition;
	FMOD_VECTOR channelVel;
	glm::vec3 channelVelocity;
private:

};