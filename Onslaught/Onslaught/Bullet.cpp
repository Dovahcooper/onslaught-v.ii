#include "Bullet.h"

Bullet::Bullet(float _dx, float _dy, glm::vec3 pos, Mesh* bulletMesh, ShaderProgram * phong, Material* mat, float newVelocity)
{
	//the basic is 20
	velocity = newVelocity;

	direction.x = _dx;
	direction.y = _dy;

	velocityVec = velocity * direction;

	GameObject::setPosition(pos);

	initializeObject(bulletMesh, phong);
	setMaterial(mat);

	setScale(10);
}

Bullet::~Bullet()
{

}

void Bullet::update(float dt)
{
	GameObject::update(dt);

	setPosition(dt);
}

void Bullet::setPosition(float &dt)
{
	GameObject::setPosition(getPosition() + (velocityVec * dt));
}
