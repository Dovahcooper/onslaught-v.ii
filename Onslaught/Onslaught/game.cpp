#include "game.h"
#include <GL/glut.h>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <iostream>

Game::Game()
{

}

Game::~Game()
{
	delete updateTimer;
}

//initialize game window setting the camera to a 45 deg FOV
void Game::initGame()
{
	updateTimer = new Timer();

	glEnable(GL_DEPTH_TEST);

	//This is all camera related movement
	cameraTranslate = glm::translate(cameraTranslate,
		glm::vec3(0.f, 7.f, -56.f)); //translating the camera; help's us center it and zoom in/out (eventually want to use arena coord to center camera)
	cameraRotate = glm::rotate(cameraRotate, glm::radians(-25.f), glm::vec3(0.1f, 0.0f, 0.0f)); //rotation; our top-down view control
	cameraTransform = cameraTranslate * cameraRotate; //the ultimate transformation, combining both rotation and translation
	cameraProj = glm::perspective(45.f,
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT,
		0.1f, 10000.f); //perspective; determines what the camera can and cannot see
	cameraOrtho = glm::ortho(0.f, (float)WINDOW_WIDTH, 0.f, (float)WINDOW_HEIGHT);
	identity = glm::mat4x4(glm::vec4(1.f, 0.f, 0.f, 0.f), glm::vec4(0.f, 1.f, 0.f, 0.f), glm::vec4(0.f, 0.f, 1.f, 0.f), glm::vec4(0.f, 0.f, 0.f, 1.f));
	lightView = glm::lookAt(glm::vec3(0.0f, 2.0f, 3.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));
	lightOrtho = glm::ortho(-100.0f, 100.0f, -100.0f, 100.0f, -12.f, 25.5f);
}

//draw function
void Game::draw()
{
	glClearColor(0, 0.0, 0.0, 1); //set the color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //buffer clears
}

void Game::update()
{
	updateTimer->tick();
	deltaTime = updateTimer->getElapsedTimeSeconds();
	//animations will occur here probably
	//keyboard func will probably do stuff here
	//maybe start animations.
	//maybe we should grab animationMath.h that
	//Michael Gharbaran gave us for lerping

}

bool Game::collisionDetection(GameObject *one, GameObject *two)
{
	float x = one->getPosition().x - two->getPosition().x;
	float y = one->getPosition().y - two->getPosition().y;
	float radiusOne = one->radius;
	float radiusTwo = two->radius;
	float totalRadius = radiusOne + radiusTwo;
	if ((x * x) + (y * y) > (totalRadius * totalRadius))
	{
		return true;
	}
	else
		return false;
}

void Game::keyboardUp(unsigned char &key, int &x, int &y)
{
	switch (key)
	{

	case 27:
		exit(1);
		break;
	default:
		break;
	}
}

void Game::keyboardDown(unsigned char &key, int &x, int &y)
{
	switch (key)
	{
	case ' ':
		//boing
		break;
	default:
		break;
	}
}

void Game::renderScore(int point1, int point2)
{
	glLoadIdentity(); //will still render even without this

					  //glLoadIdentity();
					  //glColor3f(1, 0, 0);

	std::stringstream strs1;
	strs1 << point1;
	std::string temp_str1 = strs1.str();
	char* convertedpoint1 = (char*)temp_str1.c_str();

	std::stringstream strs2;
	strs2 << point2;
	std::string temp_str2 = strs2.str();
	char* convertedpoint2 = (char*)temp_str2.c_str();

	drawStrokeFont(200, 910, 0, GLUT_STROKE_ROMAN, convertedpoint1);
	drawStrokeFont(1700, 910, 0, GLUT_STROKE_ROMAN, convertedpoint2);
}

void Game::renderGameInfo(int waveNumber)
{
	glLoadIdentity(); //will still render even without this

					  //glLoadIdentity();
					  //glColor3f(1, 0, 0);

	std::stringstream strs1;
	strs1 << waveNumber;
	std::string temp_str1 = strs1.str();
	char* convertedpoint1 = (char*)temp_str1.c_str();

	if (waveNumber == 6 || waveNumber == 12 || waveNumber >= 17)
	{
		drawStrokeFont(910, 1010, 0, GLUT_STROKE_ROMAN, "Boss");
	}
	else
	{
		drawStrokeFont(900, 1030, 0, GLUT_STROKE_ROMAN, "Wave");
		drawStrokeFont(1000, 1030, 0, GLUT_STROKE_ROMAN, convertedpoint1);
	}
}

void Game::renderText(float x, float y, float z, std::string text)
{
	glLoadIdentity(); //will still render even without this

					  //glLoadIdentity();
					  //glColor3f(1, 0, 0);

	std::stringstream strs1;
	strs1 << text;
	std::string temp_str1 = strs1.str();
	char* convertedpoint1 = (char*)temp_str1.c_str();

	drawStrokeFont(x, y, z, GLUT_STROKE_ROMAN, convertedpoint1);

}

void Game::drawStrokeFont(float x, float y, float z, void * font, char * string)
{
	glUseProgram(0);
	glColor3f(255, 255, 255);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	glOrtho(0.f, (float)WINDOW_WIDTH, 0.0f, (float)WINDOW_HEIGHT, -100.f, 100.f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	char *c;
	glPushMatrix();
	glTranslatef(x, y, z);
	glScalef(0.3, 0.3, 0);

	for (c = string; *c != '\0'; c++) {
		glutStrokeCharacter(font, *c);
	}

	glPopMatrix();
}
