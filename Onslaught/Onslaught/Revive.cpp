#include "Revive.h"

Revive::Revive(Mesh * temp)
	:Enemy(temp)
{
	velocity = glm::vec3(0, 0, 0);
	maxForce = 3;
	max_velocity = 20.0f;
	max_seek = 10.0f;
	mass = 1;
	seperationVector;
	areaRadius = 1.5;
	target = glm::vec3(0, 0, 0);
	directionFaced = glm::vec3(0, 0, 0);
	timer = 0;
	health = 50;
	enemyRadius = 1.5;
	desiredSeperation = enemyRadius;

	_enemyType = _Revive;
}

Revive::~Revive()
{
	Enemy::~Enemy();
}

glm::vec3 Revive::computeSeperation(std::vector<Enemy*> enemy_units, int index)
{
	int total_grunts = enemy_units.size(); //we need to iterate through all the grunts
	int neighbourCount = 0; //we use this to 

	for (int i = index; i < total_grunts; i++)
	{
		if (i != index && gridIndex == enemy_units[i]->gridIndex) //making sure we're not seperating from itself
		{
			float lineBetween = sqrt(pow(getPosition().y - enemy_units[i]->getPosition().y, 2) + pow(getPosition().x - enemy_units[i]->getPosition().x, 2)); //THIS IS DISTANCE BETWEEN

			if (lineBetween < (desiredSeperation + enemy_units[i]->enemyRadius)) //If another object falls within this object's range
			{
				//printf("%f", desiredSeperation);
				glm::vec3 difference = (glm::vec3(enemy_units[i]->getPosition().x - getPosition().x, enemy_units[i]->getPosition().y - getPosition().y, 0)); //we get the difference
				seperationVector += difference; //add it to the force
				neighbourCount++; //increment the number of 'neighbours'
			}
		}
	}

	if (neighbourCount == 0)
	{
		return glm::vec3(0); //if there are no neighbours, nothing happens
	}

	seperationVector /= neighbourCount; //why are we doing this
	seperationVector *= -1; //this is the offset
							//seperationVector = glm::normalize(seperationVector); //this is what keeps them apart //do we need this???

	steering = seperationVector - velocity; // to update it...?

	if (glm::length(steering) > max_velocity) //this is clamping the value
		steering = glm::normalize(steering) * max_velocity;

	steering = steering / mass; //the mass affects how much it moves

	return steering; //we return the force
}

glm::vec3 Revive::enemyArrive()
{
	//float distance = glm::length(target); //the distance is for arrive
	float distance = glm::distance(getPosition(), target);
	target = glm::normalize(target); //normalizing leads to the direction

	if (distance < areaRadius) //if it falls within the area
	{
		float t = distance / areaRadius; //this is the t value--the interval
		float arriveInfluence = glm::mix(0.f, max_seek, t); //it lerps to that point

		target *= arriveInfluence; //this influences how strongly it slows down
	}
	else
		target *= max_seek;

	steering = target - velocity;

	//the mass affects how it moves
	steering = steering / mass;

	return steering;
}

void Revive::enemyAttack()
{
	float distance = glm::length(target);
	target = glm::normalize(target);

	if (distance < 12 && timer > 1)
	{
		attacking = true;
		firing = true;
		createBullets(target.x, target.y);
		timer = 0;
	}
}

bool Revive::collision(Player * thePlayer, Player *deadPlayer, int &numAlive, int &index, int max)
{
	Enemy::collision(thePlayer);

	if (Alive == true)
	{
		//needs to revive
		for (int i = 0; i < thePlayer->playerBullets.size(); i++)
		{
			float distance = glm::distance(getPosition(), thePlayer->playerBullets[i]->getPosition());

			if (distance < 1.3f)
			{
				thePlayer->deleteBullets(i);
				health -= thePlayer->bulletDamage;
				thePlayer->score += (1 * thePlayer->scoreMultiplayer);
				if (Alive == true && health <= 0)
				{
					Alive = false;
					Spawn = false;
					deadPlayer->setHealth(deadPlayer->maxHealth);
					deadPlayer->setAlive(true);
					thePlayer->hit = false;
					deadPlayer->invincibilityFrames = 2;
					index = max;
					return true;
				}
				else
					return false;
			}

			dTimer = 1;
			setMaterial(damageTexture);
		}
	}
}

void Revive::initialize(Mesh * newMesh, Material * newMaterial, glm::vec3 newPosition, bool champion)
{
	//	Enemy::initialize(newMesh, newMaterial, champion);
	setPosition(newPosition);

	Spawn = true;
	Alive = false;

	Object = newMesh;
	setMaterial(newMaterial);

	velocity = glm::vec3(0, 0, 0);
	maxForce = 2;
	max_velocity = 30.0f;
	max_seek = 10.0f;
	mass = 1;
	seperationVector;
	areaRadius = 9;
	target = glm::vec3(0, 0, 0);
	health = 50;
}

void Revive::update(float deltaTime, int & numAlive)
{
	Enemy::update(deltaTime, numAlive, NULL);
}

void Revive::draw(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	Enemy::draw(cameraTransform, cameraProjection, pointLights, directionalLight);
}
