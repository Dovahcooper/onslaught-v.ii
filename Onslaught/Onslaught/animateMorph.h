#pragma once
#include "Mesh.h"
#include <vector>
#include <memory>
#include <fstream>
#include <iostream>
#include "glm/glm.hpp"


class animateMorph
{
public:
	animateMorph();
	~animateMorph();
	void loadMorphTarget();
	std::vector<Mesh*> morphMesh;
	float interpolate = 0.f;
	float currentFrame = 0.f;
	float nextFrame = 1.0f;
};
