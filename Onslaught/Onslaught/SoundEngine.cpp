//Brent Cowan Jan. 15, 2018
#include "SoundEngine.h"

void FmodErrorCheck(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		//printf("Fmod Error: %s\n", FMOD_ErrorString(result));
	}
}

SoundEngine::SoundEngine()
{
	init = false;
}

SoundEngine::~SoundEngine()
{
	CleanUp();
}

void SoundEngine::CleanUp()
{
	result = system->close();
	FmodErrorCheck(result);
	result = system->release();
	FmodErrorCheck(result);

	init = false;
}

void SoundEngine::update()
{
	result = system->set3DListenerAttributes(0, &listener.pos, &listener.vel, &listener.forward, &listener.up);

	FmodErrorCheck(result);

	//this needs to be called every iteration
	result = system->update(); // <- we gotta put this shiz in the soundengine class!
	FmodErrorCheck(result);
}

bool SoundEngine::Init()
{
	if (!init)
	{
		init = true;

		result = FMOD::System_Create(&system);
		FmodErrorCheck(result);
		if (result != FMOD_OK) { init = false; }

		result = system->getVersion(&version);
		if (result != FMOD_OK) { init = false; }
		FmodErrorCheck(result);

		result = system->init(100, FMOD_INIT_NORMAL, extradriverdata);
		FmodErrorCheck(result);
		if (result != FMOD_OK) { init = false; }

		/*
		Set the distance units. (meters/feet etc).
		*/
		result = system->set3DSettings(1.0f, 1.0f, 1.0f);
		FmodErrorCheck(result);
		if (result != FMOD_OK) { init = false; }
	}

	return init;
}

void SoundEngine::setListenerPosition(glm::vec3 listener_pos)
{
	listener.pos = { listener_pos.x, listener_pos.y, -listener_pos.z };
}

void SoundEngine::setListenerForwards(glm::vec3 listener_forward)
{
	listener.forward = { listener_forward.x, listener_forward.y, -listener_forward.z };
}

void SoundEngine::setListenerUp(glm::vec3 listener_up)
{
	listener.up = { listener_up.x, listener_up.y, -listener_up.z };
}

void SoundEngine::setListenerVelocity(glm::vec3 listener_vel)
{
	listener.vel = { listener_vel.x, listener_vel.y, -listener_vel.z };
}