#pragma once
#include "Enemy.h"

class Avengers : public Enemy
{
public:
	Avengers(Mesh *temp);
	~Avengers();

	glm::vec3 computeSeperation(std::vector <Enemy*> theEnemies, int index);

	glm::vec3 enemyArrive();

	void enemyAttack();

	//getters and setters for velocity, acceleration, and max force

	void collision(Player *thePlayer);

	void initialize(Mesh* newMesh, Material* newMaterial, int spawnPoint, bool champion = false);
	void update(float deltaTime, int &numAlive);
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

	void explode(std::vector<Enemy*> theEnemies, int &numAlive);

	bool explSound;
	bool explosion;

private:
	float secondTimer;
};
