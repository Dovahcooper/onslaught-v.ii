#include "Sprinkler.h"

Sprinkler::Sprinkler(Mesh * temp)
	:Enemy(temp)
{
	velocity = glm::vec3(0, 0, 0);
	maxForce = 3;
	max_velocity = 20.0f;
	max_seek = 5.0f;
	mass = 1;
	seperationVector;
	target = glm::vec3(0, 0, 0);
	directionFaced = glm::vec3(0, 0, 0);
	timer = 0;
	health = 50;
	_enemyType = _Sprinklers;
	reviveTimer = 0;
	areaRadius = 1.5;
	enemyRadius = 1.5;
	desiredSeperation = enemyRadius; //how much we want to keep it apart by
}

Sprinkler::~Sprinkler()
{
}

glm::vec3 Sprinkler::computeSeperation(std::vector<Enemy*> theEnemies, int index)
{
	int total_enemies = theEnemies.size(); //we need to iterate through all the grunts
	int neighbourCount = 0; //we use this to 

	for (int i = index; i < total_enemies; i++)
	{
		if (i != index && gridIndex == theEnemies[i]->gridIndex) //making sure we're not seperating from itself
		{
			float lineBetween = sqrt(pow(getPosition().y - theEnemies[i]->getPosition().y, 2) + pow(getPosition().x - theEnemies[i]->getPosition().x, 2)); //THIS IS DISTANCE BETWEEN

			if (lineBetween < (desiredSeperation + theEnemies[i]->enemyRadius)) //If another object falls within this object's range
			{
				//printf("%f", desiredSeperation);
				glm::vec3 difference = (glm::vec3(theEnemies[i]->getPosition().x - getPosition().x, theEnemies[i]->getPosition().y - getPosition().y, 0)); //we get the difference
				seperationVector += difference; //add it to the force
				neighbourCount++; //increment the number of 'neighbours'
			}
		}
	}

	if (neighbourCount == 0)
	{
		return glm::vec3(0); //if there are no neighbours, nothing happens
	}

	seperationVector /= neighbourCount; //why are we doing this
	seperationVector *= -1; //this is the offset
							//seperationVector = glm::normalize(seperationVector); //this is what keeps them apart //do we need this???

	steering = seperationVector - velocity; // to update it...?

	if (glm::length(steering) > max_velocity) //this is clamping the value
		steering = glm::normalize(steering) * max_velocity;

	steering = steering / mass; //the mass affects how much it moves

	return steering; //we return the force
}

glm::vec3 Sprinkler::enemyArrive()
{
	//float distance = glm::length(target); //the distance is for arrive
	float distance = glm::distance(getPosition(), target);
	target = glm::normalize(target); //normalizing leads to the direction

	if (distance < areaRadius) //if it falls within the area
	{
		float t = distance / areaRadius; //this is the t value--the interval
		float arriveInfluence = glm::mix(0.f, max_seek, t); //it lerps to that point

		target *= arriveInfluence; //this influences how strongly it slows down
	}
	else
		target *= max_seek;

	steering = target - velocity;

	//the mass affects how it moves
	steering = steering / mass;

	return steering;
}

void Sprinkler::enemyAttack()
{
	float distance = glm::length(target);
	target = glm::normalize(target);

	if (timer > 1)
	{
		attacking = true;
		firing = true;
		createBullets(target.x, target.y);
		createBullets((target.x += 0.15), (target.y += 0.15));
		createBullets((target.x += 0.3), (target.y += 0.3));
		createBullets((target.x -= 0.15), (target.y -= 0.15));
		createBullets((target.x -= 0.3), (target.y -= 0.3));

		timer = 0;
	}
}

void Sprinkler::collision(Player * thePlayer)
{
	Enemy::collision(thePlayer);

	if (Alive == true)
	{
		for (int i = 0; i < thePlayer->playerBullets.size(); i++)
		{
			float distance = glm::distance(getPosition(), thePlayer->playerBullets[i]->getPosition());

			if (distance < 1.3f)
			{
				//Alive = false;
				thePlayer->deleteBullets(i);
				setMaterial(damageTexture);
				dTimer = 0.4;
				health -= thePlayer->bulletDamage;

				if (health <= 0 && tutorial == false)
					thePlayer->score += (15 * thePlayer->scoreMultiplayer);
			}
		}
	}
}

void Sprinkler::initialize(Mesh * newMesh, Material * newMaterial, int spawnPoint, bool champion)
{
	Enemy::initialize(newMesh, newMaterial, spawnPoint, champion);

	velocity = glm::vec3(0, 0, 0);
	maxForce = 2;
	max_velocity = 30.0f;
	max_seek = 10.0f;
	mass = 1;
	seperationVector;
	areaRadius = 9;
	target = glm::vec3(0, 0, 0);
	health = 30;

	setMaterial(newMaterial);
}

void Sprinkler::update(float deltaTime, int & numAlive, PowerUp *powerUp)
{
	Enemy::update(deltaTime, numAlive, powerUp);
}

void Sprinkler::draw(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	Enemy::draw(cameraTransform, cameraProjection, pointLights, directionalLight);
}

