//Tangent reference: http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/#tangent-and-bitangent
//Shadow mapping reference: https://learnopengl.com/Advanced-Lighting/Shadows/Shadow-Mapping

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL\freeglut.h>
#include <map>
#include "stdio.h"
#include <ctime>
#include "game.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\string_cast.hpp>
#include "ArrayList.h"
#include "PowerUp.h"
#include "SoundChannel.h"
#include "Boss.h"
#include "Revive.h"
#include "FrameBufferObject.h"
#include "ParticleClass.h"
#include "fileIO.h"
#include "Lut.h"

PowerUp* thePowerUp;

//Michael Gharbaran's spatial subdivision
std::vector<glm::vec3> theGrid;

//other stuff
struct Triggers
{
	float left;
	float right;
};

enum Buttons
{
	A, B, X, Y,
	DPadUp, DPadDown, DPadLeft, DPadRight,
	Start, Select,
	LB, RB,
	LS, RS,
	LT, RT,
	UnPressed
};

std::shared_ptr<Mesh> createQuadMesh()
{
	std::shared_ptr<Mesh> quadMesh = std::make_shared<Mesh>();

	//tri 1
	quadMesh->vertexData.push_back(glm::vec3(1.f, 1.f, 0.f));
	quadMesh->textureData.push_back(glm::vec2(1.f, 1.f));

	quadMesh->vertexData.push_back(glm::vec3(-1.f, 1.f, 0.f));
	quadMesh->textureData.push_back(glm::vec2(0.f, 1.f));

	quadMesh->vertexData.push_back(glm::vec3(-1.f, -1.f, 0.f));
	quadMesh->textureData.push_back(glm::vec2(0.f, 0.f));

	//tri 2
	quadMesh->vertexData.push_back(glm::vec3(1.f, 1.f, 0.f));
	quadMesh->textureData.push_back(glm::vec2(1.f, 1.f));

	quadMesh->vertexData.push_back(glm::vec3(-1.f, -1.f, 0.f));
	quadMesh->textureData.push_back(glm::vec2(0.f, 0.f));

	quadMesh->vertexData.push_back(glm::vec3(1.f, -1.f, 0.f));
	quadMesh->textureData.push_back(glm::vec2(1.f, 0.f));

	quadMesh->createVBO();

	return quadMesh;
}

//controllers
Input::XBoxInput controller;
Triggers trig[4];
Buttons pressed[4];
Input::Stick lStick, rStick;

int test = 0, charTemp = 0;

Lut warmLut, coolLut;

//Sound System
SoundEngine *se;
bool blnGameOver = false, blnZero = true, blnOne = false, blnTwo = false, blnThree = false;
std::vector<SoundSystem*> enemyShooting, enemyExplosions, pFiring, pFiring2, announcerDialogue, menuSFX, backgroundMusic, powerUps;
std::string soundPath = "sounds/";

//player stuff
Player *playerOne, *playerTwo;
GameObject *player1HP, *player2HP;

GameObject *onslaughtLogo, *start, *quit, *tutorial, *player_cust1, *player_cust2, *player_body1, *player_body2, *gameOver, *pauseObject, *overlayObject, *leaderboard, *winScreen, *SMP1, *SMP2, *ready1, *ready2;

GameObject *player1StandardHP, *player1FastHP, *player1HeavyHP, *player1HPEmpty, *bossStandardHP;
GameObject *player2StandardHP, *player2FastHP, *player2HeavyHP, *player2HPEmpty;

//camera, overall game, arenas
Game *game;
GameObject *arena1;

//variables for player one and two //for character creation
int indexOne = 0, indexTwo = 1, tempIndex1 = 0, tempIndex2 = 0, tempTurretI1 = 0, tempTurretI2 = 0;

int powerupTemp = 0;

float standardHeights[11] = { 0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150 };
float fastHeights[8] = { 0, 24, 45, 66, 87, 108, 129, 150 };
float heavyHeights[14] = { 0, 18, 29,40,51,62,73,84,95,106,117,128,139,150 };
int sHeightI = 10, fHeightI = 7, hHeightI = 13, sHeightI2 = 10, fHeightI2 = 7, hHeightI2 = 13; //all the healthbars start at the top

																							   //meshes
Mesh * enemy, *spawnMesh, *turret, *turretPieceMesh, *enemyBullet, *coin, *repair, *graveMesh;
ShaderProgram * phong, *passThrough, *particleShader, *phongNormal;

std::vector<Mesh*> healthBars, healthBarsFast, healthBarsHeavy;
std::vector <Mesh*> bodies;
std::vector <Mesh*> turrets;
std::vector<Material*> charTurretTexture;
std::vector<Material*> charBodiesTexture;
std::vector<Material*> PlayerTextures1, PlayerTurretTex1;
std::vector<Material*> PlayerTextures2, PlayerTurretTex2;

GameObject *tempPlayer1, *tempPlayer2, *tempTurret1, *tempTurret2;

//particles
std::vector<ParticleClass*> particles;
std::vector <ParticleClass*> particlesVec, particlesVec2, particlesVec3, particlesVec4, particlesVec5, particlesVec6;

int angle = 0;
float expTime = 0;
bool explodePls = false;
glm::vec3 sep = glm::vec3(0), arrival = glm::vec3(0), newPosition = glm::vec3(0);

static bool gamePause = false, win = true; //This determines what draws in the endgame!

bool tutEnd = true;
bool bounceBack, bounceBack2;
float bounceTime = 0, bounceTime2 = 0;
std::vector <Enemy*> theEnemies;

std::map<std::string, std::shared_ptr<Mesh>> quads;
std::map<std::string, std::shared_ptr<FrameBufferObject>> frameBuf;
std::map<std::string, std::shared_ptr<ShaderProgram>> materials;

//lights for shaders
std::vector<Light> pointLights;
Light directionalLight;

int selection = 0; //UI selection for start/quit
int progression = 0;

float healthDecrease = 0, healthDecrease2 = 0; //timer for health UIs

											   //for enemy waves
float spawnTime;
float waveTimer;
int waveLimit = 3; //used to check number of enemies on screen
int waveNumber = 0; //if it's below the waveLimit, it spawns enemies
int numActive = 0; //keeps track of when the enemies are being spawned
int waveProgression = 0; //Use to keep track of what enemies to spawn each waveeee
int totalWave = 0; //to display
int avengerIndex = 15, sprinklerIndex = 19, bossIndex = 20, reviveIndex = 21;

glm::mat4 temp = glm::mat4(1.f);

glm::vec3 tutorialPosition = glm::vec3(0);
int tutorialIndex = 0, particleIndex = 0;

float yHealth = 150.0f; //y value for health UI
glm::vec3 player1Pos = glm::vec3(0);
glm::vec3 player2Pos = glm::vec3(0);

//vectors for enemy kinematics
std::vector<GameObject*> enemyTurret, turretPiece;

bool readyP1 = false, readyP2 = false; //this is for displaying 'ready'

Mesh *startBar, *bossBody;
Material enemyTexture, bulletTexture, turretTip, damageTexture, blue, green, spawnMat, repairMat, coinMat, overlayMat,
bossMat, *bloomMat, *fboMat, explTex, purpleTexture, redTexture, ready, credits, pauseMat, readyMat1, readyMat2, tutorialMat;


//Texture *particleTexture, *sPart1, *sPart2, *sPart3, *sPart4, *sPart5;
std::vector<Texture> particleTextures;

//frame delay
const int FRAME_DELAY = 1000 / FRAMES_PER_SEC;

//different states of the game
enum GameStates
{
	splashScreen = 0,
	menu = 1,
	charCreation = 2,
	level1 = 3,
	level2 = 4,
	endGame = 5,
	leaderBoard = 6
};

enum WaveStates
{
	null = 0,
	regular = 1, //just grunts
	reg_boss = 2,
	avenger = 3, //starts spawning regular explosive enemies
	avg_boss = 4,
	sprinkler = 5, //the burst shots
	final_boss = 6 //the boss
};

//default state

GameStates gameStates = splashScreen;
WaveStates waveStates = regular;


CustomizationType c_bodies, c_turrets;

void drawUI();
void drawScene(bool playerDraw);
void initFBO();
void loadShaders();
void drawMenu(bool draw);

FileIO filehandler;
int playerHighscore;
std::string playerHighscoreName;
int dataSize = filehandler.nameData.size() - 1; //score data is stored lowest at n=0 to highest at n-1 
int spawnPoint = 0;

std::vector <Material*> overlayTexture;
std::shared_ptr<Texture> shadowInt;

glm::vec3 particlePositions[8], regularPositions[8], regBossPositions[8];

void loadQuads()
{
	quads["standard"] = createQuadMesh();
	initFBO();
}

void initFBO()
{
	frameBuf["standard"] = std::make_shared<FrameBufferObject>();
	frameBuf["standard"]->createFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT, 1, true);
	frameBuf["bright"] = std::make_shared<FrameBufferObject>();
	frameBuf["bright"]->createFrameBuffer(WINDOW_WIDTH / 4, WINDOW_HEIGHT / 4, 1, true);
	frameBuf["bloom1"] = std::make_shared<FrameBufferObject>();
	frameBuf["bloom1"]->createFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT, 1, false);
	frameBuf["bloom2"] = std::make_shared<FrameBufferObject>();
	frameBuf["bloom2"]->createFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT, 1, false);
	frameBuf["shadowDepth"] = std::make_shared<FrameBufferObject>();
	frameBuf["shadowDepth"]->createFrameBuffer(1024, 1024, 0, true);
	frameBuf["lut"] = std::make_shared<FrameBufferObject>();
	frameBuf["lut"]->createFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT, 1, true);
}

//just add this function before the bloom funcion
/*void lut()
{
glm::vec4 clearColour = glm::vec4(0.f);
frameBuf["lut"]->bindFrameBufferForDrawing();
frameBuf["lut"]->clearFrameBuffer(clearColour);

frameBuf["standard"]->bindTextureForSampling(0, GL_TEXTURE0);
frameBuf["lut"]->bindTextureForSampling(0, GL_TEXTURE1);

materials["lut"]->bind();
materials["lut"]->sendUniform("constantAttenuation", 1.0f);
materials["lut"]->sendUniform("linearAttenuation", 0.1f);
materials["lut"]->sendUniform("quadraticAttenuation", 0.01f);

quads["standard"]->draw();

//materials["lut"]->unbind();

frameBuf["standard"]->unbindTexture(GL_TEXTURE0);
frameBuf["lut"]->unbindTexture(GL_TEXTURE1);
materials["lut"]->unbind();
frameBuf["lut"]->unbindFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
}*/

void bloom(int loop)
{
	//lut();
	glm::mat4 temp = glm::mat4(1.f);
	glm::vec4 clearColour = glm::vec4(0.f);

	if (gameStates == menu)
	{
		frameBuf["bright"]->bindFrameBufferForDrawing();
		frameBuf["bright"]->clearFrameBuffer(clearColour);

		if (gameStates == menu)
		{
			drawScene(false);
			drawUI();
			drawMenu(true);
		}
		else
		{
			drawScene(false);
			drawUI();
		}

		frameBuf["bright"]->unbindFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);


		frameBuf["bright"]->bindTextureForSampling(0, GL_TEXTURE0);
		frameBuf["bloom1"]->bindFrameBufferForDrawing();
		frameBuf["bloom1"]->clearFrameBuffer(clearColour);

		materials["brightPass"]->bind();
		materials["brightPass"]->sendUniformMat4("u_mv", glm::value_ptr(game->cameraTransform), false);
		materials["brightPass"]->sendUniformMat4("u_mvp", glm::value_ptr(temp), false);
		materials["brightPass"]->sendUniform("u_threshold", 0.4f);

		quads["standard"]->draw();

		materials["brightPass"]->unbind();
		frameBuf["bloom1"]->unbindFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
		frameBuf["bright"]->unbindTexture(GL_TEXTURE0);
	}
	else
	{
		frameBuf["bloom1"]->bindFrameBufferForDrawing();
		frameBuf["bloom1"]->clearFrameBuffer(clearColour);

		frameBuf["standard"]->bindTextureForSampling(0, GL_TEXTURE0);
		materials["brightPass"]->bind();
		materials["brightPass"]->sendUniformMat4("u_mv", glm::value_ptr(game->cameraTransform), false);
		materials["brightPass"]->sendUniformMat4("u_mvp", glm::value_ptr(temp), false);
		materials["brightPass"]->sendUniform("u_threshold", 0.45f);

		quads["standard"]->draw();

		materials["brightPass"]->unbind();
		frameBuf["standard"]->unbindTexture(GL_TEXTURE0);
		frameBuf["bloom1"]->unbindFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
	}

	for (int i = 0; i < loop; i++)
	{
		frameBuf["bloom2"]->bindFrameBufferForDrawing();
		frameBuf["bloom2"]->clearFrameBuffer(clearColour);

		frameBuf["bloom1"]->bindTextureForSampling(0, GL_TEXTURE0);
		materials["blurVertical"]->bind();
		materials["blurVertical"]->sendUniformMat4("u_mv", glm::value_ptr(game->cameraTransform), false);
		materials["blurVertical"]->sendUniformMat4("u_mvp", glm::value_ptr(temp), false);
		materials["blurVertical"]->sendUniform("u_verticalTexel", 1.f / WINDOW_HEIGHT);

		quads["standard"]->draw();

		frameBuf["bloom1"]->unbindTexture(GL_TEXTURE0);
		materials["blurVertical"]->unbind();
		frameBuf["bloom2"]->unbindFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);

		frameBuf["bloom1"]->bindFrameBufferForDrawing();
		frameBuf["bloom1"]->clearFrameBuffer(clearColour);

		frameBuf["bloom2"]->bindTextureForSampling(0, GL_TEXTURE0);
		materials["blurHorizontal"]->bind();
		materials["blurHorizontal"]->sendUniformMat4("u_mv", glm::value_ptr(game->cameraTransform), false);
		materials["blurHorizontal"]->sendUniformMat4("u_mvp", glm::value_ptr(temp), false);
		materials["blurHorizontal"]->sendUniform("u_horizontalTexel", 1.f / WINDOW_WIDTH);

		quads["standard"]->draw();

		frameBuf["bloom2"]->unbindTexture(GL_TEXTURE0);
		materials["blurHorizontal"]->unbind();
		frameBuf["bloom1"]->unbindFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
	}
}

void loadGame()
{
	theGrid.push_back(glm::vec3(-20, 20, 0));
	theGrid.push_back(glm::vec3(-10, 20, 0));
	theGrid.push_back(glm::vec3(0, 20, 0));
	theGrid.push_back(glm::vec3(10, 20, 0));
	theGrid.push_back(glm::vec3(20, 20, 0));

	theGrid.push_back(glm::vec3(-20, 10, 0));
	theGrid.push_back(glm::vec3(-10, 10, 0));
	theGrid.push_back(glm::vec3(0, 10, 0));
	theGrid.push_back(glm::vec3(10, 10, 0));
	theGrid.push_back(glm::vec3(20, 10, 0));

	theGrid.push_back(glm::vec3(-20, 0, 0));
	theGrid.push_back(glm::vec3(-10, 0, 0));
	theGrid.push_back(glm::vec3(0, 0, 0));
	theGrid.push_back(glm::vec3(10, 0, 0));
	theGrid.push_back(glm::vec3(20, 0, 0));

	theGrid.push_back(glm::vec3(-20, -10, 0));
	theGrid.push_back(glm::vec3(-10, -10, 0));
	theGrid.push_back(glm::vec3(0, -10, 0));
	theGrid.push_back(glm::vec3(10, -10, 0));
	theGrid.push_back(glm::vec3(20, -10, 0));

	theGrid.push_back(glm::vec3(-20, -20, 0));
	theGrid.push_back(glm::vec3(-10, -20, 0));
	theGrid.push_back(glm::vec3(0, -20, 0));
	theGrid.push_back(glm::vec3(10, -20, 0));
	theGrid.push_back(glm::vec3(20, -20, 0));

	Material* matSpecular = new Material();
	matSpecular->loadTexture(TextureType::Specular, "Textures/monkeySpecular.png");

	tutorialMat.loadTexture(TextureType::Diffuse, "Textures/tutorial.png");
	tutorialMat.specular = matSpecular->specular;

	readyMat1.loadTexture(TextureType::Diffuse, "Textures/shipselection1.png");
	readyMat1.specular = matSpecular->specular;

	readyMat2.loadTexture(TextureType::Diffuse, "Textures/shipselection2.png");
	readyMat2.specular = matSpecular->specular;

	explTex.loadTexture(TextureType::Diffuse, "Textures/explosiveShip.png");
	explTex.specular = matSpecular->specular;

	credits.loadTexture(TextureType::Diffuse, "Textures/credits.png");
	credits.specular = matSpecular->specular;

	purpleTexture.loadTexture(TextureType::Diffuse, "Textures/purple.png");
	purpleTexture.specular = matSpecular->specular;

	redTexture.loadTexture(TextureType::Diffuse, "Textures/red.png");
	redTexture.specular = matSpecular->specular;

	spawnMat.loadTexture(TextureType::Diffuse, "Textures/spawn.png");
	spawnMat.loadTexture(TextureType::Specular, "Textures/white.png");

	enemyTexture.loadTexture(TextureType::Diffuse, "Textures/EnemyShipUVTexture.png");
	enemyTexture.specular = matSpecular->specular;

	bulletTexture.loadTexture(TextureType::Diffuse, "Textures/BulletUVMap.png");
	bulletTexture.specular = matSpecular->specular;

	turretTip.loadTexture(TextureType::Diffuse, "Textures/white.png");
	turretTip.specular = matSpecular->specular;

	blue.loadTexture(TextureType::Diffuse, "Textures/blue.png");
	blue.specular = matSpecular->specular;

	green.loadTexture(TextureType::Diffuse, "Textures/green.png");
	green.specular = matSpecular->specular;

	overlayMat.loadTexture(TextureType::Diffuse, "Textures/screenOverlay1.png");
	overlayMat.specular = matSpecular->specular;

	ready.loadTexture(TextureType::Diffuse, "Textures/ready.png");
	ready.specular = matSpecular->specular;

	//in load meshes()
	overlayTexture.push_back(new Material);
	overlayTexture[0]->loadTexture(TextureType::Diffuse, "Textures/screenOverlay1.png");
	overlayTexture[0]->specular = matSpecular->specular;

	overlayTexture.push_back(new Material);
	overlayTexture[1]->loadTexture(TextureType::Diffuse, "Textures/revp1Overlay.png");
	overlayTexture[1]->specular = matSpecular->specular;

	overlayTexture.push_back(new Material);
	overlayTexture[2]->loadTexture(TextureType::Diffuse, "Textures/revp2Overlay.png");
	overlayTexture[2]->specular = matSpecular->specular;

	//for powerup
	//p1, p2, both, p1, p2
	overlayTexture.push_back(new Material);
	overlayTexture[3]->loadTexture(TextureType::Diffuse, "Textures/overlay mult p1 UV.png");
	overlayTexture[3]->specular = matSpecular->specular;

	overlayTexture.push_back(new Material);
	overlayTexture[4]->loadTexture(TextureType::Diffuse, "Textures/overlay mult p2 UV.png");
	overlayTexture[4]->specular = matSpecular->specular;

	overlayTexture.push_back(new Material);
	overlayTexture[5]->loadTexture(TextureType::Diffuse, "Textures/overlay mult both UV.png");
	overlayTexture[5]->specular = matSpecular->specular;

	overlayTexture.push_back(new Material); //for when player 1 is alive
	overlayTexture[6]->loadTexture(TextureType::Diffuse, "Textures/overlay mult revive p1 UV.png");
	overlayTexture[6]->specular = matSpecular->specular;

	overlayTexture.push_back(new Material);
	overlayTexture[7]->loadTexture(TextureType::Diffuse, "Textures/overlay mult revive p2 UV.png");
	overlayTexture[7]->specular = matSpecular->specular;

	Mesh *overlayObj = new Mesh();
	overlayObj->loadFromFile("meshes/screenOverlay.obj");
	overlayObject = new GameObject;
	overlayObject->initializeObject(overlayObj, passThrough, true);
	overlayObject->setMaterial(overlayTexture[0]);
	overlayObject->setPosition(glm::vec3((WINDOW_WIDTH / 2), (WINDOW_HEIGHT / 2), 1.0f));
	overlayObject->setSpecificScale(86, 86, 0.01f);

	leaderboard = new GameObject;
	leaderboard->initializeObject(overlayObj, passThrough, true);
	leaderboard->setMaterial("Textures/leaderboardUV.png", "Textures/monkeySpecular.png");
	leaderboard->setPosition(glm::vec3((WINDOW_WIDTH / 2), (WINDOW_HEIGHT / 2) + 15, 1.0f));
	leaderboard->setSpecificScale(86, 86, 0.01f);

	tutorial = new GameObject;
	tutorial->initializeObject(overlayObj, passThrough, true);
	tutorial->setMaterial("Textures/tutorial.png", "monkeySpecular.png");
	tutorial->setPosition(glm::vec3((WINDOW_WIDTH / 2), (WINDOW_HEIGHT / 2), 1.0f));
	tutorial->setSpecificScale(86, 86, 0.01f);

	gameOver = new GameObject;
	gameOver->initializeObject(overlayObj, passThrough, true);
	gameOver->setMaterial("Textures/game_over.png", "Textures/monkeySpecular.png");
	gameOver->setPosition(glm::vec3((WINDOW_WIDTH / 2), (WINDOW_HEIGHT / 2), 1.0f));
	gameOver->setSpecificScale(86, 86, 0.01f);

	ready1 = new GameObject;
	ready1->initializeObject(startBar, passThrough, true);
	ready1->setMaterial(&ready);
	ready1->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) - 203.f, (WINDOW_HEIGHT * 0.5f) + 210, 0.f));
	ready1->setSpecificScale(70, 70, 0.01);
	ready1->setRotation(glm::vec3(0, 180, 90));

	ready2 = new GameObject;
	ready2->initializeObject(startBar, passThrough, true);
	ready2->setMaterial(&ready);
	ready2->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) + 203.f, (WINDOW_HEIGHT * 0.5f) + 210, 0.f));
	ready2->setSpecificScale(70, 70, 0.01);
	ready2->setRotation(glm::vec3(0, 180, 90));

	pauseMat.loadTexture(TextureType::Diffuse, "Textures/pause.png");
	pauseMat.specular = matSpecular->specular;

	pauseObject = new GameObject;
	pauseObject->initializeObject(overlayObj, passThrough, true);
	pauseObject->setMaterial(&pauseMat);
	pauseObject->setPosition(glm::vec3((WINDOW_WIDTH / 2), (WINDOW_HEIGHT / 2), 1.0f));
	pauseObject->setSpecificScale(86, 86, 0.01f);

	bossMat.loadTexture(TextureType::Diffuse, "Textures/boss_ShipUV.png");
	overlayMat.specular = matSpecular->specular;

	/////////////////////////////////////////////
	//the stuff for character customization
	PlayerTextures1.push_back(new Material());
	PlayerTextures1[0]->loadTexture(TextureType::Diffuse, "Textures/Basic_Ship_TexBlue.png");
	PlayerTextures1[0]->specular = matSpecular->specular;

	PlayerTextures1.push_back(new Material());
	PlayerTextures1[1]->loadTexture(TextureType::Diffuse, "Textures/Fast_Ship_TexBlue.png");
	PlayerTextures1[1]->specular = matSpecular->specular;

	PlayerTextures1.push_back(new Material());
	PlayerTextures1[2]->loadTexture(TextureType::Diffuse, "Textures/Heavy_Ship_TexBlue.png");
	PlayerTextures1[2]->specular = matSpecular->specular;

	PlayerTurretTex1.push_back(new Material());
	PlayerTurretTex1[0]->loadTexture(TextureType::Diffuse, "Textures/Basic_Turret_TexBlue.png");
	PlayerTurretTex1[0]->specular = matSpecular->specular;

	PlayerTurretTex1.push_back(new Material());
	PlayerTurretTex1[1]->loadTexture(TextureType::Diffuse, "Textures/Fast_Turret_TexBlue.png");
	PlayerTurretTex1[1]->specular = matSpecular->specular;

	PlayerTurretTex1.push_back(new Material());
	PlayerTurretTex1[2]->loadTexture(TextureType::Diffuse, "Textures/Heavy_Turret_TexBlue.png");
	PlayerTurretTex1[2]->specular = matSpecular->specular;

	PlayerTextures2.push_back(new Material());
	PlayerTextures2[0]->loadTexture(TextureType::Diffuse, "Textures/Basic_Ship_TexGreen.png");
	PlayerTextures2[0]->specular = matSpecular->specular;

	PlayerTextures2.push_back(new Material());
	PlayerTextures2[1]->loadTexture(TextureType::Diffuse, "Textures/Fast_Ship_TexGreen.png");
	PlayerTextures2[1]->specular = matSpecular->specular;

	PlayerTextures2.push_back(new Material());
	PlayerTextures2[2]->loadTexture(TextureType::Diffuse, "Textures/Heavy_Ship_TexGreen.png");
	PlayerTextures2[2]->specular = matSpecular->specular;

	PlayerTurretTex2.push_back(new Material());
	PlayerTurretTex2[0]->loadTexture(TextureType::Diffuse, "Textures/Basic_Turret_TexGreen.png");
	PlayerTurretTex2[0]->specular = matSpecular->specular;

	PlayerTurretTex2.push_back(new Material());
	PlayerTurretTex2[1]->loadTexture(TextureType::Diffuse, "Textures/Fast_Turret_TexGreen.png");
	PlayerTurretTex2[1]->specular = matSpecular->specular;

	PlayerTurretTex2.push_back(new Material());
	PlayerTurretTex2[2]->loadTexture(TextureType::Diffuse, "Textures/Heavy_Turret_TexGreen.png");
	PlayerTurretTex2[2]->specular = matSpecular->specular;

	charTurretTexture.push_back(new Material);
	charTurretTexture[0]->loadTexture(TextureType::Diffuse, "Textures/p1 standard turret.png");
	charTurretTexture[0]->specular = matSpecular->specular;

	charTurretTexture.push_back(new Material);
	charTurretTexture[1]->loadTexture(TextureType::Diffuse, "Textures/p1 fast turret.png");
	charTurretTexture[1]->specular = matSpecular->specular;

	charTurretTexture.push_back(new Material);
	charTurretTexture[2]->loadTexture(TextureType::Diffuse, "Textures/p1 heavy turret.png");
	charTurretTexture[2]->specular = matSpecular->specular;

	charTurretTexture.push_back(new Material);
	charTurretTexture[3]->loadTexture(TextureType::Diffuse, "Textures/p2 standard turret.png");
	charTurretTexture[3]->specular = matSpecular->specular;

	charTurretTexture.push_back(new Material);
	charTurretTexture[4]->loadTexture(TextureType::Diffuse, "Textures/p2 fast turret.png");
	charTurretTexture[4]->specular = matSpecular->specular;

	charTurretTexture.push_back(new Material);
	charTurretTexture[5]->loadTexture(TextureType::Diffuse, "Textures/p2 heavy turret.png");
	charTurretTexture[5]->specular = matSpecular->specular;

	charBodiesTexture.push_back(new Material);
	charBodiesTexture[0]->loadTexture(TextureType::Diffuse, "Textures/standard ship.png");
	charBodiesTexture[0]->specular = matSpecular->specular;

	charBodiesTexture.push_back(new Material);
	charBodiesTexture[1]->loadTexture(TextureType::Diffuse, "Textures/fast ship.png");
	charBodiesTexture[1]->specular = matSpecular->specular;

	charBodiesTexture.push_back(new Material);
	charBodiesTexture[2]->loadTexture(TextureType::Diffuse, "Textures/heavy ship.png");
	charBodiesTexture[2]->specular = matSpecular->specular;

	/////////////////////////////////////////////

	healthBars.push_back(new Mesh());
	healthBars[0]->loadFromFile("meshes/HealthBar.obj");
	healthBars.push_back(new Mesh());
	healthBars[1]->loadFromFile("meshes/HealthBar1.obj");

	healthBarsFast.push_back(new Mesh());
	healthBarsFast[0]->loadFromFile("meshes/HealthBar.obj");
	healthBarsFast.push_back(new Mesh());
	healthBarsFast[1]->loadFromFile("meshes/HealthBar2.obj");

	healthBarsHeavy.push_back(new Mesh());
	healthBarsHeavy[0]->loadFromFile("meshes/HealthBar.obj");
	healthBarsHeavy.push_back(new Mesh());
	healthBarsHeavy[1]->loadFromFile("meshes/HealthBar1.obj");

	//setup the arena
	arena1 = new GameObject;
	arena1->oldInitializeObject("meshes/ArenaModel.obj");
	arena1->setRotation(glm::vec3(90, -90, 0));
	arena1->setPosition(glm::vec3(0.f, 0.f, 1.4f));
	arena1->setScale(1.2);
	arena1->radius = 27.5f;
	arena1->setMaterial("Textures/ArenaUvTextured2.png", "Textures/monkeySpecular");
	arena1->NUM_ITERATIONS = 8;

	bossStandardHP = new GameObject;
	bossStandardHP->initializeObject(healthBars[0], passThrough, true);
	bossStandardHP->setSpecificScale(600, 5, 0.1);
	bossStandardHP->setMaterial("Textures/red.png", "Textures/monkeySpecular");
	bossStandardHP->setPosition(glm::vec3(955, 1050, 0.1));

	player1StandardHP = new GameObject;
	player1HPEmpty = new GameObject;
	//Morph.push_back(player1HP);
	//player1HP->shouldAnimate = true;
	player1StandardHP->initializeObject(healthBars[0], passThrough, true);
	player1StandardHP->initializeObject(healthBars[1], passThrough, true);

	player1StandardHP->morph.loadMorphTarget();
	player1HPEmpty->oldInitializeObject("meshes/HealthBar.obj");
	player1HPEmpty->setMaterial("Textures/red.png", "Textures/monkeySpecular");
	player1StandardHP->setMaterial(&blue);
	player1StandardHP->setPosition(glm::vec3(200.f, WINDOW_HEIGHT * .25f, 0.f));
	player1HPEmpty->setPosition(glm::vec3(200.f, WINDOW_HEIGHT * .25f, 0.f));

	player1FastHP = new GameObject;
	player1FastHP->initializeObject(healthBarsFast[0], passThrough, true);
	player1FastHP->initializeObject(healthBarsFast[1], passThrough, true);

	player1FastHP->morph.loadMorphTarget();
	player1FastHP->setMaterial(&blue);
	player1FastHP->setPosition(glm::vec3(200.f, WINDOW_HEIGHT * .25f, 0.f));

	player1HeavyHP = new GameObject;
	player1HeavyHP->initializeObject(healthBarsHeavy[0], passThrough, true);
	player1HeavyHP->initializeObject(healthBarsHeavy[1], passThrough, true);

	player1HeavyHP->morph.loadMorphTarget();
	player1HeavyHP->setMaterial(&blue);
	player1HeavyHP->setPosition(glm::vec3(200.f, WINDOW_HEIGHT * .25f, 0.f));

	player2StandardHP = new GameObject;
	player2HPEmpty = new GameObject;
	player2StandardHP->initializeObject(healthBars[0], passThrough, true);
	player2StandardHP->initializeObject(healthBars[1], passThrough, true);

	player2StandardHP->morph.loadMorphTarget();
	player2HPEmpty->oldInitializeObject("meshes/HealthBar.obj");
	player2HPEmpty->setMaterial("Textures/red.png", "Textures/monkeySpecular");
	player2StandardHP->setMaterial(&green);
	player2StandardHP->setPosition(glm::vec3(WINDOW_WIDTH - 200.f, WINDOW_HEIGHT * .25f, 0.f));
	player2HPEmpty->setPosition(glm::vec3(WINDOW_WIDTH - 200.f, WINDOW_HEIGHT * .25f, 0.f));

	player2FastHP = new GameObject;
	player2FastHP->initializeObject(healthBarsFast[0], passThrough, true);
	player2FastHP->initializeObject(healthBarsFast[1], passThrough, true);

	player2FastHP->morph.loadMorphTarget();
	player2FastHP->setMaterial(&green);
	player2FastHP->setPosition(glm::vec3(WINDOW_WIDTH - 200.f, WINDOW_HEIGHT * .25f, 0.f));

	player2HeavyHP = new GameObject;
	player2HeavyHP->initializeObject(healthBarsHeavy[0], passThrough, true);
	player2HeavyHP->initializeObject(healthBarsHeavy[1], passThrough, true);

	player2HeavyHP->morph.loadMorphTarget();
	player2HeavyHP->setMaterial(&green);
	player2HeavyHP->setPosition(glm::vec3(WINDOW_WIDTH - 200.f, WINDOW_HEIGHT * .25f, 0.f));

	start = new GameObject;
	start->initializeObject(startBar, passThrough, true);
	start->setMaterial("Textures/Start.png", "Textures/monkeySpecular.png");
	start->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) - 70.f, (WINDOW_HEIGHT * 0.5f) - 130, 0.f));
	start->setScale(10);
	start->setRotation(glm::vec3(0, 180.f, 0));

	quit = new GameObject;
	quit->initializeObject(startBar, passThrough, true);
	quit->setMaterial("Textures/Quit.png", "Textures/monkeySpecular.png");
	quit->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) - 70.f, (WINDOW_HEIGHT * 0.5f) - 260, 0.f));
	quit->setRotation(glm::vec3(0, 180.f, 0));
	quit->setScale(10);

	onslaughtLogo = new GameObject;
	onslaughtLogo->initializeObject(startBar, passThrough, true);
	onslaughtLogo->setMaterial("Textures/Onslaught logo.png", "Textures/monkeySpecular.png");
	onslaughtLogo->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) - 200.f, (WINDOW_HEIGHT * 0.5f) + 80, 0.f));
	onslaughtLogo->setRotation(glm::vec3(0, 180.f, 0));

	winScreen = new GameObject;
	winScreen->initializeObject(startBar, passThrough, true);
	winScreen->setMaterial("Textures/win.png", "Textures/monkeySpecular.png");
	winScreen->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) - 400.f, (WINDOW_HEIGHT * 0.5f) + 80, 0.f));
	winScreen->setSpecificScale(120, 120, 0.01);

	SMP1 = new GameObject;
	SMP1->initializeObject(overlayObj, passThrough, true);
	SMP1->setMaterial("Textures/coin.png", "Textures/monkeySpecular.png");
	SMP1->setPosition(glm::vec3(0, 200, 1.1f));
	SMP1->setSpecificScale(1, 1, 0.01);

	SMP2 = new GameObject;
	SMP2->initializeObject(overlayObj, passThrough, true);
	SMP2->setMaterial("Textures/right_multiplier_overlay.png", "Textures/monkeySpecular.png");
	SMP2->setPosition(glm::vec3(1000, 580, 2.f));
	SMP2->setSpecificScale(80, 80, 0.01);

	player_cust1 = new GameObject;
	player_cust1->initializeObject(startBar, passThrough, true);
	player_cust1->setMaterial(charTurretTexture[0]);
	player_cust1->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) - 200.f, (WINDOW_HEIGHT * 0.5f) + 210, 0.f));
	player_cust1->setSpecificScale(70, 70, 0.01);
	player_cust1->setRotation(glm::vec3(0, 180, 90));

	player_cust2 = new GameObject;
	player_cust2->initializeObject(startBar, passThrough, true);
	player_cust2->setMaterial(charTurretTexture[3]);
	player_cust2->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) + 200.f, (WINDOW_HEIGHT * 0.5f) + 210, 0.f));
	player_cust2->setSpecificScale(70, 70, 0.01);
	player_cust2->setRotation(glm::vec3(0, 180, 90));

	player_body1 = new GameObject;
	player_body1->initializeObject(startBar, passThrough, true);
	player_body1->setMaterial(charBodiesTexture[0]);
	player_body1->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) - 200.f, (WINDOW_HEIGHT * 0.5f) + 210, 0.1f));
	player_body1->setSpecificScale(70, 70, 0.01);
	player_body1->setRotation(glm::vec3(0, 180, 90));

	player_body2 = new GameObject;
	player_body2->initializeObject(startBar, passThrough, true);
	player_body2->setMaterial(charBodiesTexture[0]);
	player_body2->setPosition(glm::vec3((WINDOW_WIDTH * 0.5f) + 200.f, (WINDOW_HEIGHT * 0.5f) + 210, 0.1f));
	player_body2->setSpecificScale(70, 70, 0.01);
	player_body2->setRotation(glm::vec3(0, 180, 90));

	enemy = new Mesh();
	enemy->loadFromFile("meshes/NewVehicle.obj");

	graveMesh = new Mesh();
	graveMesh->loadFromFile("meshes/grave.obj");

	spawnMesh = new Mesh();
	spawnMesh->loadFromFile("meshes/spawn.obj");

	enemyBullet = new Mesh();
	enemyBullet->loadFromFile("meshes/bullet.obj");

	bodies.push_back(new Mesh);
	bodies[0]->loadFromFile("meshes/ShipBasic.obj");

	bodies.push_back(new Mesh);
	bodies[1]->loadFromFile("meshes/ShipFast.obj");

	bodies.push_back(new Mesh);
	bodies[2]->loadFromFile("meshes/ShipHeavy.obj");

	turrets.push_back(new Mesh);
	turrets[0]->loadFromFile("meshes/TurretBasic.obj");

	turrets.push_back(new Mesh);
	turrets[1]->loadFromFile("meshes/TurretFast.obj");

	turrets.push_back(new Mesh);
	turrets[2]->loadFromFile("meshes/TurretHeavy.obj");

	bossBody = new Mesh();
	bossBody->loadFromFile("meshes/Boss.obj");
	//setup the enemies
	for (int i = 0; i < 15; i++)
	{
		theEnemies.push_back(new Grunt(enemyBullet));
		theEnemies[i]->initializeObject(spawnMesh, phong);
		theEnemies[i]->setPosition(glm::vec3(rand() % 30, rand() % 30, 0));
		theEnemies[i]->setRotationAngleX(90);
		theEnemies[i]->enemyTexture = &enemyTexture;
		theEnemies[i]->setDamageTexture(&turretTip);
		theEnemies[i]->setMaterial(&turretTip);
		theEnemies[i]->setBulletTexture(&redTexture);
	}

	for (int i = 0; i < 4; i++)
	{
		theEnemies.push_back(new Avengers(enemyBullet));
		theEnemies[avengerIndex + i]->initializeObject(spawnMesh, phong);
		theEnemies[avengerIndex + i]->setPosition(glm::vec3(rand() % 30, rand() % 30, 0));
		theEnemies[avengerIndex + i]->setRotationAngleX(90);
		theEnemies[avengerIndex + i]->setBulletTexture(&explTex);
		theEnemies[avengerIndex + i]->setDamageTexture(&turretTip);
	}

	theEnemies.push_back(new Sprinkler(enemyBullet));
	theEnemies[sprinklerIndex]->initializeObject(spawnMesh, phong);
	theEnemies[sprinklerIndex]->setPosition(glm::vec3(rand() % 30, rand() % 30, 0));
	theEnemies[sprinklerIndex]->setRotation(glm::vec3(90, 0, 0));
	theEnemies[sprinklerIndex]->setBulletTexture(&purpleTexture);
	theEnemies[sprinklerIndex]->setDamageTexture(&turretTip);

	//the boss!
	theEnemies.push_back(new Boss(enemyBullet, bossBody, &purpleTexture, phong));
	theEnemies[bossIndex]->initializeObject(spawnMesh, phong);
	theEnemies[bossIndex]->setRotationAngleX(90);
	theEnemies[bossIndex]->enemyTexture = &bossMat;
	theEnemies[bossIndex]->setMaterial(&bossMat);
	theEnemies[bossIndex]->setDamageTexture(&turretTip);

	//the reviver!
	theEnemies.push_back(new Revive(enemyBullet));
	theEnemies[reviveIndex]->initializeObject(graveMesh, phong);
	theEnemies[reviveIndex]->setRotationAngleX(90);
	theEnemies[reviveIndex]->setBulletTexture(&turretTip);
	theEnemies[reviveIndex]->setDamageTexture(&redTexture);

	playerOne = new Player(indexOne, &blue, &blue);
	playerOne->damage = &blue;
	playerOne->loadMeshes(bodies[0], turrets[0], CustomizationType::Basic, CustomizationType::Basic, phong, PlayerTextures1[0], PlayerTurretTex1[0]);
	playerOne->setScale(0.55);
	playerOne->GameObject::setPosition(glm::vec3(-15, 0, 0));
	playerOne->setDamage(&redTexture);

	playerTwo = new Player(indexTwo, &green, &green);
	playerTwo->damage = &green;
	playerTwo->loadMeshes(bodies[1], turrets[1], CustomizationType::Basic, CustomizationType::Basic, phong, PlayerTextures2[0], PlayerTurretTex2[0]);
	playerTwo->setScale(0.55);
	playerTwo->GameObject::setPosition(glm::vec3(15, 0, 0));
	playerTwo->setDamage(&redTexture);

	tempPlayer1 = new GameObject;
	tempPlayer1->setMaterial(PlayerTextures1[0]);
	tempPlayer1->initializeObject(bodies[0], phong);
	tempPlayer1->setScale(1.2);
	tempPlayer1->setPosition(glm::vec3(-11, -1, 1));
	tempPlayer1->setRotation(glm::vec3(70, 180, 0));

	tempTurret1 = new GameObject;
	tempTurret1->setMaterial(PlayerTurretTex1[0]);
	tempTurret1->initializeObject(turrets[0], phong);
	tempTurret1->setPosition(glm::vec3(-11, 0, 1));
	tempTurret1->setRotation(glm::vec3(70, 180, 0));

	tempPlayer2 = new GameObject;
	tempPlayer2->setMaterial(PlayerTextures2[0]);
	tempPlayer2->initializeObject(bodies[0], phong);
	tempPlayer2->setScale(1.2);
	tempPlayer2->setPosition(glm::vec3(11, -1, 1));
	tempPlayer2->setRotation(glm::vec3(70, 180, 0));

	tempTurret2 = new GameObject;
	tempTurret2->setMaterial(PlayerTurretTex2[0]);
	tempTurret2->initializeObject(turrets[0], phong);
	tempTurret2->setPosition(glm::vec3(11, 0, 1));
	tempTurret2->setRotation(glm::vec3(70, 180, 0));

	//the powerups/////////////////////////////////
	//the repair mat
	repairMat.loadTexture(TextureType::Diffuse, "Textures/RepairTest.png");
	repairMat.specular = matSpecular->specular;

	//the coin mat
	coinMat.loadTexture(TextureType::Diffuse, "Textures/Coin.png");
	coinMat.specular = matSpecular->specular;
	coinMat.loadTexture(TextureType::Normal, "Textures/CoinNormal.png");

	//the repair object
	repair = new Mesh();
	repair->loadFromFile("meshes/RepairPowerUpModel.obj");

	//the coin object
	coin = new Mesh();
	coin->loadFromFile("meshes/Coin.obj");

	phongNormal = new ShaderProgram;

	phongNormal->load("shaders/PhongNormals.vert", "shaders/PhongNormals.frag");

	//set up the game object for the powerup
	thePowerUp = new PowerUp();
	thePowerUp->initializeObject(coin, phongNormal);
	thePowerUp->setScale(1.5);

	delete matSpecular;

	gamePause = false;

	for (int i = 0; i < 8; i++)
	{
		regularPositions[i] = theEnemies[0]->spawnPosition[i];
		regBossPositions[i] = ((Grunt*)theEnemies[0])->gruntPosition[i];

		if (waveStates == 2)
			particlePositions[i] = regBossPositions[i];
		else
			particlePositions[i] = regularPositions[i];
	}
}

void loadShaders()
{
	materials["standard"] = std::make_shared<ShaderProgram>();
	materials["standard"]->load("shaders/default.vert", "shaders/unlit.frag");
	materials["standard"]->linkProgram();

	materials["brightPass"] = std::make_shared<ShaderProgram>();
	materials["brightPass"]->load("shaders/default.vert", "shaders/brightPass.frag");
	materials["brightPass"]->linkProgram();

	materials["blurVertical"] = std::make_shared<ShaderProgram>();
	materials["blurVertical"]->load("shaders/default.vert", "shaders/blurVertical.frag");
	materials["blurVertical"]->linkProgram();

	materials["blurHorizontal"] = std::make_shared<ShaderProgram>();
	materials["blurHorizontal"]->load("shaders/default.vert", "shaders/blurHorizontal.frag");
	materials["blurHorizontal"]->linkProgram();

	materials["bloom"] = std::make_shared<ShaderProgram>();
	materials["bloom"]->load("shaders/default.vert", "shaders/Bloom.frag");
	materials["bloom"]->linkProgram();

	shadowInt = std::make_shared<Texture>();
	if (!shadowInt->load("Textures/Shadow Interactive.png", true))
	{
		std::cerr << "Error loading Splash Texture" << std::endl;
	}

	//materials["lut"] = std::make_shared<ShaderProgram>();
	//materials["lut"]->load("shaders/default.vert", "shaders/LUT.frag");
	//materials["lut"]->linkProgram();
	//
	//warmLut.setLutSize(32.f);
	//warmLut.load("warm.cube");
	//
	//coolLut.setLutSize(32.f);
	//coolLut.load("cool.cube");

}

void drawUI()
{
	if (gameStates == splashScreen)
	{
		shadowInt->bind(GL_TEXTURE0);

		materials["standard"]->bind();
		materials["standard"]->sendUniformMat4("u_mv", glm::value_ptr(game->cameraTransform), false);
		materials["standard"]->sendUniformMat4("u_mvp", glm::value_ptr(temp), false);

		quads["standard"]->draw();

		materials["standard"]->unbind();

		shadowInt->unbind(GL_TEXTURE0);
	}
	if (gameStates == charCreation)
	{
		glDisable(GL_DEPTH_TEST);

		player_cust1->drawUI(game->identity, game->cameraOrtho);
		player_cust2->drawUI(game->identity, game->cameraOrtho);

		player_body1->drawUI(game->identity, game->cameraOrtho);
		player_body2->drawUI(game->identity, game->cameraOrtho);

		if (readyP1)
			ready1->drawUI(game->identity, game->cameraOrtho);

		if (readyP2)
			ready2->drawUI(game->identity, game->cameraOrtho);

		tutorial->drawUI(game->identity, game->cameraOrtho);

		if (readyP1 == true && readyP2 == true)
		{
			tutorial->setMaterial(&readyMat2);
		}
		else
			tutorial->setMaterial(&readyMat1);

		glEnable(GL_DEPTH_TEST);
	}


	//after both players are ready, the game begins and enemies are drawn
	if (gameStates == level1)
	{
		glDisable(GL_DEPTH_TEST);
		tutorial->drawUI(game->identity, game->cameraOrtho);
		glEnable(GL_DEPTH_TEST);
	}

	if (gameStates == menu)
	{
		onslaughtLogo->drawUI(game->identity, game->cameraOrtho);
	}
	if (gameStates == endGame)
	{
		if (win == false)
			gameOver->drawUI(game->identity, game->cameraOrtho);
		else if (win == true)
			winScreen->drawUI(game->identity, game->cameraOrtho);
	}
	if (gameStates == leaderBoard)
	{
		leaderboard->drawUI(game->identity, game->cameraOrtho);
	}
	if (gameStates == level2)
	{
		if (tempIndex1 == 0)
			player1StandardHP->drawUI(game->identity, game->cameraOrtho);
		else if (tempIndex1 == 1)
			player1FastHP->drawUI(game->identity, game->cameraOrtho);
		else if (tempIndex1 == 2)
			player1HeavyHP->drawUI(game->identity, game->cameraOrtho);

		player1HPEmpty->drawUI(game->identity, game->cameraOrtho);

		if (tempIndex2 == 0)
			player2StandardHP->drawUI(game->identity, game->cameraOrtho);
		else if (tempIndex2 == 1)
			player2FastHP->drawUI(game->identity, game->cameraOrtho);
		else if (tempIndex2 == 2)
			player2HeavyHP->drawUI(game->identity, game->cameraOrtho);

		if (waveStates == final_boss || waveStates == reg_boss || waveStates == avg_boss)
		{
			bossStandardHP->drawUI(game->identity, game->cameraOrtho);
		}

		player2HPEmpty->drawUI(game->identity, game->cameraOrtho);
		//glDisable(GL_DEPTH_TEST);
		overlayObject->drawUI(game->identity, game->cameraOrtho);
		//glEnable(GL_DEPTH_TEST);
		SMP1->drawUI(game->identity, game->cameraOrtho);
		SMP2->drawUI(game->identity, game->cameraOrtho);
	}
	if (gamePause == true)
	{
		glDisable(GL_DEPTH_TEST);
		pauseObject->drawUI(game->identity, game->cameraOrtho);
		glEnable(GL_DEPTH_TEST);
	}
}

void drawMenu(bool draw)
{
	if (draw)
	{
		if (gameStates == menu)
		{
			if (selection == 0)
				start->drawUI(game->identity, game->cameraOrtho);
			else
				quit->drawUI(game->identity, game->cameraOrtho);
		}
	}
	else
	{
		if (gameStates == menu)
		{
			start->drawUI(game->identity, game->cameraOrtho);
			quit->drawUI(game->identity, game->cameraOrtho);
		}
	}
}

void drawScene(bool playerDraw)
{
	if (gameStates != splashScreen)
		arena1->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);

	if (gameStates == charCreation)
	{
		tempPlayer1->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);
		tempPlayer2->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);

		tempTurret1->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);
		tempTurret2->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);
	}

	if (gameStates != splashScreen && gameStates != charCreation && gameStates != leaderBoard)
	{	//player one is always drawn as it's splining around on the menu
		if (playerOne->getAlive() == true)
			playerOne->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);

		//player two spawns after the game starts and enter the tutorial, at that point the UI and player two are drawn
		if (playerTwo->getAlive() == true && gameStates != menu)
		{
			playerTwo->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);
		}
	}

	if (gameStates == leaderBoard)
	{
		//filehandler.sortData();
		int dataSize = filehandler.nameData.size() - 1; //score data is stored lowest at n=0 to highest at n-1 

		if (playerOne->score > playerTwo->score)
			playerHighscore = playerOne->score;
		else if (playerOne->score < playerTwo->score)
			playerHighscore = playerTwo->score;

		game->renderText((WINDOW_WIDTH / 2) + 10, (WINDOW_HEIGHT / 2) + 223, 0, playerHighscoreName);
		game->renderText((WINDOW_WIDTH / 2) - 40, (WINDOW_HEIGHT / 2) + 160, 0, std::to_string(playerHighscore));

		game->renderText((WINDOW_WIDTH / 2) - 380, (WINDOW_HEIGHT / 2) + 70, 0, filehandler.nameData[dataSize]);
		game->renderText((WINDOW_WIDTH / 2) + 280, (WINDOW_HEIGHT / 2) + 70, 0, std::to_string(filehandler.scoreData[dataSize]));

		game->renderText((WINDOW_WIDTH / 2) - 380, (WINDOW_HEIGHT / 2) + 20, 0, filehandler.nameData[dataSize - 1]);
		game->renderText((WINDOW_WIDTH / 2) + 280, (WINDOW_HEIGHT / 2) + 20, 0, std::to_string(filehandler.scoreData[dataSize - 1]));

		game->renderText((WINDOW_WIDTH / 2) - 380, (WINDOW_HEIGHT / 2) - 30, 0, filehandler.nameData[dataSize - 2]);
		game->renderText((WINDOW_WIDTH / 2) + 280, (WINDOW_HEIGHT / 2) - 30, 0, std::to_string(filehandler.scoreData[dataSize - 2]));

		game->renderText((WINDOW_WIDTH / 2) - 380, (WINDOW_HEIGHT / 2) - 80, 0, filehandler.nameData[dataSize - 3]);
		game->renderText((WINDOW_WIDTH / 2) + 280, (WINDOW_HEIGHT / 2) - 80, 0, std::to_string(filehandler.scoreData[dataSize - 3]));

		game->renderText((WINDOW_WIDTH / 2) - 380, (WINDOW_HEIGHT / 2) - 130, 0, filehandler.nameData[dataSize - 4]);
		game->renderText((WINDOW_WIDTH / 2) + 280, (WINDOW_HEIGHT / 2) - 130, 0, std::to_string(filehandler.scoreData[dataSize - 4]));
	}

	if (gameStates == endGame && win == true)
	{
		game->renderText((WINDOW_WIDTH / 2) - 40, (WINDOW_HEIGHT / 2) + 70, 0, std::to_string(playerOne->score + playerTwo->score));
	}

	if (gameStates == level2 || gameStates == level1)
	{
		if (thePowerUp->Alive == true)
			thePowerUp->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);

		for (int i = 0; i < theEnemies.size() - 2; i++)
		{
			theEnemies[i]->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);

			if (theEnemies[i]->Spawn == true && theEnemies[i]->_enemyType == EnemyType(0))
			{
				if (theEnemies[i]->SpawnIndex == 0)
				{
					for (int j = 0; j < 62; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 1)
				{
					for (int j = 62; j < 124; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 2)
				{
					for (int j = 124; j < 186; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 3)
				{
					for (int j = 186; j < 248; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 4)
				{
					for (int j = 248; j < 310; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 5)
				{
					for (int j = 310; j < 372; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 6)
				{
					for (int j = 372; j < 434; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 7)
				{
					for (int j = 434; j < 500; j++)
						particlesVec[j]->draw(game->cameraTransform, game->cameraProj);
				}
			}
			if (theEnemies[i]->Spawn == true && theEnemies[i]->_enemyType == EnemyType(1))
			{
				if (theEnemies[i]->SpawnIndex == 0)
				{
					for (int j = 0; j < 62; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 1)
				{
					for (int j = 62; j < 124; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 2)
				{
					for (int j = 124; j < 186; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 3)
				{
					for (int j = 186; j < 248; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 4)
				{
					for (int j = 248; j < 310; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 5)
				{
					for (int j = 310; j < 372; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 6)
				{
					for (int j = 372; j < 434; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 7)
				{
					for (int j = 434; j < 500; j++)
						particlesVec2[j]->draw(game->cameraTransform, game->cameraProj);
				}

			}
			if (theEnemies[i]->Spawn == true && theEnemies[i]->_enemyType == EnemyType(2))
			{
				if (theEnemies[i]->SpawnIndex == 0)
				{
					for (int j = 0; j < 62; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 1)
				{
					for (int j = 62; j < 124; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 2)
				{
					for (int j = 124; j < 186; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 3)
				{
					for (int j = 186; j < 248; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 4)
				{
					for (int j = 248; j < 310; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 5)
				{
					for (int j = 310; j < 372; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 6)
				{
					for (int j = 372; j < 434; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}
				if (theEnemies[i]->SpawnIndex == 7)
				{
					for (int j = 434; j < 500; j++)
						particlesVec3[j]->draw(game->cameraTransform, game->cameraProj);
				}

			}

			if (explodePls == true)
			{
				particles[3]->draw(game->cameraTransform, game->cameraProj);
			}
		}

		if (theEnemies[bossIndex]->Spawn == true && theEnemies[bossIndex]->Alive == false)
		{
			for (int j = 0; j < 62; j++)
				particlesVec6[j]->draw(game->cameraTransform, game->cameraProj);
		}

		if (theEnemies[reviveIndex]->Spawn == true && theEnemies[reviveIndex]->Alive == false)
		{
			for (int j = 0; j < 62; j++)
				particlesVec5[j]->draw(game->cameraTransform, game->cameraProj);
		}

		if (theEnemies[bossIndex]->Alive == true || theEnemies[bossIndex]->Spawn == true)
		{
			theEnemies[bossIndex]->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);
		}
		if (theEnemies[reviveIndex]->Alive == true || theEnemies[reviveIndex]->Spawn == true)
		{
			theEnemies[reviveIndex]->draw(game->cameraTransform, game->cameraProj, pointLights, directionalLight);
		}

		game->renderScore(playerOne->score, playerTwo->score);
		game->renderGameInfo(totalWave+1);
		//game->renderGameInfo(activeMusic);
	}
}

//drawing our depth for shadows
void drawLightScene()
{
	/*glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);*/
	if (gameStates != splashScreen && gameStates != charCreation && gameStates != leaderBoard)
	{	//player one is always drawn as it's splining around on the menu
		if (playerOne->getAlive() == true)
			playerOne->drawFromLight(game->lightView, game->lightOrtho);

		//player two spawns after the game starts and enter the tutorial, at that point the UI and player two are drawn
		if (playerTwo->getAlive() == true && gameStates != menu)
		{
			playerTwo->drawFromLight(game->lightView, game->lightOrtho);
		}
	}
	/*glCullFace(GL_BACK);
	glDisable(GL_CULL_FACE);*/
	if (gameStates != splashScreen)
	{
		arena1->drawFromLight(game->lightView, game->lightOrtho);
	}

	if (gameStates == level2)
	{
		if (thePowerUp->Alive == true)
			thePowerUp->drawFromLight(game->lightView, game->lightOrtho);
	}

}

//function for drawing things, FUN :D
void DisplayCallbackFunction(void)
{
	game->draw();

	//draw depth for shadows
	frameBuf["shadowDepth"]->bindFrameBufferForDrawing();
	frameBuf["shadowDepth"]->clearFrameBuffer(glm::vec4(0.f, 0.f, 0.f, 1.f));

	drawLightScene();

	frameBuf["shadowDepth"]->unbindFrameBuffer(1024, 1024);

	frameBuf["standard"]->bindFrameBufferForDrawing();
	frameBuf["standard"]->clearFrameBuffer(glm::vec4(0.f, 0.f, 0.f, 1.f));

	frameBuf["shadowDepth"]->bindDepthTextureForSampling(GL_TEXTURE4);
	drawScene(true);
	frameBuf["shadowDepth"]->unbindTexture(GL_TEXTURE4);
	drawUI();
	drawMenu(false);

	frameBuf["standard"]->unbindFrameBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);

	glm::mat4 temp = glm::mat4(1.f);

	//particles->draw(game->cameraTransform, game->cameraProj);

	if (gameStates != splashScreen)
	{
		bloom(3);

		frameBuf["standard"]->bindTextureForSampling(0, GL_TEXTURE0);
		frameBuf["bloom1"]->bindTextureForSampling(0, GL_TEXTURE1);

		materials["bloom"]->bind();
		materials["bloom"]->sendUniformMat4("u_mv", glm::value_ptr(game->cameraTransform), false);
		materials["bloom"]->sendUniformMat4("u_mvp", glm::value_ptr(temp), false);

		quads["standard"]->draw();

		materials["bloom"]->unbind();
		frameBuf["bloom1"]->unbindTexture(GL_TEXTURE1);
		frameBuf["standard"]->unbindTexture(GL_TEXTURE0);
	}
	else
	{
		frameBuf["standard"]->bindTextureForSampling(0, GL_TEXTURE0);

		materials["standard"]->bind();
		materials["standard"]->sendUniformMat4("u_mv", glm::value_ptr(game->cameraTransform), false);
		materials["standard"]->sendUniformMat4("u_mvp", glm::value_ptr(temp), false);

		quads["standard"]->draw();

		materials["standard"]->unbind();
		frameBuf["standard"]->unbindTexture(GL_TEXTURE0);
	}

	//swap the buffers
	glutSwapBuffers();
}

void restart()
{
	for (int i = 0; i < theEnemies.size(); i++)
	{
		theEnemies[i]->Alive = false;
		theEnemies[i]->Spawn = false;
		theEnemies[i]->firing = false;
		theEnemies[i]->tutorial = false;
		theEnemies[i]->blnTutorialCollision = false;
		enemyShooting[i]->channel->setPaused(true);
		theEnemies[i]->deleteAllBullets();
	}

	((Boss*)theEnemies[bossIndex])->activated = false;

	sHeightI = 10, fHeightI = 7, hHeightI = 13, sHeightI2 = 10, fHeightI2 = 7, hHeightI2 = 13;
	blnGameOver = false, blnZero = true, blnOne = false, blnTwo = false, blnThree = false;
	for (int i = 0; i < 6; i++)
	{ //sets all sound effects to pause
		if (i < 2)
		{
			menuSFX[i]->channel->setPaused(true);
			powerUps[i]->channel->setPaused(true);
			thePowerUp->playSound = false;
		}
		else if (i < 3)
		{
			pFiring[i]->channel->setPaused(true);
			pFiring2[i]->channel->setPaused(true);
		}
		else if (i < 4)
		{
			backgroundMusic[i]->channel->setPaused(true);
		}
		announcerDialogue[i]->channel->setPaused(true);
	}
	readyP1 = false;
	readyP2 = false;
	waveStates = WaveStates(1);
	waveLimit = 3;
	waveNumber = 0;
	numActive = 0;
	waveProgression = 0;
	totalWave = 0;
	indexOne = 0;
	indexTwo = 1;
	tempIndex1 = 0;
	tempIndex2 = 0;
	tempTurretI1 = 0;
	tempTurretI2 = 0;
	c_bodies = CustomizationType(0);
	c_turrets = CustomizationType(0);
	playerOne->loadMeshes(bodies[0], turrets[0], c_bodies, c_turrets, phong, PlayerTextures1[0], PlayerTurretTex1[0]);
	playerTwo->loadMeshes(bodies[0], turrets[0], c_bodies, c_turrets, phong, PlayerTextures2[0], PlayerTurretTex2[0]);
	tempPlayer1->Object = bodies[tempIndex1];
	tempPlayer2->Object = bodies[tempIndex2];
	tempTurret1->Object = turrets[tempTurretI1];
	tempTurret2->Object = turrets[tempTurretI2];
	numActive = 0;
	selection = 0;
	progression = 0;
	yHealth = 150.0f;
	sHeightI = 10;
	fHeightI = 7;
	hHeightI = 13;
	sHeightI2 = 10;
	fHeightI2 = 7;
	hHeightI2 = 13;
	spawnTime = 0;
	waveTimer = 0;
	thePowerUp->Alive = false;
	player_body1->setMaterial(charBodiesTexture[tempIndex1]);
	player_cust1->setMaterial(charTurretTexture[tempTurretI1]);
	player_body2->setMaterial(charBodiesTexture[tempIndex2]);
	player_cust2->setMaterial(charTurretTexture[3 + tempTurretI2]);
	tempPlayer1->setMaterial(PlayerTextures1[tempIndex1]);
	tempPlayer2->setMaterial(PlayerTextures2[tempIndex2]);
	tempTurret1->setMaterial(PlayerTurretTex1[tempTurretI1]);
	tempTurret2->setMaterial(PlayerTurretTex2[tempTurretI2]);
	gameStates = menu;
	playerHighscore = 0;
	for (int i = 0; i < 6; i++)
	{
		se->system->playSound(announcerDialogue[i]->sound, 0, false, &announcerDialogue[i]->channel);
		announcerDialogue[i]->channel->setPaused(true);
	}
}

void tutorialInitialize()
{
	if (theEnemies[0]->Alive == false && theEnemies[0]->Spawn == false)
	{
		theEnemies[0]->initialize(spawnMesh, &spawnMat, 5, false);
		theEnemies[0]->calculateTarget(playerOne, playerTwo);
		theEnemies[0]->tutorial = true;
	}
	else if (theEnemies[avengerIndex]->Alive == false && theEnemies[avengerIndex]->Spawn == false)
	{
		theEnemies[avengerIndex]->initialize(spawnMesh, &spawnMat, 6, false);
		theEnemies[avengerIndex]->calculateTarget(playerOne, playerTwo);
		theEnemies[avengerIndex]->tutorial = true;
	}
	else if (theEnemies[sprinklerIndex]->Alive == false && theEnemies[sprinklerIndex]->Spawn == false)
	{
		theEnemies[sprinklerIndex]->initialize(spawnMesh, &spawnMat, 7, false);
		theEnemies[sprinklerIndex]->calculateTarget(playerOne, playerTwo);
		theEnemies[sprinklerIndex]->tutorial = true;
	}
}

void tutorialDeinitialize()
{
	theEnemies[0]->Alive = false;
	theEnemies[0]->Spawn = false;
	theEnemies[0]->tutorial = false;
	theEnemies[avengerIndex]->Alive = false;
	theEnemies[avengerIndex]->Spawn = false;
	theEnemies[avengerIndex]->tutorial = false;
	theEnemies[sprinklerIndex]->Alive = false;
	theEnemies[sprinklerIndex]->Spawn = false;
	theEnemies[sprinklerIndex]->tutorial = false;
}

//keyboard function for when a key is held down
void keyboardCallbackFunc(unsigned char key, int x, int y)
{
	game->keyboardUp(key, x, y);
}

void waveReset(WaveStates _state, int _waveLimit)
{
	for (int i = 0; i < theEnemies.size(); i++)
	{
		theEnemies[i]->Alive = false;
		theEnemies[i]->Spawn = false;
	}
	int waveLimit = _waveLimit;
	int waveNumber = 0;
	int numActive = 0;
	int waveProgression = 0;
	waveStates = _state;
}

//opposite of above, this is when a key is released
void keyboardUpCallbackFunc(unsigned char key, int x, int y)
{
	game->keyboardUp(key, x, y);

	switch (key)
	{
	case 13:
		if (playerHighscoreName.length() == 4)
		{
			//have prompt to confirm name or not?
			//save player highscore now
			filehandler.writeOnFile(playerHighscore, playerHighscoreName);
			filehandler.sortData();
			playerHighscoreName.clear();
			playerHighscore = 0;
			gameStates = menu;
			playerOne->reset(glm::vec3(-15, 0, 0));
			playerTwo->reset(glm::vec3(15, 0, 0));
			restart();
		}
		break;

	case 'p':
		gamePause = !gamePause;
		break;
	case 'P':
		gamePause = !gamePause;
		break;
	case 'R':
		gameStates = menu;
		playerOne->reset(glm::vec3(-15, 0, 0));
		playerTwo->reset(glm::vec3(15, 0, 0));
		restart();
		break;
	case 'T':
		for (int i = 0; i < theEnemies.size(); i++)
		{
			theEnemies[i]->toggleTexture = !theEnemies[i]->toggleTexture;
		}
		arena1->toggleTexture = !arena1->toggleTexture;
		playerOne->toggleTexture = !playerOne->toggleTexture;
		playerOne->playerTurret->toggleTexture = !playerOne->playerTurret->toggleTexture;
		playerTwo->toggleTexture = !playerTwo->toggleTexture;
		playerTwo->playerTurret->toggleTexture = !playerTwo->playerTurret->toggleTexture;
		break;
	case 'L':
		if (arena1->NUM_ITERATIONS != 0)
		{
			for (int i = 0; i < theEnemies.size(); i++)
			{
				theEnemies[i]->NUM_ITERATIONS = 0;
			}
			arena1->NUM_ITERATIONS = 0;
			playerOne->NUM_ITERATIONS = 0;
			playerOne->playerTurret->NUM_ITERATIONS = 0;
			playerTwo->NUM_ITERATIONS = 0;
			playerTwo->playerTurret->NUM_ITERATIONS = 0;
		}
		else
		{
			for (int i = 0; i < theEnemies.size(); i++)
			{
				theEnemies[i]->NUM_ITERATIONS = 8;
			}
			arena1->NUM_ITERATIONS = 8;
			playerOne->NUM_ITERATIONS = 8;
			playerOne->playerTurret->NUM_ITERATIONS = 8;
			playerTwo->NUM_ITERATIONS = 8;
			playerTwo->playerTurret->NUM_ITERATIONS = 8;
		}
		break;

	case 'n':
		if (gameStates < 6)
			gameStates = GameStates(gameStates + 1);

		if (theEnemies[0]->Alive == true || theEnemies[0]->Spawn == true) theEnemies[0]->setHealth(-10);
		if (theEnemies[avengerIndex]->Alive == true || theEnemies[avengerIndex]->Spawn == true)
		{
			theEnemies[avengerIndex]->Alive = false;
			theEnemies[avengerIndex]->Spawn = false;
		}
		if (theEnemies[sprinklerIndex]->Alive == true || theEnemies[sprinklerIndex]->Spawn == true) theEnemies[sprinklerIndex]->setHealth(-10);

		if (gameStates == level1)
		{
			tutorialInitialize();
		}
		else if (gameStates == level2)
		{
			tutorialDeinitialize();
		}
		else if (gameStates == endGame)
		{
			filehandler.sortData();
		}

		break;

	default:
		if (playerHighscoreName.length()<4)
			playerHighscoreName += key;
		break;
	}
}

void powerUpCalculations()
{
	if (thePowerUp->Alive == false && thePowerUp->powerupChance < 1)
	{
		powerupTemp = rand() % 2;

		if (powerupTemp == 0)
		{
			thePowerUp->initialize(thePowerUp->getPosition(), PowerUpType(1), coin, &coinMat);
		}
		else if (powerupTemp == 1)
		{
			thePowerUp->initialize(thePowerUp->getPosition(), PowerUpType(0), repair, &repairMat);
		}

		thePowerUp->powerupChance = 200;
	}
}

void waveManagement()
{
	if (tutEnd == true)
	{
		numActive = 0;
		tutEnd = false;
	}

	if ((playerOne->getAlive() == false && playerTwo->getAlive() == true) || (playerTwo->getAlive() == false && playerOne->getAlive() == true))
	{
		if (playerOne->getAlive() == false)
		{
			if (theEnemies[reviveIndex]->Alive == false && theEnemies[reviveIndex]->Spawn == false)
			{
				((Revive*)theEnemies[reviveIndex])->initialize(graveMesh, &spawnMat, playerOne->getPosition(), false);
				((Revive*)theEnemies[reviveIndex])->setPosition(playerOne->getPosition());
				announcerDialogue[3]->channel->setPaused(false);
				se->system->playSound(announcerDialogue[3]->sound, 0, true, &announcerDialogue[3]->channel);
			}
			player1StandardHP->setMaterial(&blue);
		}
		else if (playerTwo->getAlive() == false)
		{
			if (theEnemies[reviveIndex]->Alive == false && theEnemies[reviveIndex]->Spawn == false)
			{
				((Revive*)theEnemies[reviveIndex])->initialize(graveMesh, &spawnMat, playerTwo->getPosition(), false);
				((Revive*)theEnemies[reviveIndex])->setPosition(playerTwo->getPosition());
				announcerDialogue[3]->channel->setPaused(false);
				se->system->playSound(announcerDialogue[3]->sound, 0, true, &announcerDialogue[3]->channel);
			}
			player2StandardHP->setMaterial(&green);
		}
		theEnemies[reviveIndex]->setRotationAngleY(90);
	}

	//if there are still enemies left to spawn....
	if (waveNumber < waveLimit)
	{
		spawnTime += game->deltaTime;
		//check which wave state it is //depending on which, spawn enemies differently
		srand(time(NULL));

		if (spawnTime >= 1.5)
		{
			int temp = 0;
			if (waveStates == reg_boss)
				temp = rand() % 8;
			else if (waveStates == avg_boss)
				temp = rand() % 3;
			else
				temp = rand() % 15;

			if (waveStates == regular)
			{
				if (theEnemies[temp]->Spawn == false && theEnemies[temp]->Alive == false)
				{
					((Grunt*)theEnemies[temp])->initialize(spawnMesh, &spawnMat, 0, spawnPoint, false);
					theEnemies[temp]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}
				blnZero = false, blnOne = true;
				blnTwo = false, blnThree = false;
			}
			else if (waveStates == reg_boss)
			{
				waveLimit = 8;
				if (theEnemies[bossIndex]->Spawn == false && theEnemies[bossIndex]->Alive == false && ((Boss*)theEnemies[bossIndex])->activated == false)
				{
					bossStandardHP->setSpecificScale(600, 5, 0.1);
					((Boss*)theEnemies[bossIndex])->initialize(spawnMesh, &spawnMat, waveStates);
					theEnemies[bossIndex]->calculateTarget(playerOne, playerTwo);
				}

				if (theEnemies[bossIndex]->getHealth() <= 0 && ((Boss*)theEnemies[bossIndex])->activated == true)
				{
					numActive = 0;
					spawnTime = 0;
				}
				if (theEnemies[temp]->Spawn == false && theEnemies[temp]->Alive == false)
				{
					((Grunt*)theEnemies[temp])->initialize(spawnMesh, &spawnMat, 2, temp, false);
					theEnemies[temp]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}

			}
			else if (waveStates == avenger)
			{
				if (theEnemies[avengerIndex]->Spawn == false && theEnemies[avengerIndex]->Alive == false)
				{
					theEnemies[avengerIndex]->initialize(spawnMesh, &spawnMat, spawnPoint, false);
					theEnemies[avengerIndex]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}

				else if (theEnemies[temp]->Spawn == false && theEnemies[temp]->Alive == false)
				{
					((Grunt*)theEnemies[temp])->initialize(spawnMesh, &spawnMat, 0, spawnPoint, false);
					theEnemies[temp]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}
			}
			else if (waveStates == avg_boss)
			{
				waveLimit = 8;
				int chance = rand() % 100;

				if (chance < 8)
				{
					if (theEnemies[avengerIndex + temp]->Spawn == false && theEnemies[avengerIndex + temp]->Alive == false)
					{
						theEnemies[avengerIndex + temp]->initialize(spawnMesh, &spawnMat, spawnPoint, false);
						theEnemies[avengerIndex + temp]->calculateTarget(playerOne, playerTwo);
						spawnTime = 0;
						waveNumber++;
						numActive++;
						spawnPoint++;
					}
				}
				else
				{
					if (theEnemies[temp]->Spawn == false && theEnemies[temp]->Alive == false)
					{
						((Grunt*)theEnemies[temp])->initialize(spawnMesh, &spawnMat, 0, temp, false);
						theEnemies[temp]->calculateTarget(playerOne, playerTwo);
						spawnTime = 0;
						waveNumber++;
						numActive++;
						spawnPoint++;
					}
				}

				if (theEnemies[bossIndex]->Spawn == false && theEnemies[bossIndex]->Alive == false && ((Boss*)theEnemies[bossIndex])->activated == false)
				{
					bossStandardHP->setSpecificScale(600, 5, 0.1);
					((Boss*)theEnemies[bossIndex])->initialize(spawnMesh, &spawnMat, waveStates);
					theEnemies[bossIndex]->calculateTarget(playerOne, playerTwo);
				}

			}
			else if (waveStates == sprinkler)
			{
				if (theEnemies[avengerIndex]->Spawn == false && theEnemies[avengerIndex]->Alive == false)
				{
					theEnemies[avengerIndex]->initialize(spawnMesh, &spawnMat, spawnPoint, false);
					theEnemies[avengerIndex]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}
				else if (theEnemies[sprinklerIndex]->Spawn == false && theEnemies[sprinklerIndex]->Alive == false)
				{
					theEnemies[sprinklerIndex]->initialize(spawnMesh, &spawnMat, spawnPoint, false);
					theEnemies[sprinklerIndex]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}
				else if (theEnemies[temp]->Spawn == false && theEnemies[temp]->Alive == false)
				{
					((Grunt*)theEnemies[temp])->initialize(spawnMesh, &spawnMat, 0, spawnPoint, false);
					theEnemies[temp]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}
			}
			else if (waveStates == final_boss)
			{
				waveLimit = 12;

				if (theEnemies[temp]->Spawn == false && theEnemies[temp]->Alive == false)
				{
					theEnemies[temp]->initialize(spawnMesh, &spawnMat, spawnPoint, false);
					theEnemies[temp]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}
				else if (theEnemies[avengerIndex]->Spawn == false && theEnemies[avengerIndex]->Alive == false)
				{
					theEnemies[avengerIndex]->initialize(spawnMesh, &spawnMat, spawnPoint, false);
					theEnemies[avengerIndex]->calculateTarget(playerOne, playerTwo);
					spawnTime = 0;
					waveNumber++;
					numActive++;
					spawnPoint++;
				}

				if (theEnemies[bossIndex]->Spawn == false && theEnemies[bossIndex]->Alive == false && ((Boss*)theEnemies[bossIndex])->activated == false)
				{
					bossStandardHP->setSpecificScale(600, 5, 0.1);
					((Boss*)theEnemies[bossIndex])->initialize(spawnMesh, &spawnMat, waveStates);
					theEnemies[bossIndex]->calculateTarget(playerOne, playerTwo);
				}
			}

			if (spawnPoint >= 7) spawnPoint = 0;
		}
	}
	else if (numActive <= 0 && waveNumber == waveLimit && waveStates == reg_boss && theEnemies[bossIndex]->Alive == true)
	{
		waveProgression = 0;
		numActive = 0;
		waveNumber = 0;
	}
	else if (numActive <= 0 && waveStates == reg_boss && theEnemies[bossIndex]->Alive == false)
	{
		waveStates = WaveStates(waveStates + 1);
		totalWave++;
		waveProgression = 0;
		waveNumber = 0;
		((Boss*)theEnemies[bossIndex])->activated = false;

		for (int i = 0; i < 8; i++)
		{
			particlePositions[i] = regularPositions[i];
			theEnemies[i]->Alive = false;
			theEnemies[i]->Spawn = false;
		}
		numActive = 0;
		blnZero = false; blnOne = true; blnTwo = false; blnThree = false;
	}
	else if (numActive <= 0 && waveNumber == waveLimit && waveStates == avg_boss && theEnemies[bossIndex]->Alive == true)
	{
		waveProgression = 0;
		numActive = 0;
		waveNumber = 0;
	}
	else if (numActive <= 0 && waveStates == avg_boss && theEnemies[bossIndex]->Alive == false)
	{
		waveStates = WaveStates(waveStates + 1);
		totalWave++;
		waveProgression = 0;
		numActive = 0;
		waveNumber = 0;
		((Boss*)theEnemies[bossIndex])->activated = false;
		blnZero = false; blnOne = true; blnTwo = false; blnThree = false;
	}
	else if (numActive <= 0 && waveNumber >= waveLimit && waveStates != final_boss && waveStates != reg_boss && waveStates != avg_boss)
	{
		//if all enemies of a wave are dead, progress
		waveTimer += game->deltaTime;
		if (waveTimer >= 1)
		{
			waveNumber = 0;
			spawnTime = 0;
			waveProgression++;
			totalWave++;
			waveTimer = 0;
			numActive = 0;
			if (waveLimit < 13)
			{
				waveLimit += 2;
			}
		}
	}
	else if (waveStates == final_boss)
	{
		waveTimer += game->deltaTime;
		if (waveTimer >= 1)
		{
			waveNumber = 0;
			spawnTime = 0;
			waveTimer = 0;
			numActive = 0;
		}
	}
	if (waveProgression >= 4 && waveStates != final_boss)
	{
		if (waveStates <7)
			waveStates = WaveStates(waveStates + 1);

		waveProgression = 0;
		totalWave++;

		if (waveStates == reg_boss || waveStates == avg_boss || waveStates == final_boss) //we know that this starts, but it doesn't pause...... for some reason....
		{
			blnZero = false; blnOne = false; blnTwo = true; blnThree = false;
		}
		else
		{
			blnZero = false; blnOne = true; blnTwo = false; blnThree = false;
		}
	}

	if (waveStates == final_boss && ((Boss*)theEnemies[bossIndex])->activated == true && ((Boss*)theEnemies[bossIndex])->Alive == false && ((Boss*)theEnemies[bossIndex])->Spawn == false)
	{
		blnZero = false; blnOne = false; blnTwo = false; blnThree = true;
		waveStates = null;
		filehandler.sortData();
		gameStates = endGame;
		win = true;
	}
}

void enemySpawning(int i)
{
	//for getting the enemies to actually spawn
	if (theEnemies[i]->Spawn == true && theEnemies[i]->Alive == false && (theEnemies[i]->spawnTimer > 5 && theEnemies[i]->tutorial == false || theEnemies[i]->blnTutorialCollision == true))
	{
		theEnemies[i]->Spawn = false;
		theEnemies[i]->Alive = true;
		theEnemies[i]->spawnTimer = 0;
		theEnemies[i]->blnTutorialCollision = false;
		if (theEnemies[i]->_enemyType == EnemyType(1))
		{
			((Avengers*)theEnemies[i])->initializeObject(enemy, phong);
			theEnemies[i]->setMaterial(&explTex);
			theEnemies[i]->damageTexture = &turretTip;
			theEnemies[i]->enemyTexture = &explTex;
			theEnemies[i]->setHealth(50);
		}
		else if (theEnemies[i]->_enemyType == EnemyType(2))
		{
			((Sprinkler*)theEnemies[i])->initializeObject(enemy, phong);
			theEnemies[i]->setMaterial(&purpleTexture);
			theEnemies[i]->damageTexture = &turretTip;
			theEnemies[i]->enemyTexture = &purpleTexture;
			theEnemies[i]->setHealth(50);
		}
		else if (theEnemies[i]->_enemyType == EnemyType(3))
		{
			((Boss*)theEnemies[i])->initializeObject(bossBody, phong);
			theEnemies[i]->setMaterial(&bossMat);
		}
		else if (theEnemies[i]->_enemyType == EnemyType(4))
		{
			((Revive*)theEnemies[i])->initializeObject(enemy, phong);
			((Revive*)theEnemies[i])->setMaterial(&turretTip);
			((Revive*)theEnemies[i])->damageTexture = &redTexture;
			((Revive*)theEnemies[i])->enemyTexture = &turretTip;
			theEnemies[i]->setHealth(50);
		}
		else
		{
			theEnemies[i]->initializeObject(enemy, phong);
			theEnemies[i]->setMaterial(&enemyTexture);
			theEnemies[i]->setHealth(30);
		}
	}

	if ((waveStates == reg_boss || waveStates == avg_boss || waveStates == final_boss) && ((Boss*)theEnemies[bossIndex])->activated == true && ((Boss*)theEnemies[bossIndex])->bossWeapons.size() > 0)
	{
		if (((Boss*)theEnemies[bossIndex])->bossWeapons[0]->Alive == false && ((Boss*)theEnemies[bossIndex])->bossWeapons[0]->Spawn == false)
		{
			((Boss*)theEnemies[bossIndex])->bossWeapons[0]->initialize(spawnMesh, &spawnMat, 0, true);
		}

		if (((Boss*)theEnemies[bossIndex])->bossWeapons[1]->Alive == false && ((Boss*)theEnemies[bossIndex])->bossWeapons[1]->Spawn == false)
		{
			((Boss*)theEnemies[bossIndex])->bossWeapons[1]->initialize(spawnMesh, &spawnMat, 0, true);
		}

		if (((Boss*)theEnemies[bossIndex])->bossWeapons[0]->Spawn == true && ((Boss*)theEnemies[bossIndex])->bossWeapons[0]->Alive == false && ((Boss*)theEnemies[bossIndex])->bossWeapons[0]->spawnTimer > 10)
		{
			((Boss*)theEnemies[bossIndex])->bossWeapons[0]->Spawn = false;
			((Boss*)theEnemies[bossIndex])->bossWeapons[0]->Alive = true;
			((Boss*)theEnemies[bossIndex])->bossWeapons[0]->setHealth(50);
			((Boss*)theEnemies[bossIndex])->bossWeapons[0]->initializeObject(enemy, phong);
			((Boss*)theEnemies[bossIndex])->bossWeapons[0]->setMaterial(&purpleTexture);
			((Boss*)theEnemies[bossIndex])->bossWeapons[0]->spawnTimer = 0;
		}

		if (((Boss*)theEnemies[bossIndex])->bossWeapons[1]->Spawn == true && ((Boss*)theEnemies[bossIndex])->bossWeapons[1]->Alive == false && ((Boss*)theEnemies[bossIndex])->bossWeapons[1]->spawnTimer > 10)
		{
			((Boss*)theEnemies[bossIndex])->bossWeapons[1]->Spawn = false;
			((Boss*)theEnemies[bossIndex])->bossWeapons[1]->Alive = true;
			((Boss*)theEnemies[bossIndex])->bossWeapons[1]->setHealth(50);
			((Boss*)theEnemies[bossIndex])->bossWeapons[1]->initializeObject(enemy, phong);
			((Boss*)theEnemies[bossIndex])->bossWeapons[1]->setMaterial(&purpleTexture);
			((Boss*)theEnemies[bossIndex])->bossWeapons[1]->spawnTimer = 0;
		}
	}
}

void enemyFunctions()
{
	//Michael Gharbaran's AI Optimization
	for (int i = 0; i < theEnemies.size(); i++)
	{
		int closestGridIndex = 0;
		float closestDistance = 100000;

		for (int j = 0; j < theGrid.size(); j++)
		{
			float closestCell = glm::distance((theGrid[j]), (theEnemies[i]->getPosition()));

			if (closestCell < closestDistance)
			{
				closestGridIndex = j;
				closestDistance = closestCell;
			}
		}
		theEnemies[i]->gridIndex = closestGridIndex;
	}

	for (int i = 0; i < theEnemies.size(); i++)
	{
		theEnemies[i]->updateBullets(game->deltaTime);
		theEnemies[i]->update(game->deltaTime, numActive, thePowerUp);

		if (theEnemies[i]->tutorial == true && theEnemies[i]->blnTutorialCollision == false)
		{
			theEnemies[i]->tutorialCollision(playerOne);
			theEnemies[i]->tutorialCollision(playerTwo);
		}

		enemySpawning(i);

		enemyShooting[i]->setPosition(theEnemies[i]->getPosition());
		enemyShooting[i]->update(game->deltaTime, *se->system, false);

		if (theEnemies[i]->firing == true)
		{
			enemyShooting[i]->channel->setPaused(false);
			se->system->playSound(enemyShooting[i]->sound, 0, false, &enemyShooting[i]->channel);
			enemyShooting[i]->channel->setVolume(0.7);
			theEnemies[i]->firing = false;
		}

		sep = theEnemies[i]->computeSeperation(theEnemies, i);
		arrival = theEnemies[i]->enemyArrive(); //storing all the values we need in temp variables

		if ((theEnemies[i]->_enemyType == EnemyType(0) || theEnemies[i]->_enemyType == EnemyType(2)))
		{
			if (theEnemies[i]->_enemyType == EnemyType(0))
			{
				((Grunt*)theEnemies[i])->collision(playerOne);
				((Grunt*)theEnemies[i])->collision(playerTwo);
			}
			else if (theEnemies[i]->_enemyType == EnemyType(2))
			{
				((Sprinkler*)theEnemies[i])->collision(playerOne);
				((Sprinkler*)theEnemies[i])->collision(playerTwo);
			}

			if (theEnemies[i]->Alive == true && theEnemies[i]->Spawn == false)
			{
				theEnemies[i]->calculateTarget(playerOne, playerTwo);
				theEnemies[i]->enemyAttack();

				if (waveStates != WaveStates(2))
				{
					theEnemies[i]->acceleration = ((2.5f * sep) + (1.5f * arrival));//we need to update the acceleration (Newton's law)
					theEnemies[i]->velocity = theEnemies[i]->velocity + (theEnemies[i]->acceleration * game->deltaTime); //we need to update velocity (Newton's law)

					newPosition = theEnemies[i]->getPosition() + (theEnemies[i]->velocity * game->deltaTime); //THEN we update position (again, newton's law)
					theEnemies[i]->setPosition(newPosition); //setting the new position lawd help me		
				}
			}
		}

		else if (theEnemies[i]->_enemyType == EnemyType(1))
		{
			((Avengers*)theEnemies[i])->collision(playerOne);
			((Avengers*)theEnemies[i])->collision(playerTwo);
			if (theEnemies[i]->Alive == true && theEnemies[i]->Spawn == false)
			{

				((Avengers*)theEnemies[i])->update(game->deltaTime, numActive);


				if (((Avengers*)theEnemies[i])->explSound == true)
				{
					se->system->playSound(enemyExplosions[i - avengerIndex]->sound, 0, false, &enemyExplosions[i - avengerIndex]->channel);
					enemyExplosions[i - avengerIndex]->setPosition(theEnemies[i]->getPosition());
					enemyExplosions[i - avengerIndex]->channel->setPaused(false);
					enemyExplosions[i - avengerIndex]->update(game->deltaTime, *se->system, false);
					enemyExplosions[i - avengerIndex]->setVolume(10);
					((Avengers*)theEnemies[i])->explSound = false;
				}

				theEnemies[i]->enemyAttack();
				theEnemies[i]->calculateTarget(playerOne, playerTwo);

				if (((Avengers*)theEnemies[i])->explosion == false)
				{
					theEnemies[i]->acceleration = ((2.5f * sep) + (1.5f * arrival));//we need to update the acceleration (Newton's law)
					theEnemies[i]->velocity = theEnemies[i]->velocity + (theEnemies[i]->acceleration * game->deltaTime); //we need to update velocity (Newton's law)

					newPosition = theEnemies[i]->getPosition() + (theEnemies[i]->velocity * game->deltaTime); //THEN we update position (again, newton's law)


					theEnemies[i]->setPosition(newPosition); //setting the new position lawd help me	
				}
				else
				{
					((Avengers*)theEnemies[i])->explode(theEnemies, numActive);
					particles[3]->initialPosition = theEnemies[i]->getPosition();
					explodePls = true;

				}
			}
		}
		//the boss
		else if (theEnemies[i]->Alive == true && theEnemies[i]->Spawn == false && theEnemies[i]->_enemyType == EnemyType(3))
		{
			particles[5]->update(game->deltaTime);
			((Boss*)theEnemies[i])->calculateTarget(playerOne, playerTwo);
			((Boss*)theEnemies[i])->enemyAttack();
			((Boss*)theEnemies[i])->collision(playerOne, bossStandardHP);
			((Boss*)theEnemies[i])->collision(playerTwo, bossStandardHP);
			((Boss*)theEnemies[i])->update(game->deltaTime, numActive, thePowerUp);
		}

		//the revive
		if (theEnemies[i]->Alive == true && theEnemies[i]->Spawn == false && theEnemies[i]->_enemyType == EnemyType(4))
		{
			((Revive*)theEnemies[i])->update(game->deltaTime, numActive);

			if (playerOne->getAlive() == false)
			{
				bool temp;
				if (tempIndex1 == 0)
					temp = ((Revive*)theEnemies[i])->collision(playerTwo, playerOne, numActive, sHeightI, 10);
				else if (tempIndex1 == 1)
					temp = ((Revive*)theEnemies[i])->collision(playerTwo, playerOne, numActive, fHeightI, 7);
				else if (tempIndex1 == 2)
					temp = ((Revive*)theEnemies[i])->collision(playerTwo, playerOne, numActive, hHeightI, 13);

				if (temp == true && playerOne->getAlive() == true)
				{
					sHeightI = 10; fHeightI = 7; hHeightI = 13;
					se->system->playSound(announcerDialogue[4]->sound, 0, false, &announcerDialogue[4]->channel);
				}
			}

			if (playerTwo->getAlive() == false)
			{
				bool temp;
				if (tempIndex2 == 0)
					temp = ((Revive*)theEnemies[i])->collision(playerOne, playerTwo, numActive, sHeightI2, 10);
				else if (tempIndex2 == 1)
					temp = ((Revive*)theEnemies[i])->collision(playerOne, playerTwo, numActive, fHeightI2, 7);
				else if (tempIndex2 == 2)
					temp = ((Revive*)theEnemies[i])->collision(playerOne, playerTwo, numActive, hHeightI2, 13);

				if (temp == true && playerTwo->getAlive() == true)
				{
					sHeightI2 = 10; fHeightI2 = 7; hHeightI2 = 13;
					se->system->playSound(announcerDialogue[4]->sound, 0, false, &announcerDialogue[4]->channel);
				}
			}

			theEnemies[i]->calculateTarget(playerOne, playerTwo);
			((Revive*)theEnemies[i])->enemyAttack();

			glm::vec3 sep = theEnemies[i]->computeSeperation(theEnemies, i);
			glm::vec3 arrival = theEnemies[i]->enemyArrive(); //storing all the values we need in temp variables

			theEnemies[i]->acceleration = ((2.5f * sep) + (1.5f * arrival));//we need to update the acceleration (Newton's law)
			theEnemies[i]->velocity = theEnemies[i]->velocity + (theEnemies[i]->acceleration * game->deltaTime); //we need to update velocity (Newton's law)

			glm::vec3 newPosition = theEnemies[i]->getPosition() + (theEnemies[i]->velocity * game->deltaTime); //THEN we update position (again, newton's law)
			theEnemies[i]->setPosition(newPosition); //setting the new position lawd help me		
		}

		particles[3]->update(game->deltaTime);

		if (explodePls == true)
		{
			particles[3]->doExplode = false;
			particles[3]->reset = false;
			expTime += game->deltaTime;
			if (expTime >= 1)
			{
				particles[3]->doExplode = true;
			}
			if (expTime >= 10)
			{
				//TO DO: set doExplode back to true
				//find a way to properly reset
				//get position set back to explosive enemy
				explodePls = false;
				expTime = 0;
			}
		}
		else if (explodePls == false)
		{
			expTime = 0;
			particles[3]->reset = true;
		}
	}
}

void controllerUpdates()
{
	controller.DownloadPackets(2);

	if (gameStates == menu)
	{
		controller.GetSticks(0, lStick, rStick);

		if (controller.GetButton(0, Input::Button::A) && pressed[0] == UnPressed)
		{
			pressed[0] = A;
			if (selection == 1)
			{
				exit(1);
			}
			else if (selection == 0)
			{
				tutorial->setMaterial(&readyMat1);
				gameStates = charCreation;
				se->setListenerPosition(glm::vec3(0, 0, 0));
				charTemp = 1;
			}
		}
		else if (!controller.GetButton(0, Input::Button::A) && pressed[0] == A)
		{
			pressed[0] = UnPressed;
		}

		if (controller.GetButton(0, Input::Button::DPadDown) || lStick.yAxis < 0)
		{
			selection = 1;
		}
		else if (controller.GetButton(0, Input::Button::DPadUp) || lStick.yAxis > 0)
		{
			selection = 0;
		}
	}

	if (gameStates == charCreation)
	{
		for (int i = 0; i < 4; i++)
		{
			controller.GetTriggers(i, trig[i].left, trig[i].right);
		}


		if (readyP1 != true)
		{

			if (trig[0].left >= 0.9f && pressed[0] != LT)
			{
				--tempIndex1;
				if (tempIndex1 < 0)
				{
					tempIndex1 = 2;
				}
				tempPlayer1->Object = bodies[tempIndex1];
				tempPlayer1->setMaterial(PlayerTextures1[tempIndex1]);
				player_body1->setMaterial(charBodiesTexture[tempIndex1]);
				pressed[0] = LT;
				se->system->playSound(menuSFX[0]->sound, 0, false, &menuSFX[0]->channel);
			}
			else if (trig[0].left <= 0.0f && pressed[0] == LT)
			{
				pressed[0] = UnPressed;
			}

			if (trig[0].right >= 0.9f && pressed[0] != RT)
			{
				++tempIndex1;
				if (tempIndex1 > 2)
				{
					tempIndex1 = 0;
				}
				tempPlayer1->Object = bodies[tempIndex1];
				tempPlayer1->setMaterial(PlayerTextures1[tempIndex1]);
				player_body1->setMaterial(charBodiesTexture[tempIndex1]);
				se->system->playSound(menuSFX[0]->sound, 0, false, &menuSFX[0]->channel);
				pressed[0] = RT;
			}
			else if (trig[0].right == 0.0f && pressed[0] == RT)
			{
				pressed[0] = UnPressed;
			}

			if (controller.GetButton(0, Input::Button::LB) && pressed[0] != LB)
			{
				--tempTurretI1;
				if (tempTurretI1 < 0)
				{
					tempTurretI1 = 2;
				}
				tempTurret1->Object = turrets[tempTurretI1];
				tempTurret1->setMaterial(PlayerTurretTex1[tempTurretI1]);
				player_cust1->setMaterial(charTurretTexture[tempTurretI1]);
				pressed[0] = LB;
				se->system->playSound(menuSFX[0]->sound, 0, false, &menuSFX[0]->channel);
			}
			else if (!controller.GetButton(0, Input::Button::LB) && pressed[0] == LB)
			{
				pressed[0] = UnPressed;
			}

			if (controller.GetButton(0, Input::Button::RB) && pressed[0] != RB)
			{
				++tempTurretI1;
				if (tempTurretI1 > 2)
				{
					tempTurretI1 = 0;
				}
				tempTurret1->Object = turrets[tempTurretI1];
				tempTurret1->setMaterial(PlayerTurretTex1[tempTurretI1]);
				player_cust1->setMaterial(charTurretTexture[tempTurretI1]);
				pressed[0] = RB;
				se->system->playSound(menuSFX[0]->sound, 0, false, &menuSFX[0]->channel);
			}
			else if (!controller.GetButton(0, Input::Button::RB) && pressed[0] == RB)
			{
				pressed[0] = UnPressed;
			}
		}

		if (readyP2 != true)
		{
			if (trig[1].left > +0.95f && pressed[1] != LT)
			{
				--tempIndex2;
				if (tempIndex2 < 0)
				{
					tempIndex2 = 2;
				}
				tempPlayer2->Object = bodies[tempIndex2];
				tempPlayer2->setMaterial(PlayerTextures2[tempIndex2]);
				player_body2->setMaterial(charBodiesTexture[tempIndex2]);
				pressed[1] = LT;
				se->system->playSound(menuSFX[1]->sound, 0, false, &menuSFX[1]->channel);
			}
			else if (trig[1].left == 0.f && pressed[1] == LT)
			{
				pressed[1] = UnPressed;
			}

			if (trig[1].right >= 0.9f && pressed[1] != RT)
			{
				++tempIndex2;
				if (tempIndex2 > 2)
				{
					tempIndex2 = 0;
				}
				tempPlayer2->Object = bodies[tempIndex2];
				tempPlayer2->setMaterial(PlayerTextures2[tempIndex2]);
				player_body2->setMaterial(charBodiesTexture[tempIndex2]);
				pressed[1] = RT;
				se->system->playSound(menuSFX[1]->sound, 0, false, &menuSFX[1]->channel);
			}
			else if (trig[1].right == 0.f && pressed[1] == RT)
			{
				pressed[1] = UnPressed;
			}

			if (controller.GetButton(1, Input::Button::LB) && pressed[1] != LB)
			{
				--tempTurretI2;
				if (tempTurretI2 < 0)
				{
					tempTurretI2 = 2;
				}
				tempTurret2->Object = turrets[tempTurretI2];
				tempTurret2->setMaterial(PlayerTurretTex2[tempTurretI2]);
				player_cust2->setMaterial(charTurretTexture[3 + tempTurretI2]);
				pressed[1] = LB;
				se->system->playSound(menuSFX[1]->sound, 0, false, &menuSFX[1]->channel);
			}
			else if (!controller.GetButton(1, Input::Button::LB) && pressed[1] == LB)
			{
				pressed[1] = UnPressed;
			}

			if (controller.GetButton(1, Input::Button::RB) && pressed[1] != RB)
			{
				++tempTurretI2;
				if (tempTurretI2 > 2)
				{
					tempTurretI2 = 0;
				}
				tempTurret2->Object = turrets[tempTurretI2];
				tempTurret2->setMaterial(PlayerTurretTex2[tempTurretI2]);
				player_cust2->setMaterial(charTurretTexture[3 + tempTurretI2]);
				pressed[1] = RB;
				se->system->playSound(menuSFX[1]->sound, 0, false, &menuSFX[1]->channel);
			}
			else if (!controller.GetButton(1, Input::Button::RB) && pressed[1] == RB)
			{
				pressed[1] = UnPressed;
			}
		}

		if (controller.GetButton(0, Input::Button::A) && pressed[0] != A)
		{
			c_bodies = CustomizationType(tempIndex1);
			c_turrets = CustomizationType(tempTurretI1);
			playerOne->loadMeshes(bodies[tempIndex1], turrets[tempTurretI1], c_bodies, c_turrets, phong, PlayerTextures1[tempIndex1], PlayerTurretTex1[tempTurretI1]);

			progression++;
			pressed[0] = A;

			if (readyP1 == false)
				se->system->playSound(announcerDialogue[1]->sound, 0, false, &announcerDialogue[1]->channel);

			readyP1 = true;
		}
		if (controller.GetButton(1, Input::Button::A) && pressed[1] != A)
		{
			c_bodies = CustomizationType(tempIndex2);
			c_turrets = CustomizationType(tempTurretI2);
			playerTwo->loadMeshes(bodies[tempIndex2], turrets[tempTurretI2], c_bodies, c_turrets, phong, PlayerTextures2[tempIndex2], PlayerTurretTex2[tempTurretI2]);

			progression++;
			pressed[1] = A;
			if (readyP2 == false)
				se->system->playSound(announcerDialogue[2]->sound, 0, false, &announcerDialogue[2]->channel);

			readyP2 = true;
		}

		if (controller.GetButton(0, Input::Button::B) && pressed[0] != B && readyP1 == true && progression != 3)
		{
			progression--;
			pressed[0] = B;
			readyP1 = false;
		}
		if (controller.GetButton(1, Input::Button::B) && pressed[1] != B && readyP2 == true && progression != 3)
		{
			progression--;
			pressed[1] = B;
			readyP2 = false;
		}

		if (controller.GetButton(0, Input::Button::A) == false && readyP1 == true)
		{
			pressed[0] = UnPressed;
		}
		else if (controller.GetButton(1, Input::Button::A) == false && readyP2 == true)
		{
			pressed[1] = UnPressed;
		}

		if (readyP1)
		{
			ready1->update(game->deltaTime);
			ready1->setRotation(glm::vec3(0, 180, 90));
		}
		if (readyP2)
		{
			ready2->update(game->deltaTime);
			ready1->setRotation(glm::vec3(0, 180, 90));
		}

		if (readyP1 == true && readyP2 == true && (pressed[0] == A && pressed[1] == A))
		{
			progression = 3;
			pressed[0] = UnPressed;
			pressed[1] = UnPressed;
		}

		if ((controller.GetButton(0, Input::Button::A) || controller.GetButton(1, Input::Button::A)) && readyP1 == true && readyP2 == true && progression >= 3)
		{
			c_bodies = CustomizationType(tempIndex2);
			c_turrets = CustomizationType(tempTurretI2);
			playerTwo->loadMeshes(bodies[tempIndex2], turrets[tempTurretI2], c_bodies, c_turrets, phong, PlayerTextures2[tempIndex2], PlayerTurretTex2[tempTurretI2]);

			progression = 0;
			pressed[0] = UnPressed;
			pressed[1] = UnPressed;
			readyP1 = false;
			readyP2 = false;

			tutorialInitialize();
			blnZero = 0; blnOne = true; blnTwo = false; blnThree = false;

			gameStates = level1;
			tutorial->setMaterial(&tutorialMat);
		}

	}

	if (gameStates == level1)
	{
		if (controller.GetButton(0, Input::Button::A) && pressed[0] == UnPressed)
		{
			progression++;
			pressed[0] = A;
		}
		if (controller.GetButton(1, Input::Button::A) && pressed[1] == UnPressed)
		{
			progression++;
			pressed[1] = A;
		}

		if (progression >= 2)
		{
			pressed[0] = UnPressed;
			pressed[1] = UnPressed;

			tutorialDeinitialize();
			gameStates = level2;
		}
	}

	if (gamePause == false)
	{
		if (!controller.GetConnected(0))
		{
			//printf("Controller 1 not connected");
		}
		else
		{
			if (gameStates != menu && gameStates != splashScreen)
			{
				if (playerOne->getAlive() == true)
					playerOne->update(game->deltaTime, controller, gameStates);
			}
		}

		if (!controller.GetConnected(1))
		{
			//printf("Controller 2 not connected");
		}
		else
		{
			if (gameStates != menu)
			{
				if (playerTwo->getAlive() == true && gameStates != splashScreen)
					playerTwo->update(game->deltaTime, controller, gameStates);
			}
		}
	}

	if (gamePause == true)
	{
		if (controller.GetButton(0, Input::Button::A) && pressed[0] == UnPressed)
		{
			pressed[0] = A;
			pauseObject->setMaterial(&credits);
		}
		else if (controller.GetButton(0, Input::Button::B) && pressed[0] == A)
		{
			pressed[0] = UnPressed;
			pauseObject->setMaterial(&pauseMat);
		}
	}

	if (controller.GetButton(0, Input::Button::Start) && pressed[0] == UnPressed)
	{
		pressed[0] = Start;
	}
	else if (!controller.GetButton(0, Input::Button::Start) && pressed[0] == Start)
	{
		gamePause = !gamePause;
		pressed[0] = UnPressed;
		pauseObject->setMaterial(&pauseMat);
	}

	if (controller.GetButton(1, Input::Button::Start) && pressed[1] == UnPressed)
	{
		pressed[1] = Start;
	}
	else if (!controller.GetButton(1, Input::Button::Start) && pressed[1] == Start)
	{
		gamePause = !gamePause;
		pressed[0] = UnPressed;
	}

	if (controller.GetButton(0, Input::Button::Select))
	{
		gameStates = menu;
		playerOne->reset(glm::vec3(-15, 0, 0));
		playerTwo->reset(glm::vec3(15, 0, 0));
		restart();
	}

	if (gameStates == endGame)
	{
		if (!win)
		{
			if (controller.GetButton(0, Input::Button::A) && pressed[0] == UnPressed)
			{
				pressed[0] = A;
			}
			else if (!controller.GetButton(0, Input::Button::A) && pressed[0] == A)
			{
				gameStates = menu;
				playerOne->reset(glm::vec3(-15, 0, 0));
				playerTwo->reset(glm::vec3(15, 0, 0));
				restart();
				pressed[0] = UnPressed;
			}

			if (controller.GetButton(0, Input::Button::B) && pressed[0] == UnPressed)
			{
				glutExit();
			}
		}
		else
		{
			if (controller.GetButton(0, Input::Button::A) && pressed[0] == UnPressed)
			{
				pressed[0] = A;
			}
			else if (!controller.GetButton(0, Input::Button::A) && pressed[0] == A)
			{
				gameStates = leaderBoard;
				pressed[0] = UnPressed;
			}
		}
	}
}

void updateUI()
{
	if (gameStates != menu)
	{
		if (playerOne->getAlive() == true && playerTwo->getAlive() == false && playerOne->scoreMultiplayer == 1)
		{
			overlayObject->setMaterial(overlayTexture[2]);
		}
		else if (playerOne->getAlive() == false && playerTwo->getAlive() == true && playerTwo->scoreMultiplayer == 1)
		{
			overlayObject->setMaterial(overlayTexture[1]);
		}
		else if (playerOne->getAlive() == true && playerTwo->getAlive() == true && playerOne->scoreMultiplayer > 1 && playerTwo->scoreMultiplayer == 1)
		{
			overlayObject->setMaterial(overlayTexture[3]); //balive p1score
		}
		else if (playerOne->getAlive() == true && playerTwo->getAlive() == true && playerTwo->scoreMultiplayer > 1 && playerOne->scoreMultiplayer == 1)
		{
			overlayObject->setMaterial(overlayTexture[4]); //balive p2score 
		}
		else if (playerOne->getAlive() == true && playerTwo->getAlive() == true && playerOne->scoreMultiplayer > 1 && playerTwo->scoreMultiplayer > 1)
		{
			overlayObject->setMaterial(overlayTexture[5]); //balive p1score p2score
		}
		else if (playerOne->getAlive() == true && playerTwo->getAlive() == false && playerOne->scoreMultiplayer > 1)
		{
			overlayObject->setMaterial(overlayTexture[7]); //p2ded p1score
		}
		else if (playerOne->getAlive() == false && playerTwo->getAlive() == true && playerTwo->scoreMultiplayer > 1)
		{
			overlayObject->setMaterial(overlayTexture[6]); //p1ded p2score
		}
		else
		{
			overlayObject->setMaterial(overlayTexture[0]);
		}
		overlayObject->update(game->deltaTime);
		overlayObject->setSpecificScale(86, 86, 0.01f);
		overlayObject->setRotation(glm::vec3(0));
	}

	if (gameStates == leaderBoard)
	{
		leaderboard->update(game->deltaTime);
	}

	if (gameStates == charCreation)
	{
		player_cust1->update(game->deltaTime);
		player_cust2->update(game->deltaTime);
		player_body1->update(game->deltaTime);
		player_body2->update(game->deltaTime);

		tempPlayer1->update(game->deltaTime);
		tempPlayer2->update(game->deltaTime);
		tempTurret1->update(game->deltaTime);
		tempTurret2->update(game->deltaTime);
	}

	if (gameStates == menu)
	{
		start->update(game->deltaTime);
		quit->update(game->deltaTime);
		onslaughtLogo->update(game->deltaTime);

		if (selection == 0)
		{
			start->setSpecificScale(30.f, 30.f, 0.01f);
			quit->setSpecificScale(30.f, 30.f, 0.01f);
			onslaughtLogo->setSpecificScale(70.f, 70.f, 0.01f);
		}
		else if (selection == 1)
		{
			start->setSpecificScale(30.f, 30.f, 0.01f);
			quit->setSpecificScale(30.f, 30.f, 0.01f);
			onslaughtLogo->setSpecificScale(70.f, 70.f, 0.01f);
		}
	}
	if (gameStates != menu)
	{
		overlayObject->update(game->deltaTime);
		overlayObject->setSpecificScale(173, 176, 0.01f);
		overlayObject->setRotation(glm::vec3(0, 180, 0));

		SMP1->update(game->deltaTime);
		SMP1->setRotationAngleY(180.f);

		SMP2->update(game->deltaTime);
		SMP2->setRotationAngleY(180.f);

	}
	if (gameStates == level1)
	{
		tutorial->update(game->deltaTime);
	}
	if (gamePause == false)
	{
		if (playerOne->getAlive())
		{
			if (playerOne->hit) //it animates while the player is hit
			{
				if (tempIndex1 == 0)
				{
					player1StandardHP->setMaterial(&redTexture);
					if (sHeightI > 0) sHeightI--;
				}
				else if (tempIndex1 == 1)
				{
					player1FastHP->setMaterial(&redTexture);
					if (fHeightI > 0) fHeightI--;
				}
				else if (tempIndex1 == 2)
				{
					player1HeavyHP->setMaterial(&redTexture);
					if (hHeightI > 0) hHeightI--;
				}
				playerOne->hit = false;
			}
			if (tempIndex1 == 0)
			{
				player1StandardHP->setSpecificScale(250.0f, standardHeights[sHeightI], 0.02f);
			}
			if (tempIndex1 == 1)
			{
				player1FastHP->setSpecificScale(250.0f, fastHeights[fHeightI], 0.02f);
			}
			else if (tempIndex1 == 2)
			{
				player1HeavyHP->setSpecificScale(250.0f, heavyHeights[hHeightI], 0.02f);
			}
		}
		else
		{
			player1StandardHP->setSpecificScale(250.0f, 0.f, 0.02f);
			player1FastHP->setSpecificScale(250.0f, 0.f, 0.02f);
			player1HeavyHP->setSpecificScale(250.0f, 0.f, 0.02f);
			sHeightI = 0;
			fHeightI = 0;
			hHeightI = 0;
		}

		if (playerTwo->getAlive())
		{
			if (playerTwo->hit)
			{
				if (tempIndex2 == 0)
				{
					player2StandardHP->setMaterial(&redTexture);
					if (sHeightI2 > 0)	sHeightI2--;
				}
				else if (tempIndex2 == 1)
				{
					player2FastHP->setMaterial(&redTexture);
					if (fHeightI2 > 0) fHeightI2--;
				}
				else if (tempIndex2 == 2)
				{
					player2HeavyHP->setMaterial(&redTexture);
					if (hHeightI2 > 0) hHeightI2--;
				}

				//healthDecrease2 += 1.05;

				playerTwo->hit = false;
			}
			if (tempIndex2 == 0)
			{
				player2StandardHP->setSpecificScale(250.0f, standardHeights[sHeightI2], 0.02f);

			}
			if (tempIndex2 == 1)
			{
				player2FastHP->setSpecificScale(250.0f, fastHeights[fHeightI2], 0.02f);
			}
			else if (tempIndex2 == 2)
			{
				player2HeavyHP->setSpecificScale(250.0f, heavyHeights[hHeightI2], 0.02f);
			}
		}
		else
		{
			player2StandardHP->setSpecificScale(250.0f, 0.f, 0.02f);
			player2FastHP->setSpecificScale(250.0f, 0.f, 0.02f);
			player2HeavyHP->setSpecificScale(250.0f, 0.f, 0.02f);
			sHeightI2 = 0;
			fHeightI2 = 0;
			hHeightI2 = 0;
		}
	}
	if (gameStates == level1 || gameStates == charCreation)
	{
		tutorial->update(game->deltaTime);
	}


}

void updateScene()
{
	se->update();
	if (gameStates == menu)
	{
		playerOne->menuUpdate(game->deltaTime);
		backgroundMusic[0]->update(game->deltaTime, *se->system, true);
		backgroundMusic[0]->setPosition(playerOne->getPosition());
	}
	if (gameStates == charCreation)
	{
		if (readyP1 == true)
		{
			announcerDialogue[1]->channel->setPaused(false);
			announcerDialogue[1]->update(game->deltaTime, *se->system, true);
		}
		else if (readyP2 == true)
		{
			announcerDialogue[2]->channel->setPaused(false);
			announcerDialogue[2]->update(game->deltaTime, *se->system, true);
		}
	}
	if (gameStates == level2)
	{
		if (thePowerUp->playSound == true)
		{
			int type = thePowerUp->_type;
			se->system->playSound(powerUps[type]->sound, 0, false, &powerUps[type]->channel);
			thePowerUp->playSound = false;
		}

		announcerDialogue[0]->channel->setPaused(false);
		announcerDialogue[0]->update(game->deltaTime, *se->system, true);

		announcerDialogue[4]->update(game->deltaTime, *se->system, false);
		announcerDialogue[3]->update(game->deltaTime, *se->system, false);
		announcerDialogue[5]->update(game->deltaTime, *se->system, false);

	}
	if (gameStates == level2 || gameStates == level1)
	{
		if (playerOne->firing == true)
		{
			pFiring[tempTurretI1]->channel->setPaused(false);
			pFiring[tempTurretI1]->setVolume(0.9);
			pFiring[tempTurretI1]->update(game->deltaTime, *se->system, false);
			pFiring[tempTurretI1]->setPosition(playerOne->getPosition());
		}
		else
		{
			pFiring[tempTurretI1]->channel->setPaused(true);
		}

		if (playerTwo->firing == true)
		{
			pFiring2[tempTurretI2]->channel->setPaused(false);
			pFiring2[tempTurretI2]->setVolume(0.9);
			pFiring2[tempTurretI2]->update(game->deltaTime, *se->system, false);
			pFiring2[tempTurretI2]->setPosition(playerTwo->getPosition());
		}
		else
		{
			pFiring2[tempTurretI2]->channel->setPaused(true);
		}
	}

	backgroundMusic[0]->channel->setPaused(!blnZero);
	backgroundMusic[1]->channel->setPaused(!blnOne);
	backgroundMusic[2]->channel->setPaused(!blnTwo);
	backgroundMusic[3]->channel->setPaused(!blnThree);

	backgroundMusic[0]->update(game->deltaTime, *se->system, true);
	backgroundMusic[1]->update(game->deltaTime, *se->system, true);
	backgroundMusic[2]->update(game->deltaTime, *se->system, true);
	backgroundMusic[3]->update(game->deltaTime, *se->system, true);

}

//this is where everything gets updated, good stuff
void timerCallbackFunc(int value)
{
	if (gameStates == splashScreen)
	{
		loadGame();
		gameStates = menu;
		blnZero = true; blnOne = false; blnTwo = false; blnThree = false;
	}

	controllerUpdates();

	if (waveStates == 2)
	{
		particlePositions[0] = (glm::vec3(9.5, 9, 0));
		particlePositions[1] = glm::vec3(-9.5, -9, 0);
		particlePositions[2] = glm::vec3(-9.5, 9, 0);
		particlePositions[3] = glm::vec3(9.5, -9, 0);
		particlePositions[4] = glm::vec3(0, 12.5, 0);
		particlePositions[5] = glm::vec3(0, -12.5, 0);
		particlePositions[6] = glm::vec3(-12.5, 0, 0);
		particlePositions[7] = glm::vec3(12.5, 0, 0);
	}

	angle += 5;

	particleIndex = 0;
	for (int i = 0; i < 500; i++)
	{
		if (i == 62 || i == 124 || i == 186 || i == 248 || i == 310 || i == 372 || i == 434 || i == 500)
			++particleIndex;
		if (particleIndex > 7) particleIndex = 0;
		particlesVec[i]->initialPosition = glm::vec3(particlePositions[particleIndex].x + cos(angle * 3.14 / 180), particlePositions[particleIndex].y + sin(angle * 3.14 / 180), 0);
		particlesVec[i]->updateSpawn(game->deltaTime);
		particlesVec2[i]->initialPosition = glm::vec3(particlePositions[particleIndex].x + cos(angle * 3.14 / 180), particlePositions[particleIndex].y + sin(angle * 3.14 / 180), 0);
		particlesVec2[i]->updateSpawn(game->deltaTime);
		particlesVec3[i]->initialPosition = glm::vec3(particlePositions[particleIndex].x + cos(angle * 3.14 / 180), particlePositions[particleIndex].y + sin(angle * 3.14 / 180), 0);
		particlesVec3[i]->updateSpawn(game->deltaTime);
		particlesVec5[i]->initialPosition = glm::vec3(theEnemies[reviveIndex]->getPosition().x + 2 * cos(angle * 3.14 / 180), theEnemies[reviveIndex]->getPosition().y + 2 * sin(angle * 3.14 / 180), 0);
		particlesVec5[i]->updateSpawn(game->deltaTime);
		particlesVec6[i]->initialPosition = glm::vec3(theEnemies[bossIndex]->getPosition().x + 5 * cos(angle * 3.14 / 180), theEnemies[bossIndex]->getPosition().y + 5 * sin(angle * 3.14 / 180), 0);
		particlesVec6[i]->updateSpawn(game->deltaTime);
	}

	if (gamePause == false)
	{
		if (gameStates == level2)
		{
			powerUpCalculations();
			waveManagement();
		}

		player1Pos = playerOne->getPosition();
		player2Pos = playerTwo->getPosition();

		if ((gameStates == level2 || gameStates == level1) && (playerOne->getAlive() == true || playerTwo->getAlive() == true))
		{
			enemyFunctions();

			if (thePowerUp->Alive == true && gameStates != level1)
			{
				thePowerUp->update(game->deltaTime);

				if (tempIndex1 == 0)
					thePowerUp->Collision(playerOne, sHeightI, 10);

				if (tempIndex1 == 1)
					thePowerUp->Collision(playerOne, fHeightI, 7);

				if (tempIndex1 == 2)
					thePowerUp->Collision(playerOne, hHeightI, 13);

				if (tempIndex2 == 0)
					thePowerUp->Collision(playerTwo, sHeightI2, 10);

				if (tempIndex2 == 1)
					thePowerUp->Collision(playerTwo, fHeightI2, 7);

				if (tempIndex2 == 2)
					thePowerUp->Collision(playerTwo, hHeightI2, 13);
			}
		}

		if (gameStates == level1)
		{
			tutEnd = true;
			numActive = 1;
			tutorialInitialize();
		}

		if (playerOne->getAlive() == false && playerTwo->getAlive() == false)
		{
			filehandler.sortData();
			blnZero = false; blnOne = false; blnTwo = false; blnThree = true;
			gameStates = endGame;
			pressed[0] = UnPressed;
			pressed[1] = UnPressed;
			if (blnGameOver == false)
			{
				announcerDialogue[5]->channel->setPaused(false);
				se->system->playSound(announcerDialogue[5]->sound, 0, false, &announcerDialogue[5]->channel);
				blnGameOver = true;
			}
			win = false;
		}

		if (playerOne->dTimer > 0) //CLEAN THIS LATER
		{
			playerOne->dTimer -= game->deltaTime;
		}
		else if (playerOne->dTimer <= 0)
		{
			playerOne->dTimer = 0;
			playerOne->setMaterial(playerOne->normal);

			if (tempIndex1 == 0)
				player1StandardHP->setMaterial(&blue);
			else if (tempIndex1 == 1)
				player1FastHP->setMaterial(&blue);
			else if (tempIndex1 == 2)
				player1HeavyHP->setMaterial(&blue);
		}

		if (playerTwo->dTimer > 0)
		{
			playerTwo->dTimer -= game->deltaTime;
		}
		else if (playerTwo->dTimer <= 0)
		{
			playerTwo->dTimer = 0;
			playerTwo->setMaterial(playerTwo->normal);

			if (tempIndex2 == 0)
				player2StandardHP->setMaterial(&green);
			else if (tempIndex2 == 1)
				player2FastHP->setMaterial(&green);
			else if (tempIndex2 == 2)
				player2HeavyHP->setMaterial(&green);
		}

		playerOne->updateBullets(game->deltaTime);
		playerTwo->updateBullets(game->deltaTime);

		if (game->collisionDetection(playerOne, arena1))
		{
			playerOne->GameObject::setPosition(player1Pos);
			playerOne->velocity = glm::vec3(0);
			//printf("is this working?");
			bounceBack = true;
		}
		else
		{
			//playerOne->velocity = glm::vec3(12.5f, 0/f, 0.f);
		}

		if (bounceBack)
		{
			glm::vec3 targetVector = (glm::vec3(0.f, 0.f, 0.f) - playerOne->getPosition());
			float distance = glm::length(targetVector); //the distance is for arrive
			targetVector = glm::normalize(targetVector);

			glm::vec3 steering = targetVector - playerOne->velocity;
			steering *= 50.f;

			//the mass affects how it moves
			steering = steering / 1.0f;
			playerOne->velocity = playerOne->velocity + (steering * (game->deltaTime));
			printf("%f\n", playerOne->velocity.x);
			glm::vec3 newPosition = playerOne->getPosition() + (playerOne->velocity * (game->deltaTime));
			newPosition.z = 0.f;
			playerOne->GameObject::setPosition(newPosition);
			//printf("is this working?");
			bounceTime += game->deltaTime * 5;
			if (bounceTime > 0.5)
			{
				bounceBack = false;
				bounceTime = 0;
				if (playerOne->isMoving)
					playerOne->velocity = glm::vec3(6.0, 6.0, 0);
			}
		}

		if (game->collisionDetection(playerTwo, arena1))
		{
			playerTwo->GameObject::setPosition(player2Pos);
			playerTwo->velocity = glm::vec3(0);
			bounceBack2 = true;
		}
		else
		{
			//	playerTwo->velocity = glm::vec3(12.5f, 12.5f, 0.f);
		}

		if (bounceBack2)
		{
			glm::vec3 targetVector = (glm::vec3(0.f, 0.f, 0.f) - playerTwo->getPosition());
			float distance = glm::length(targetVector); //the distance is for arrive
			targetVector = glm::normalize(targetVector);

			glm::vec3 steering = targetVector - playerTwo->velocity;
			steering *= 50.f;

			//the mass affects how it moves
			steering = steering / 1.0f;
			playerTwo->velocity = playerTwo->velocity + (steering * (game->deltaTime));
			glm::vec3 newPosition = playerTwo->getPosition() + (playerTwo->velocity * (game->deltaTime));
			newPosition.z = 0.f;
			playerTwo->GameObject::setPosition(newPosition);
			bounceTime2 += game->deltaTime * 5;
			if (bounceTime2 > 0.5)
			{
				bounceBack2 = false;
				bounceTime2 = 0;
				if (playerTwo->isMoving)
					playerTwo->velocity = glm::vec3(6.0, 6.0, 0);
			}
		}

		if (gameStates != 1)
		{
			player1StandardHP->update(game->deltaTime);
			player1FastHP->update(game->deltaTime);
			player1HeavyHP->update(game->deltaTime);
			player1HPEmpty->update(value);
			player1HPEmpty->setSpecificScale(250.0f, 150.f, 0.01f);

			player2StandardHP->update(game->deltaTime);
			player2FastHP->update(game->deltaTime);
			player2HeavyHP->update(game->deltaTime);
			player2HPEmpty->update(value);
			player2HPEmpty->setSpecificScale(250.0f, 150.f, 0.01f);

			if (waveStates == final_boss || waveStates == reg_boss || waveStates == avg_boss)
			{
				bossStandardHP->update(value);
			}
		}

		if (gameStates == endGame)
		{
			pFiring[tempTurretI1]->channel->setPaused(true);
			pFiring2[tempTurretI2]->channel->setPaused(true);
			if (win == false)
			{
				gameOver->update(game->deltaTime);
			}
			else if (win == true)
			{
				winScreen->update(game->deltaTime);
				winScreen->setRotation(glm::vec3(0, 180.f, 0));
			}
		}
	}
	else if (gamePause == true)
	{
		if (gameStates != menu && gameStates != charCreation)
		{
			pauseObject->update(value);
		}
	}

	arena1->update(game->deltaTime);
	updateScene();
	updateUI();
	game->update();

	glutPostRedisplay();
	glutTimerFunc(FRAME_DELAY, timerCallbackFunc, 0);
}

int main(int argc, char **argv)
{
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInit(&argc, argv);
	glutInitContextVersion(4, 2);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutCreateWindow("ONSLAUGHT");
	glutFullScreenToggle();
	glutSetCursor(GLUT_CURSOR_NONE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#ifndef _DEBUG
	//ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif

	glewExperimental = true;

	if (glewInit() != GLEW_OK)
	{
		printf("Glew did not initialize\n");
		system("pause");
		return 0;
	}

	printf("OpenGL version %s", glGetString(GL_VERSION));

	//set up of callbacks
	glutDisplayFunc(DisplayCallbackFunction);
	glutKeyboardFunc(keyboardCallbackFunc);
	glutKeyboardUpFunc(keyboardUpCallbackFunc);
	glutTimerFunc(1, timerCallbackFunc, 0);

	Light light1, light2, light3, light4, light5, light6, light7, light8;
	light1.positionOrDirection = glm::vec4(16.f, 22.f, 2.f, 1.f);
	light1.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light1.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light1.specular = glm::vec3(0.f, 1.f, 1.f);
	light1.specularExponent = 250.f;
	light1.constantAttenuation = 1.f;
	light1.linearAttenuation = 0.1f;
	light1.quadraticAttenuation = 0.0001f;

	light2.positionOrDirection = glm::vec4(-22.f, -22.f, 2.f, 1.f);
	light2.positionOrDirection = glm::vec4(0.f, 4.f, 2.f, 1.f);
	light2.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light2.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light2.specular = glm::vec3(0.f, 1.f, 1.f);
	light2.specularExponent = 250.f;
	light2.constantAttenuation = 1.f;
	light2.linearAttenuation = 0.1f;
	light2.quadraticAttenuation = 0.001f;

	light3.positionOrDirection = glm::vec4(24.f, -24.f, 2.f, 1.f);
	light3.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light3.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light3.specular = glm::vec3(0.f, 1.f, 1.f);
	light3.specularExponent = 250.f;
	light3.constantAttenuation = 1.f;
	light3.linearAttenuation = 0.1f;
	light3.quadraticAttenuation = 0.0001f;

	light4.positionOrDirection = glm::vec4(-18.f, 14.f, 2.f, 1.f);
	light4.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light4.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light4.specular = glm::vec3(0.f, 1.f, 1.f);
	light4.specularExponent = 250.f;
	light4.constantAttenuation = 1.f;
	light4.linearAttenuation = 0.1f;
	light4.quadraticAttenuation = 0.0001f;

	light5.positionOrDirection = glm::vec4(0.f, -30.f, 2.f, 1.f);
	light5.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light5.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light5.specular = glm::vec3(0.f, 1.f, 1.f);
	light5.specularExponent = 150.f;
	light5.constantAttenuation = 1.f;
	light5.linearAttenuation = 0.1f;
	light5.quadraticAttenuation = 0.2f;

	light6.positionOrDirection = glm::vec4(-31.f, -4.f, 2.f, 1.f);
	light6.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light6.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light6.specular = glm::vec3(0.f, 1.f, 1.f);
	light6.specularExponent = 250.f;
	light6.constantAttenuation = 1.f;
	light6.linearAttenuation = 0.1f;
	light6.quadraticAttenuation = 0.001f;

	light7.positionOrDirection = glm::vec4(30.f, -4.f, 2.f, 1.f);
	light7.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light7.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light7.specular = glm::vec3(0.f, 1.f, 1.f);
	light7.specularExponent = 250.f;
	light7.constantAttenuation = 1.f;
	light7.linearAttenuation = 0.1f;
	light7.quadraticAttenuation = 0.001f;

	light8.positionOrDirection = glm::vec4(0.f, 29.f, 2.f, 1.f);
	light8.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	light8.diffuse = glm::vec3(0.f, 1.f, 1.f);
	light8.specular = glm::vec3(0.f, 1.f, 1.f);
	light8.specularExponent = 250.f;
	light8.constantAttenuation = 1.f;
	light8.linearAttenuation = 0.1f;
	light8.quadraticAttenuation = 0.001f;

	pointLights.push_back(light1);
	pointLights.push_back(light2);
	pointLights.push_back(light3);
	pointLights.push_back(light4);
	pointLights.push_back(light5);
	pointLights.push_back(light6);
	pointLights.push_back(light7);
	pointLights.push_back(light8);

	directionalLight.positionOrDirection = glm::vec4(-1.f, -10.f, -20.f, 0.f);
	directionalLight.ambient = glm::vec3(0.01f);
	directionalLight.diffuse = glm::vec3(1.f);
	directionalLight.specular = glm::vec3(1.f);
	directionalLight.specularExponent = 50.f;

	//setup the camera
	game = new Game;
	game->initGame();

	loadQuads();
	loadShaders();

	phong = new ShaderProgram();
	phong->load("shaders/Phong.vert", "shaders/Phong.frag");

	passThrough = new ShaderProgram();
	passThrough->load("shaders/animationVertex.vert", "shaders/viewShade.frag");

	particleShader = new ShaderProgram();
	particleShader->loadGeo("shaders/particleVert.vert", "shaders/particleFrag.frag", "shaders/particleGeo.geo");

	startBar = new Mesh();
	startBar->loadFromFile("meshes/StartBar.obj");

	for (int i = 0; i < 6; i++)
	{
		particleTextures.push_back(Texture());
	}

	particleTextures[0].load("Textures/explosionParticles.png"); //particleTexture
	particleTextures[1].load("Textures/gruntSpawnP.png"); //spart1
	particleTextures[2].load("Textures/expSpawnP.png"); //spart2
	particleTextures[3].load("Textures/sprinkSpawnP.png"); //spart3
	particleTextures[4].load("Textures/reviveSpawnP.png"); //spart4
	particleTextures[5].load("Textures/reviveSpawnP.png");//spart5

	for (int i = 0; i < 6; i++)
	{
		particles.push_back(new ParticleClass);
	}

	for (int i = 0; i < 500; i++)
	{
		particles[0] = new ParticleClass;
		particles[0]->lifeRange = glm::vec3(1.0f, 2.0f, 0.0f);
		particles[0]->initialForceMin = glm::vec3(0.f, 0.0f, -5.0f);
		particles[0]->initialForceMax = glm::vec3(0.f, 0.0f, 5.0f);
		particles[0]->initialize(1, particleShader, &particleTextures[1]);
		particles[0]->play();

		particlesVec.push_back(particles[0]);

		particles[1] = new ParticleClass;
		particles[1]->lifeRange = glm::vec3(1.0f, 2.0f, 0.0f);
		particles[1]->initialForceMin = glm::vec3(0.f, 0.0f, -5.0f);
		particles[1]->initialForceMax = glm::vec3(0.f, 0.0f, 5.0f);
		particles[1]->initialize(1, particleShader, &particleTextures[2]);
		particles[1]->play();

		particlesVec2.push_back(particles[1]);

		particles[2] = new ParticleClass;
		particles[2]->lifeRange = glm::vec3(1.0f, 2.0f, 0.0f);
		particles[2]->initialForceMin = glm::vec3(0.f, 0.0f, -5.0f);
		particles[2]->initialForceMax = glm::vec3(0.f, 0.0f, 5.0f);
		particles[2]->initialize(1, particleShader, &particleTextures[3]);
		particles[2]->play();

		particlesVec3.push_back(particles[2]);

		particles[4] = new ParticleClass;
		particles[4]->lifeRange = glm::vec3(1.0f, 2.0f, 0.0f);
		particles[4]->initialForceMin = glm::vec3(0.f, 0.0f, -5.0f);
		particles[4]->initialForceMax = glm::vec3(0.f, 0.0f, 5.0f);
		particles[4]->initialize(1, particleShader, &particleTextures[4]);
		particles[4]->play();

		particlesVec5.push_back(particles[4]);

		particles[5] = new ParticleClass;
		particles[5]->lifeRange = glm::vec3(1.0f, 2.0f, 0.0f);
		particles[5]->initialForceMin = glm::vec3(0.f, 0.0f, -5.0f);
		particles[5]->initialForceMax = glm::vec3(0.f, 0.0f, 5.0f);
		particles[5]->initialize(1, particleShader, &particleTextures[0]);
		particles[5]->play();

		particlesVec6.push_back(particles[5]);
	}

	particles[3] = new ParticleClass;
	particles[3]->lifeRange = glm::vec3(1.0f, 2.0f, 0.0f);
	particles[3]->initialForceMin = glm::vec3(-10.f, 0.0f, -5.0f);
	particles[3]->initialForceMax = glm::vec3(1.f, 5.0f, 5.0f);
	particles[3]->partColour = glm::vec3(1.0, 0.5, 0.0);
	particles[3]->initialize(100, particleShader, &particleTextures[0]);
	particles[3]->play();

	se = new SoundEngine();

	if (!se->Init())
	{
		printf("Fatal Error: Exiting program\n");
		system("pause");
		exit(1);
	}

	se->setListenerPosition(glm::vec3(0, 20, -5));
	se->setListenerVelocity(glm::vec3(0));

	se->system->set3DSettings(0.6, 1.0, 0.3);

	for (int i = 0; i < 4; i++)
	{
		backgroundMusic.push_back(new SoundSystem());
		if (i == 0)
			backgroundMusic[i]->createSound(soundPath + "Opening_Menu_Screen.wav", *se->system);
		else if (i == 1)
			backgroundMusic[i]->createSound(soundPath + "Onslaught_Normal_FIght.wav", *se->system);
		else if (i == 2)
			backgroundMusic[i]->createSound(soundPath + "Boss_Fight_Music.wav", *se->system);
		else if (i == 3)
			backgroundMusic[i]->createSound(soundPath + "End_Menu_Screent.wav", *se->system);

		backgroundMusic[i]->sound->set3DMinMaxDistance(1.f, 5.0f);
		backgroundMusic[i]->setMode(FMOD_LOOP_NORMAL);
		backgroundMusic[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(backgroundMusic[i]->sound, NULL, false, &backgroundMusic[i]->channel);
		backgroundMusic[i]->channel->setPaused(true);
		backgroundMusic[i]->setPosition(glm::vec3(0, 0, 0));
		backgroundMusic[i]->channel->setVolume(0.3);
	}


	for (int i = 0; i < 6; i++)
	{
		announcerDialogue.push_back(new SoundSystem());
		if (i == 0)
			announcerDialogue[i]->createSound(soundPath + "Pitched dialogue/Let_The_Onslaught_Begin.wav", *se->system);
		if (i == 1)
			announcerDialogue[i]->createSound(soundPath + "Pitched dialogue/Player_1_Ready.wav", *se->system);
		if (i == 2)
			announcerDialogue[i]->createSound(soundPath + "Pitched dialogue/Player_2_Ready.wav", *se->system);
		if (i == 3)
			announcerDialogue[i]->createSound(soundPath + "Pitched dialogue/Player_Down.wav", *se->system);
		if (i == 4)
			announcerDialogue[i]->createSound(soundPath + "Pitched dialogue/Player_Revived.wav", *se->system);
		if (i == 5)
			announcerDialogue[i]->createSound(soundPath + "Pitched dialogue/Game_Over.wav", *se->system);

		announcerDialogue[i]->setMode(FMOD_LOOP_OFF);
		announcerDialogue[i]->sound->set3DMinMaxDistance(1.f, 5.0f);
		announcerDialogue[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(announcerDialogue[i]->sound, NULL, false, &announcerDialogue[i]->channel);
		announcerDialogue[i]->channel->setPaused(true);
		announcerDialogue[i]->setPosition(glm::vec3(0, 0, 0));
	}

	for (int i = 0; i < 3; i++)
	{
		//stanard fast and heavy
		pFiring.push_back(new SoundSystem);
		if (i == 0)
			pFiring[i]->createSound(soundPath + "Guns/Grunt_Standard_Shot.wav", *se->system);
		else if (i == 1)
			pFiring[i]->createSound(soundPath + "Guns/Fast_Shot.wav", *se->system);
		else if (i == 2)
			pFiring[i]->createSound(soundPath + "Guns/Heavy_Shot.wav", *se->system);

		pFiring[i]->setMode(FMOD_LOOP_NORMAL);
		pFiring[i]->sound->set3DMinMaxDistance(1.f, 3.0f);
		pFiring[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(pFiring[i]->sound, NULL, false, &pFiring[i]->channel);
		pFiring[i]->channel->setPaused(true);
		pFiring[i]->setVolume(0.9);

		pFiring2.push_back(new SoundSystem);
		if (i == 0)
			pFiring2[i]->createSound(soundPath + "Guns/Grunt_Standard_Shot.wav", *se->system);
		else if (i == 1)
			pFiring2[i]->createSound(soundPath + "Guns/Fast_Shot.wav", *se->system);
		else if (i == 2)
			pFiring2[i]->createSound(soundPath + "Guns/Heavy_Shot.wav", *se->system);

		pFiring2[i]->setMode(FMOD_LOOP_NORMAL);
		pFiring2[i]->sound->set3DMinMaxDistance(1.f, 3.0f);
		pFiring2[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(pFiring2[i]->sound, NULL, false, &pFiring2[i]->channel);
		pFiring2[i]->channel->setPaused(true);
		pFiring2[i]->setVolume(0.9);
	}

	for (int i = 0; i < 4; i++)
	{
		enemyExplosions.push_back(new SoundSystem());
		enemyExplosions[i]->createSound(soundPath + "Explosion.wav", *se->system);
		enemyExplosions[i]->setMode(FMOD_LOOP_OFF);
		enemyExplosions[i]->sound->set3DMinMaxDistance(1.f, 2.0f);
		enemyExplosions[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(enemyExplosions[i]->sound, NULL, false, &enemyExplosions[i]->channel);
		enemyExplosions[i]->channel->setPaused(true);
	}

	for (int i = 0; i < 22; i++)
	{
		enemyShooting.push_back(new SoundSystem());
		if (i < avengerIndex || i == reviveIndex || i == bossIndex) //the regular ones + the revive and boss
		{
			enemyShooting[i]->createSound(soundPath + "Guns/Grunt_Standard_Shot.wav", *se->system);
		}
		else if (i < sprinklerIndex) //i'm setting the avenger sounds here
		{
			enemyShooting[i]->createSound(soundPath + "Guns/Avenger_Shot.wav", *se->system);
		}
		else if (i == sprinklerIndex) //lastly the sprinkler sounds
		{
			enemyShooting[i]->createSound(soundPath + "Guns/Sprinkler_Shot.wav", *se->system);
		}

		enemyShooting[i]->sound->set3DMinMaxDistance(1.f, 3.0f);
		enemyShooting[i]->setMode(FMOD_LOOP_OFF);
		enemyShooting[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(enemyShooting[i]->sound, 0, false, &enemyShooting[i]->channel);
		enemyShooting[i]->channel->setPaused(true);
		enemyShooting[i]->channel->setVolume(0.9);
	}

	for (int i = 0; i < 2; i++)
	{
		menuSFX.push_back(new SoundSystem);
		menuSFX[i]->createSound(soundPath + "Menu_Shift.wav", *se->system);
		menuSFX[i]->channel->setVolume(1.5);
		menuSFX[i]->setMode(FMOD_LOOP_OFF);
		menuSFX[i]->sound->set3DMinMaxDistance(1.f, 5.0f);
		menuSFX[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(menuSFX[i]->sound, NULL, false, &menuSFX[i]->channel);
		menuSFX[i]->channel->setPaused(true);
	}

	for (int i = 0; i < 2; i++)
	{
		powerUps.push_back(new SoundSystem);
		if (i == 0)
			powerUps[i]->createSound(soundPath + "Repair_Sound.wav", *se->system);
		else if (i == 1)
			powerUps[i]->createSound(soundPath + "2x_Pickup.wav", *se->system);

		powerUps[i]->setMode(FMOD_LOOP_OFF);
		powerUps[i]->sound->set3DMinMaxDistance(1.5f, 3.0f);
		powerUps[i]->setRolloff(FMOD_3D_INVERSEROLLOFF);
		se->system->playSound(powerUps[i]->sound, NULL, false, &powerUps[i]->channel);
		powerUps[i]->channel->setPaused(true);
		powerUps[i]->channel->setVolume(0.1);


	}

	glutMainLoop();

	//theEnemies.clear(); 
	//bodies.clear(); 
	//turrets.clear(); 
	//charTurretTexture.clear(); 
	//charBodiesTexture.clear(); PlayerTextures1.clear(); 
	//PlayerTurretTex1.clear(); PlayerTextures2.clear(); PlayerTurretTex2.clear(); enemyShooting.clear(); enemyExplosions.clear(); pFiring.clear(); pFiring2.clear(); announcerDialogue.clear();
	//menuSFX.clear();

	for (int i = 0; i < 6; i++)
	{
		particles[i]->freeMemory();
	}

	delete playerOne, playerTwo, arena1, enemy, enemyTexture, enemyTurret, turretPieceMesh, turretTip, phong, passThrough,
		enemyBullet, bulletTexture, damageTexture, player1HP, player2HP, bloomMat, fboMat, leaderboard, winScreen,
		healthBars, healthBarsFast, healthBarsHeavy, player1StandardHP, player1FastHP, player1HeavyHP, player1HPEmpty,
		player2StandardHP, player2FastHP, player2HeavyHP, player2HPEmpty, graveMesh, onslaughtLogo, player_cust1, player_cust2, se,
		backgroundMusic;

	return 0;
}



















