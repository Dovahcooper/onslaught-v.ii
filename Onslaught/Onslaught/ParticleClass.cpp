#include "ParticleClass.h"
#include <algorithm>
#include <random>
#include <time.h>
#include "glm/gtc/random.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

ParticleClass::ParticleClass()
{
	// Initialize memory pointers to null
	particles.positions = nullptr;
	particles.velocities = nullptr;
	particles.remainingLives = nullptr;
	particles.accelerations = nullptr;
	particles.masses = nullptr;
	numParticles = 0;

	playing = false;

	shader = new ShaderProgram();
}

ParticleClass::~ParticleClass()
{
	freeMemory();
}


void ParticleClass::initialize(unsigned int newNumParticles, ShaderProgram *newShader, Texture *newTexture)
{
	//set Shader
	shader = newShader;
	partTexture = newTexture;

	// Clear existing memory
	if (allocated)
		freeMemory();

	if (newNumParticles) // make sure new array size is not 0
	{
		particles.positions = new glm::vec3[newNumParticles];
		particles.velocities = new glm::vec3[newNumParticles];
		particles.accelerations = new glm::vec3[newNumParticles];
		particles.remainingLives = new float[newNumParticles];
		particles.masses = new float[newNumParticles];
		memset(particles.remainingLives, 0, sizeof(float) * newNumParticles);

		numParticles = newNumParticles;
		allocated = true; // mark that memory has been allocated

		// Set up VBO
		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);

		glBindVertexArray(vao);

		glEnableVertexAttribArray(0);  //Vertices
		//glEnableVertexAttribArray(1); //UVs
		//glEnableVertexAttribArray(2);//Normies	//not needed quite yet

		//vertices
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * (newNumParticles * 3),
			particles.positions, GL_DYNAMIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);

		glBindVertexArray(GL_NONE);
	}
}

void ParticleClass::update(float dt)
{
	// Make sure particle memory is initialized
	if (allocated && playing)
	{
		// loop through each particle
		for (int i = 0; i < numParticles; i++)
		{
			//srand(time(NULL));
			// Get each property for the particle using pointer arithmetic
			glm::vec3* pos = particles.positions + i;
			glm::vec3* vel = particles.velocities + i;
			glm::vec3* accel = particles.accelerations + i;
			float* life = particles.remainingLives + i;
			float* mass = particles.masses + i;
			// other properties... 

			
			if (doExplode == false && *life <= 0)
			{
				//playing = false;
				// if dead respawn
				*pos = initialPosition;

				(*vel).x = (rand() % 5 + 1)+ ((rand() % 5 + 1) * -1);
				(*vel).y = (rand() % 5 + 1)+ ((rand() % 5 + 1) * -1);
				(*vel).z = rand() % 2;

				*life = glm::linearRand(lifeRange.x, lifeRange.y);
				*mass = glm::linearRand(0.5f, 1.0f);
				*accel = *vel / *mass;

				//doExplode = true;
				
			}

			if (doExplode == true && *life <= 0)
			{
				*pos = glm::vec3(-800, 0 , 0);

				(*vel).x = glm::mix(initialForceMin.x, initialForceMax.x, glm::linearRand(0.0f, 1.0f));
				(*vel).y = glm::mix(initialForceMin.y, initialForceMax.y, glm::linearRand(0.0f, 1.0f));
				(*vel).z = glm::mix(initialForceMin.z, initialForceMax.z, glm::linearRand(0.0f, 1.0f));

				*life = glm::linearRand(lifeRange.x, lifeRange.y);
				*mass = glm::linearRand(0.5f, 1.0f);
				*accel = *vel / *mass;

			}

			// Update position and velocity
			
			(*pos).x += (*vel).x * dt * 0.2;
			(*pos).y += (*vel).y * dt * 0.2;
			(*pos).z += (*vel).z * dt * 0.2;
			//*vel += dt;
			*life -= (dt * 0.25);
			if (reset)
				*life = 0;
			
			//timeKeep += dt;
		}
	}
}

void ParticleClass::updateSpawn(float dt)
{
	// Make sure particle memory is initialized
	if (allocated && playing)
	{
		// loop through each particle
		for (int i = 0; i < numParticles; i++)
		{
			// Get each property for the particle using pointer arithmetic
			glm::vec3* pos = particles.positions + i;
			glm::vec3* vel = particles.velocities + i;
			glm::vec3* accel = particles.accelerations + i;
			float* life = particles.remainingLives + i;
			float* mass = particles.masses + i;
			// other properties... 

			// check if alive
			if (*life <= 0)
			{
				// if dead respawn
				*pos = initialPosition;

				(*vel).x = glm::mix(initialForceMin.x, initialForceMax.x, glm::linearRand(0.0f, 1.0f));
				(*vel).y = glm::mix(initialForceMin.y, initialForceMax.y, glm::linearRand(0.0f, 1.0f));
				(*vel).z = glm::mix(initialForceMin.z, initialForceMax.z, glm::linearRand(0.0f, 1.0f));

				*life = glm::linearRand(lifeRange.x, lifeRange.y);
				*mass = glm::linearRand(0.5f, 1.0f);
				*accel = *vel / *mass;
			}

			// Update position and velocity
			(*pos).x = (*pos).x; //cos(angle * 3.14 / 180) * 1;
			(*pos).y = (*pos).y; //sin(angle * 3.14 / 180) * 1;
			(*pos).z += (*vel).z * dt + (*accel).z * 0.5f * (dt*dt);
			*vel += dt;
			*life -= dt;

			angle += 5;
			timeKeep += dt;
		}
	}
}

void ParticleClass::setMaterial(std::string diffuse)
{
	material->loadTexture(TextureType::Diffuse, diffuse);
}

void ParticleClass::draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection)
{
	//bind vao and vbo
	glBindVertexArray(vao);

	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * (numParticles * 3), particles.positions);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//bind shader
	shader->bind();
	//send uniforms
	shader->sendUniformMat4("uView", glm::value_ptr(cameraTransform), false);
	shader->sendUniformMat4("uProj", glm::value_ptr(cameraProjection), false);
	shader->sendUniform("material.diffuse", 0);
	shader->sendUniform("partColour", partColour);

	glActiveTexture(GL_TEXTURE0);
	partTexture->bind();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	//draw arrays, then unbind vao
	glBindVertexArray(vao);
	glPointSize(2);
	glDrawArrays(GL_POINTS, 0, numParticles);
	glBindVertexArray(GL_NONE);

	partTexture->unbind();

	glDepthMask(GL_TRUE);
	//unbind shader
	shader->unbind();
}

void ParticleClass::freeMemory()
{
	// Free up all arrays here
	if (allocated)
	{
		delete[] particles.positions;
		delete[] particles.velocities;
		delete[] particles.remainingLives;
		delete[] particles.accelerations;
		delete[] particles.masses;

		particles.positions = nullptr;
		particles.velocities = nullptr;
		particles.remainingLives = nullptr;
		particles.accelerations = nullptr;
		particles.masses = nullptr;

		allocated = false;
		numParticles = 0;
	}
}