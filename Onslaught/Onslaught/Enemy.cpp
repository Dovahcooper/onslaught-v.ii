#include "Enemy.h"

Enemy::Enemy(Mesh * temp)
{
	bulletMesh = temp;
	enemyTexture = new Material();
	damageTexture = new Material();
	bulletTexture = new Material();
	gridIndex = 0;
	spawnTimer = 0;
	dTimer = 0;
	tutorial = false;
	blnTutorialCollision = false;

	spawnPosition[0] = (glm::vec3(17, 16, 0));
	spawnPosition[1] = glm::vec3(-17, -16, 0);
	spawnPosition[2] = glm::vec3(-17, 16, 0);
	spawnPosition[3] = glm::vec3(17, -16, 0);
	spawnPosition[4] = glm::vec3(0, 23, 0);
	spawnPosition[5] = glm::vec3(0, -23, 0);
	spawnPosition[6] = glm::vec3(-20, 0, 0);
	spawnPosition[7] = glm::vec3(20, 0, 0);
	firing = false;
	NUM_ITERATIONS = 8;
	shotCounter = 0;
}

Enemy::~Enemy()
{
	for (int i = 0; i < enemyBullet.size();i++)
		delete enemyBullet[i];

	delete enemyTexture;
	delete damageTexture;
	delete bulletTexture;
	bulletMesh->unload();
}

void Enemy::calculateTarget(Player * thePlayer, Player * thePlayer2)
{
	float playerDistance = glm::distance(getPosition(), thePlayer->getPosition());
	float playerDistance2 = glm::distance(getPosition(), thePlayer2->getPosition());

	if (thePlayer->getAlive() == false)
	{
		target = (thePlayer2->getPosition() - getPosition()); //it needs the target to seek to it
		directionFaced = target;
		playerPos = thePlayer2->getPosition();
	}
	else if (thePlayer2->getAlive() == false)
	{
		target = (thePlayer->getPosition() - getPosition()); //it needs the target to seek to it
		directionFaced = target;
		playerPos = thePlayer->getPosition();
	}
	else if (thePlayer->getAlive() == false && thePlayer2->getAlive() == false)
	{
		Alive = false;
	}
	else
	{
		if (playerDistance < playerDistance2)
		{
			target = (thePlayer->getPosition() - getPosition()); //it needs the target to seek to it
			directionFaced = target;
			playerPos = thePlayer->getPosition();
		}
		else
		{
			target = (thePlayer2->getPosition() - getPosition()); //it needs the target to seek to it
			directionFaced = target;
			playerPos = thePlayer2->getPosition();
		}
	}
}

glm::vec3 Enemy::computeSeperation(std::vector <Enemy*> enemies, int index)
{
	return glm::vec3();
}

glm::vec3 Enemy::enemyArrive()
{
	return glm::vec3();
}

void Enemy::enemyAttack()
{

}

void Enemy::collision(Player * thePlayer)
{
	for (int i = 0; i < enemyBullet.size(); i++)
	{
		float distance = glm::distance(enemyBullet[i]->getPosition(), thePlayer->getPosition());

		if (distance < 1.3f)
		{
			//logic for the player getting hurt here
			if (tutorial == false && thePlayer->invincibilityFrames <= 0 && thePlayer->getAlive() == true)
			{
				if (thePlayer->getHealth() > 0)
					thePlayer->damageTaken(10);

				if (thePlayer < 0) thePlayer->setHealth(0);
				thePlayer->hit = true;
			}
			if (thePlayer->getAlive() == true)
			{
				thePlayer->dTimer = 0.5;
				thePlayer->setMaterial(thePlayer->damage);
				deleteBullets(i);
			}
		}
	}
}

void Enemy::tutorialCollision(Player * thePlayer)
{
	for (int i = 0; i < thePlayer->playerBullets.size(); i++)
	{
		float distance = glm::distance(getPosition(), thePlayer->playerBullets[i]->getPosition());

		if (distance < 1.3f)
		{
			//Alive = false;
			thePlayer->deleteBullets(i);
			blnTutorialCollision = true;
		}
	}
}

void Enemy::changeAppearange(Mesh * newMesh, Material * newMaterial)
{
	Object = newMesh;
	enemyTexture = newMaterial;
}

void Enemy::deleteAllBullets()
{
	for (int i = enemyBullet.size() - 1; i > 0; i--)
	{
		delete enemyBullet[i];
		enemyBullet.erase((enemyBullet.begin() + i));
	}
}

void Enemy::initialize(Mesh* newMesh, Material* newMaterial, int spawnPoint, bool champion)
{
	if (champion == false)
	{
		SpawnIndex = spawnPoint;
		setPosition(spawnPosition[spawnPoint]);
	}

	Spawn = true;
	Alive = false;

	Object = newMesh;
	setMaterial(newMaterial);
}

void Enemy::update(float deltaTime, int & numAlive, PowerUp *powerUp, bool FKoverride)
{
	GameObject::update(deltaTime, FKoverride);

	if (dTimer > 0)
	{
		dTimer -= deltaTime;
		if (dTimer <= 0)
		{
			dTimer = 0;
			setMaterial(enemyTexture);
		}
	}

	if (Alive == true && health <= 0 && _enemyType != _Avengers && _enemyType != _Boss && tutorial == false)
	{
		Alive = false;
		Spawn = false;
		numAlive--;

		if (powerUp != NULL && powerUp->Alive == false)
		{
			powerUp->powerupChance = (rand() % 4);
			powerUp->setPosition(getPosition());
		}
	}

	else if (health <= 0 && tutorial == true && Alive == true)
	{
		blnTutorialCollision = false;
		if (_enemyType != _Avengers)
			tutorial = false;
	}

	if (Alive == true && _enemyType != _Boss)
	{
		timer += deltaTime;
		directionFaced = glm::normalize(directionFaced);
		float angle = glm::degrees(glm::acos(directionFaced.x / 1));
		if (directionFaced.y > 0)
		{
			setRotationAngleY((angle)-90);
		}
		else
		{
			setRotationAngleY((-angle) - 90);
		}
		//subtract a little bit to nudge in the right direction
	}
	else if (Spawn == true)
	{
		spawnTimer += (1 * deltaTime);
		setRotationAngleY((getRotationAngleY() + 3 * deltaTime));
	}
}

void Enemy::updateBullets(float deltaTime)
{
	for (int i = 0; i < enemyBullet.size(); i++)
	{
		enemyBullet[i]->update(deltaTime);
		if (enemyBullet[i]->getPosition().x > 30 || enemyBullet[i]->getPosition().x < -30 || enemyBullet[i]->getPosition().y < -30 || enemyBullet[i]->getPosition().y > 30) deleteBullets(i);
	}
}

void Enemy::draw(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	if (Alive == true || Spawn == true)
		GameObject::draw(cameraTransform, cameraProjection, pointLights, directionalLight);

	for (int i = 0; i < enemyBullet.size(); i++)
	{
		enemyBullet[i]->draw(cameraTransform, cameraProjection, pointLights, directionalLight);
	}
}

void Enemy::checkSpawn(Mesh * newMesh, Material * newMaterial, bool champion)
{

}

void Enemy::createBullets(float & _x, float & _y)
{
	enemyBullet.push_back(new Bullet(_x, _y, getPosition(), bulletMesh, shader, bulletTexture, 15.f));
}

void Enemy::deleteBullets(int index)
{
	delete enemyBullet[index];
	enemyBullet.erase((enemyBullet.begin() + index));
	//delete enemyBullet[index];
}

void Enemy::setBulletTexture(Material * bullet)
{
	bulletTexture = bullet;
}

void Enemy::setDamageTexture(Material * mat)
{
	damageTexture = mat;
}

void Enemy::setHealth(float temp)
{
	health = temp;
}

float Enemy::getTimer()
{
	return timer;
}

void Enemy::setTimer(float temp)
{
	timer = temp;
}

float Enemy::getHealth()
{
	return health;
}

int Enemy::getBulletSize()
{
	return enemyBullet.size();
}
