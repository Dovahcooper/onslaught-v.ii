#include "PowerUp.h"

PowerUp::PowerUp()
{
	Alive = false;
	powerupChance = 200;
	timer = 0;
	NUM_ITERATIONS = 8;
	playSound = false;
}

PowerUp::~PowerUp()
{
}

void PowerUp::initialize(glm::vec3 thePosition, PowerUpType type, Mesh* body, Material* texture)
{
	Alive = true;
	setPosition(thePosition);

	_type = type;

	if (_type == PowerUpType(0))
	{
		setRotationAngleX(90);
		setRotationAngleY(-90);
		setRotationAngleZ(0);
	}
	else if (_type == PowerUpType(1))
	{
		setRotationAngleX(0);
		setRotationAngleY(0);
		setRotationAngleZ(180);
	}

	setMaterial(texture);

	Object = body;
}

void PowerUp::Collision(Player * thePlayer, int &index, int max)
{
	float distance = glm::distance(getPosition(), thePlayer->getPosition());

	if (distance < 3)
	{
		if (_type == PowerUpType(0)) //the health powerup
		{
			if (thePlayer->getHealth() < thePlayer->maxHealth)
			{
				thePlayer->setHealth(thePlayer->getHealth() + 10);
				if (index < max)
					index++;

				Alive = false;
				timer = 0;
				playSound = true;
			}
		}
		if (_type == PowerUpType(1)) //the score powerup
		{
			thePlayer->scoreMultiplayer++;
			thePlayer->scoreTimer += 15;

			Alive = false;
			timer = 0;
			playSound = true;
		}

	}
}

void PowerUp::update(float deltaTime)
{
	GameObject::update(deltaTime);

	if (Alive == true)
	{
		timer += deltaTime;
		if (timer > 6)
		{
			Alive = false;
			timer = 0;
		}
	}
}

void PowerUp::draw(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	GameObject::draw(cameraTransform, cameraProjection, pointLights, directionalLight);
}
