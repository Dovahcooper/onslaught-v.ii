#pragma once

#include "Timer.h"
#include "ShaderProgram.h"
#include "Mesh.h"
#include "Grunt.h"
#include "Player.h"
#include "GameObject.h"
#include <glm\gtc\type_ptr.hpp>
#include <string>
#include <sstream>

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define FRAMES_PER_SEC 60

class Game
{
public:
	Game();
	~Game();

	void initGame();
	void draw();

	void update();
	bool collisionDetection(GameObject *one, GameObject *two);

	void keyboardUp(unsigned char &key, int &x, int &y);
	void keyboardDown(unsigned char &key, int &x, int &y);

	void Game::renderScore(int point1, int point2);
	void Game::renderGameInfo(int waveNumber);
	void renderText(float x, float y, float z, std::string text);
	void Game::drawStrokeFont(float x, float y, float z, void *font, char *string);

	glm::mat4 cameraTransform;
	glm::mat4 cameraProj;
	glm::mat4 cameraOrtho;
	glm::mat4 lightOrtho;
	glm::mat4 lightView;
	glm::mat4x4 identity;
	ShaderProgram viewShade;
	Timer* updateTimer;
	float deltaTime;

private:

	glm::mat4 playerTransform;
	glm::mat4 cameraRotate;
	glm::mat4 cameraTranslate;
};