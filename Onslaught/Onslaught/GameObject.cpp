#include "GameObject.h"
#include <iostream>
#include <GL/glut.h>
#include "animateMorph.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

GameObject::GameObject()
{
	Object = new Mesh();
	shader = new ShaderProgram(); // no need for this one as it gets overwritten in initializeObject
	mat = new Material(); // no need for this as "setMaterial" gets called at some point

	AABB = height * width;
	parent = nullptr;
	scaleX = 0;
	scaleY = 0;
	scaleZ = 0;
	toggleTexture = 1;
	NUM_ITERATIONS = 8;

	lightShader = new ShaderProgram();
	//rendering 
	if (!lightShader->load("shaders/viewShade.vert", "shaders/shadowPass.frag"))
	{
		std::cout << "Shaders failed to initialize" << std::endl;
		system("pause");
		exit(0);
	}
}

GameObject::~GameObject()
{
	// You can't delete a shader that other objects depend on!
	//if (shader != NULL)
	//shader->unload();

	//Object->unload(); // Can't unload this for the same reason as the shader, other objects in the scene point to this same Mesh!

	//delete mat; // since you use setMaterial to set this pointer to Memory alloacted by another object delete this memory is very unsafe (its the reason for your crash)

	// Children should never delete their parent, but a parent can delete it's children. but given the current jank factor I'd just leave it as is.
	//if (parent != nullptr)
	//	delete parent,children;

	//delete shader, Object, mat;
}

void GameObject::oldInitializeObject(std::string fileLocation)
{
	if (!Object->loadFromFile(fileLocation))
	{
		std::cout << "Model failed to load" << std::endl;
		system("PAUSE");
		exit(0);
	}

	if (!shader->load("shaders/Phong.vert", "shaders/Phong.frag"))
	{
		std::cout << "Shaders failed to initialize" << std::endl;
		system("pause");
		exit(0);
	}

	if (!passThrough.load("shaders/animationVertex.vert", "shaders/viewShade.frag"))
	{
		std::cout << "Shaders failed to initialize" << std::endl;
		system("pause");
		exit(0);
	}
}

void GameObject::oldInitializeObject2(std::string fileLocation)
{
	Object = new Mesh();
	if (!Object->loadFromFile(fileLocation))
	{
		std::cout << "Model failed to load" << std::endl;
		system("PAUSE");
		exit(0);
	}

	if (!shader->load("shaders/animationVertex.vert", "shaders/Phong.frag"))
	{
		std::cout << "Shaders failed to initialize" << std::endl;
		system("pause");
		exit(0);
	}

	if (!passThrough.load("shaders/animationVertex.vert", "shaders/viewShade.frag"))
	{
		std::cout << "Shaders failed to initialize" << std::endl;
		system("pause");
		exit(0);
	}

	morph.morphMesh.push_back(Object);
	Object = morph.morphMesh[0];
	printf("%i\n", morph.morphMesh.size());
}

void GameObject::initializeObject(Mesh * meshObject, ShaderProgram * newPhong, bool active)
{
	Object = meshObject;
	shader = newPhong;

	if (active == true)
	{
		morph.morphMesh.push_back(Object);
		Object = morph.morphMesh[0];
		printf("%i\n", morph.morphMesh.size());
	}
}

void GameObject::initializeObject(std::string fileLocation, TextureColour _color)
{
	switch (_color)
	{
	case TextureColour::Blue:
		//load the blue texture here
		setMaterial("Textures/PlayerUV.png", "Textures/monkeySpecular.png");
		break;
	case TextureColour::Green:
		//load the green texture here
		setMaterial("Textures/PlayerUVMap.png", "Textures/monkeySpecular.png");
		break;
	case TextureColour::Red:
		//load the enemy texture here
		setMaterial("Textures/red.png", "Textures/monkeySpecular.png");
		break;
	case TextureColour::Arena:
		//loads arena texture
		setMaterial("Textures/ArenaUvTexture.png", "Textures/monkeySpecular.png");
		break;
	default:
		//load the red one here, I haven't given 
		//the player's room to set anything other than
		//green or blue, and the actual default is red,
		//I'm just being thorough
		break;

	}
	//loads in the shaders and meshes
	if (!shader->load("shaders/viewShade.vert", "shaders/viewShade.frag"))
	{
		std::cout << "Shaders failed to initialize" << std::endl;
		system("pause");
		exit(0);
	}

	if (!Object->loadFromFile(fileLocation))
	{
		std::cout << "Model failed to load" << std::endl;
		system("PAUSE");
		exit(0);
	}
}

void GameObject::update(float deltaTime, bool FKoverride)
{
	//this is what updates the rotation, position, and scale
	glm::mat4 rx = glm::rotate(glm::radians(rotX), glm::vec3(1.0f, 0.0f, 0.0f));
	glm::mat4 ry = glm::rotate(glm::radians(rotY), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 rz = glm::rotate(glm::radians(rotZ), glm::vec3(0.0f, 0.0f, 1.0f));

	localRotation = rx * ry * rz;

	if (shouldAnimate)
	{
		morph.interpolate += deltaTime;

		if (morph.interpolate >= 1.0f)
		{
			morph.interpolate = 0;

			if (animateBackwards == false)
			{
				morph.currentFrame++;
				morph.nextFrame++;
			}
			else if (animateBackwards == true && morph.currentFrame > 0)
			{
				morph.currentFrame--;
				morph.nextFrame--;
				animateBackwards = false;
			}
			if (morph.nextFrame == morph.morphMesh.size())
			{
				morph.nextFrame = 0; //0 is max health
			}

			if (morph.currentFrame == morph.morphMesh.size())
			{
				morph.currentFrame = 0;
			}

			Object = morph.morphMesh[morph.currentFrame];

		}
	}

	localMatrix = (glm::translate(localPosition)) *  localRotation * objectScale;

	if (FKoverride == false)
	{
		if (parent)
		{
			localToWorldMatrix = parent->getLocalToWorldMatrix() * localMatrix;
		}
		else
		{
			localToWorldMatrix = localMatrix;
		}

		// Update children
		for (int i = 0; i < children.size(); i++)
			children[i]->update(deltaTime);
	}
}

void GameObject::draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight)
{
	//rendering 
	shader->bind();
	glm::mat4 lightOrtho = glm::ortho(-100.0f, 100.0f, -100.0f, 100.0f, -12.f, 25.5f);
	glm::mat4 lightView = glm::lookAt(glm::vec3(0.0f, 2.0f, 3.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));

	glm::mat4 shadowMatrix = lightOrtho * lightView;
	shader->sendUniformMat4("uModel", glm::value_ptr(localToWorldMatrix), false); //we dont have to transpose
	shader->sendUniformMat4("uView", glm::value_ptr(cameraTransform), false);
	shader->sendUniformMat4("uProj", glm::value_ptr(cameraProjection), false);
	shader->sendUniformMat4("uLightMat", glm::value_ptr(shadowMatrix), false);

	shader->sendUniform("material.diffuse", 0);
	shader->sendUniform("material.specular", 1);
	shader->sendUniform("material.hue", mat->hue);
	shader->sendUniform("material.specularExponent", mat->specularExponent);
	shader->sendUniform("blend", morph.interpolate);
	shader->sendUniform("textureToggle", toggleTexture);
	shader->sendUniform("NUM_ITERATIONS", NUM_ITERATIONS);


	//for (int i = 0; i < pointLights.size(); i++)
	for (int i = 0; i < NUM_ITERATIONS; i++)
	{
		std::string prefix = "pointLights[" + std::to_string(i) + "].";
		shader->sendUniform(prefix + "position", cameraTransform * pointLights[i].positionOrDirection);
		shader->sendUniform(prefix + "ambient", pointLights[i].ambient);
		shader->sendUniform(prefix + "diffuse", pointLights[i].diffuse);
		shader->sendUniform(prefix + "specular", pointLights[i].specular);

		shader->sendUniform(prefix + "specularExponent", pointLights[i].specularExponent);
		shader->sendUniform(prefix + "constantAttenuation", pointLights[i].constantAttenuation);
		shader->sendUniform(prefix + "linearAttenuation", pointLights[i].linearAttenuation);
		shader->sendUniform(prefix + "quadraticAttenuation", pointLights[i].quadraticAttenuation);
	}

	shader->sendUniform("directionalLight.direction", cameraTransform * directionalLight.positionOrDirection);
	shader->sendUniform("directionalLight.ambient", directionalLight.ambient);
	shader->sendUniform("directionalLight.diffuse", directionalLight.diffuse);
	shader->sendUniform("directionalLight.specular", directionalLight.specular);
	shader->sendUniform("directionalLight.specularExponent", directionalLight.specularExponent);

	glActiveTexture(GL_TEXTURE0);
	mat->diffuse.bind();
	glActiveTexture(GL_TEXTURE1);
	mat->specular.bind();
	glActiveTexture(GL_TEXTURE2);
	mat->normal.bind();

	glBindVertexArray(Object->vao);
	glDrawArrays(GL_TRIANGLES, 0, Object->getNumVertices());
	glBindVertexArray(GL_NONE); //to unbind the monkey

								//textureObject.unbind();
	mat->normal.unbind();
	glActiveTexture(GL_TEXTURE1);
	mat->specular.unbind();
	glActiveTexture(GL_TEXTURE0);
	mat->diffuse.unbind();

	shader->unbind();

	drawChildren(cameraTransform, cameraProjection, pointLights, directionalLight);
}

void GameObject::drawUI(glm::mat4 cameraTransform, glm::mat4 cameraOrtho)
{
	//rendering tings
	shader->bind();
	shader->sendUniformMat4("uModel", glm::value_ptr(localMatrix), false); //we dont have to transpose
	shader->sendUniformMat4("uView", glm::value_ptr(cameraTransform), false);
	shader->sendUniformMat4("uProj", glm::value_ptr(cameraOrtho), false);

	shader->sendUniform("material.diffuse", 0);
	shader->sendUniform("material.specular", 1);
	shader->sendUniform("material.hue", mat->hue);
	shader->sendUniform("material.specularExponent", mat->specularExponent);
	shader->sendUniform("blend", morph.interpolate);

	glActiveTexture(GL_TEXTURE0);
	mat->diffuse.bind();
	glActiveTexture(GL_TEXTURE1);
	mat->specular.bind();

	glBindVertexArray(Object->vao);
	glDrawArrays(GL_TRIANGLES, 0, Object->getNumVertices());
	glBindVertexArray(GL_NONE); //to unbind the monkey	

	mat->specular.unbind();
	glActiveTexture(GL_TEXTURE0);
	mat->diffuse.unbind();

	shader->unbind();
}

void GameObject::drawFromLight(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection)
{
	lightShader->bind();
	lightShader->sendUniformMat4("uModel", glm::value_ptr(localToWorldMatrix), false); //we dont have to transpose
	lightShader->sendUniformMat4("uView", glm::value_ptr(cameraTransform), false);
	lightShader->sendUniformMat4("uProj", glm::value_ptr(cameraProjection), false);

	glBindVertexArray(Object->vao);
	glDrawArrays(GL_TRIANGLES, 0, Object->getNumVertices());
	glBindVertexArray(GL_NONE); //to unbind the monkey

	lightShader->unbind();
}


void GameObject::drawChildren(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	for (int i = 0; i < children.size(); ++i)
	{
		children[i]->draw(cameraTransform, cameraProjection, pointLights, directionalLight);
	}
}

void GameObject::addChild(GameObject * newChild)
{
	if (newChild)
	{
		children.push_back(newChild);
		newChild->parent = this; // tell new child that this game object is its parent
	}
}

void GameObject::removeChild(GameObject * rip)
{
	for (int i = 0; i < children.size(); ++i)
	{
		if (children[i] == rip) // compare memory locations (pointers)
		{
			children.erase(children.begin() + i);
		}
	}
}

void GameObject::setPosition(glm::vec3 newPosition)
{
	localPosition = newPosition; //updates the position
}

void GameObject::setRotationAngleX(float temp)
{
	rotX = temp; //setting the x rotation angle
}

void GameObject::setRotationAngleY(float temp)
{
	//sets the rotation angle y
	rotY = temp;
}

void GameObject::setRotationAngleZ(float temp)
{
	//set rotation angle z
	rotZ = temp;
}

void GameObject::setRotation(glm::vec3 rotation, float deltaTime)
{
	//objectTransform = glm::rotate(objectTransform, deltaTime * (glm::pi<float>() / 4), rotation);
	localRotation = glm::rotate(localRotation, deltaTime * (glm::pi<float>() / 4), rotation);
}

void GameObject::setRotation(glm::vec3 rotation)
{
	rotX = rotation.x;
	rotY = rotation.y;
	rotZ = rotation.z;
}

void GameObject::setScale(float temp)
{
	scaleX = scaleY = scaleX = temp;
	objectScale = glm::scale(glm::vec3(temp)); //sets the uniform scale
}

void GameObject::setSpecificScale(float x, float y, float z)
{
	scaleX = x;
	scaleY = y;
	scaleZ = z;
	objectScale = glm::scale(glm::vec3(scaleX, scaleY, scaleZ)); //sets scale specifically

}

void GameObject::setMaterial(std::string diffuse, std::string specular)
{
	mat->loadTexture(TextureType::Diffuse, diffuse);
	mat->loadTexture(TextureType::Specular, specular);
}

void GameObject::setMaterial(Material* newMat)
{
	mat = newMat;
}

glm::vec3 GameObject::getPosition()
{
	return localPosition; //returns the position
}

float GameObject::getRotationAngleX()
{
	return rotX; //return the angle x
}

float GameObject::getRotationAngleY()
{
	return rotY; //return angle y
}

float GameObject::getRotationAngleZ()
{
	return rotZ; //returns angle z
}

glm::mat4 GameObject::getObjectScale()
{
	return objectScale; //returns scale
}

glm::mat4 GameObject::getLocalMatrix()
{
	return localMatrix;
}

glm::mat4 GameObject::getLocalToWorldMatrix()
{
	return localToWorldMatrix;
}

glm::vec3 GameObject::getWorldPosition()
{
	if (parent)
		return parent->getLocalToWorldMatrix() * glm::vec4(localPosition, 1.0f);
	else
		return localPosition;
}

glm::mat4 GameObject::getWorldRotation()
{
	if (parent)
		return parent->getWorldRotation() * localRotation;
	else
		return localRotation;
}

std::vector<GameObject*> GameObject::getChildren()
{
	return children;
}

GameObject * GameObject::getChild(int i)
{
	return children[i];
}

GameObject * GameObject::getParent()
{
	return parent;
}

float GameObject::getScaleX()
{
	return scaleX;
}

float GameObject::getScaleY()
{
	return scaleY;
}

float GameObject::getScaleZ()
{
	return scaleZ;
}



