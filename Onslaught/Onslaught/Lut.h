#include <iostream>
#include <string>
#include <math.h>
#include <fstream>
#include <vector>

#include <GL\glew.h>
#include <GL\freeglut.h>
#include <glm\vec3.hpp>
#include <glm\gtx\color_space.hpp>

using namespace std;
class Lut
{
public:
	Lut();
	~Lut();
	void load(string fileName);
	void setLutSize(float s);
	void bind();
	void unbind();
private:
	GLuint texture3D;
	float LUTsize = 64.f;
	vector<glm::vec3> LUT;
	bool isBinded = false;
	string LUTpath = "Textures/";


};