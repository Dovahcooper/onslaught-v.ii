#include "ShaderProgram.h"
#include <fstream>
#include <iostream>
#include <vector>

ShaderProgram::ShaderProgram()
{

}

ShaderProgram::~ShaderProgram()
{
	if (loaded)
	{
		unload();
	}
}

bool ShaderProgram::load(const std::string &vertFile, const std::string &fragFile)
{
	//create shader and program objects
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	program = glCreateProgram();

	//load our source code
	std::string source = readFile(vertFile);
	const GLchar* temp = static_cast<const GLchar*>(source.c_str());
	glShaderSource(vertexShader, 1, &temp, NULL);
	///frag's turn
	source = readFile(fragFile);
	temp = static_cast<const GLchar*>(source.c_str());
	glShaderSource(fragShader, 1, &temp, NULL);

	//complie shader code
	if (!compileShader(vertexShader))
	{
		std::cerr << "Vertex Shader failed to compile" << std::endl;

		outputShaderLog(vertexShader);
		unload();

		return false;
	}
	if (!compileShader(fragShader))
	{
		std::cerr << "Fragment Shader failed to compile" << std::endl;

		outputShaderLog(fragShader);
		unload();

		return false;
	}

	//setup our progrm object
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragShader);

	if (!linkProgram())
	{
		std::cerr << "Shader Program Failed to Link" << std::endl;

		outputProgramLog();
		unload();

		return false;
	}

	loaded = true;
	return true;
}

bool ShaderProgram::loadGeo(const std::string &vertFile, const std::string &fragFile, const std::string &geoFile)
{
	//create shader and program objects
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	geoShader = glCreateShader(GL_GEOMETRY_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	program = glCreateProgram();

	//load our source code
	std::string source = readFile(vertFile);
	const GLchar* temp = static_cast<const GLchar*>(source.c_str());
	glShaderSource(vertexShader, 1, &temp, NULL);
	///frag's turn
	source = readFile(fragFile);
	temp = static_cast<const GLchar*>(source.c_str());
	glShaderSource(fragShader, 1, &temp, NULL);
	//geometry shader
	source = readFile(geoFile);
	temp = static_cast<const GLchar*>(source.c_str());
	glShaderSource(geoShader, 1, &temp, NULL);

	//complie shader code
	if (!compileShader(vertexShader))
	{
		std::cerr << "Vertex Shader failed to compile" << std::endl;

		outputShaderLog(vertexShader);
		unload();

		return false;
	}
	if (!compileShader(fragShader))
	{
		std::cerr << "Fragment Shader failed to compile" << std::endl;

		outputShaderLog(fragShader);
		unload();

		return false;
	}

	if (!compileShader(geoShader))
	{
		std::cerr << "Geometry Shader failed to compile" << std::endl;

		outputShaderLog(geoShader);
		unload();

		return false;
	}

	//setup our progrm object
	glAttachShader(program, vertexShader);
	glAttachShader(program, geoShader);
	glAttachShader(program, fragShader);

	if (!linkProgram())
	{
		outputProgramLog();
		std::cerr << "Shader Program Failed to Link" << std::endl;

		unload();

		return false;
	}

	loaded = true;
	return true;
}


bool ShaderProgram::isLoaded() const
{
	return loaded;
}

bool ShaderProgram::linkProgram()
{
	glLinkProgram(program);

	GLint success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);

	return success == GL_TRUE;
}

void ShaderProgram::unload()
{
	if (vertexShader != 0)
	{
		glDetachShader(program, vertexShader);
		glDeleteShader(vertexShader);
		vertexShader = 0;
	}
	if (geoShader != 0)
	{
		glDetachShader(program, geoShader);
		glDeleteShader(geoShader);
		geoShader = 0;
	}
	if (fragShader != 0)
	{
		glDetachShader(program, fragShader);
		glDeleteShader(fragShader);
		fragShader = 0;
	}
	if (program != 0)
	{
		glDeleteProgram(program);
		program = 0;
	}

	loaded = false;
}

void ShaderProgram::bind()
{
	glUseProgram(program);
}

void ShaderProgram::unbind()
{
	glUseProgram(GL_NONE);
}

void ShaderProgram::addAttribute(unsigned int index, const std::string &attribName)
{
	glBindAttribLocation(program, index, attribName.c_str());
}

int ShaderProgram::getAttribLocation(const std::string &attribName)
{
	return glGetAttribLocation(program, attribName.c_str());
}

int ShaderProgram::getUniformLocation(const std::string &uniformName)
{
	return glGetUniformLocation(program, uniformName.c_str());
}

void ShaderProgram::sendUniform(const std::string &uniformName, int integer)
{
	GLint location = getUniformLocation(uniformName);
	glUniform1i(location, integer);
}
void ShaderProgram::sendUniform(const std::string &uniformName, unsigned int unsignedInteger)
{
	GLint location = getUniformLocation(uniformName);
	glUniform1ui(location, unsignedInteger);
}
void ShaderProgram::sendUniform(const std::string &uniformName, float scalar)
{
	GLint location = getUniformLocation(uniformName);
	glUniform1f(location, scalar);
}
void ShaderProgram::sendUniform(const std::string &uniformName, const glm::vec2 &vector)
{
	GLint location = getUniformLocation(uniformName);
	glUniform2f(location, vector.x, vector.y);
}
void ShaderProgram::sendUniform(const std::string &uniformName, const glm::vec3 &vector)
{
	GLint location = getUniformLocation(uniformName);
	glUniform3f(location, vector.x, vector.y, vector.z);
}
void ShaderProgram::sendUniform(const std::string &uniformName, const glm::vec4 &vector)
{
	GLint location = getUniformLocation(uniformName);
	glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
}
void ShaderProgram::sendUniformMat3(const std::string &uniformName, float *matrix, bool transpose)
{
	GLint location = getUniformLocation(uniformName);
	glUniformMatrix3fv(location, 1, transpose, matrix);
}
void ShaderProgram::sendUniformMat4(const std::string &uniformName, float *matrix, bool transpose)
{
	GLint location = getUniformLocation(uniformName);
	glUniformMatrix4fv(location, 1, transpose, matrix);
}

std::string ShaderProgram::readFile(const std::string &fileName) const
{
	std::ifstream input(fileName);

	if (!input)
	{
		std::cerr << "Shader File not Found" << std::endl;
		return "";
	}

	std::string data(std::istreambuf_iterator<char>(input), (std::istreambuf_iterator<char>()));
	return data;
}

bool ShaderProgram::compileShader(GLuint shader) const
{
	glCompileShader(shader);

	GLint success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	return success == GL_TRUE;
}

void ShaderProgram::outputShaderLog(GLuint shader) const
{
	std::string infoLog;

	GLint infoLen;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

	infoLog.resize(infoLen);

	glGetShaderInfoLog(shader, infoLen, &infoLen, &infoLog[0]);

	std::cout << infoLog << std::endl;
}

void ShaderProgram::outputProgramLog() const
{
	std::string infoLog;

	GLint infoLen;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLen);

	infoLog.resize(infoLen);

	glGetProgramInfoLog(program, infoLen, &infoLen, &infoLog[0]);

	std::cout << infoLog << std::endl;
}