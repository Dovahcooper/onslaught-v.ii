#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

class FileIO
{
public:
	FileIO();
	~FileIO();
	void initialize();
	void sortData();
	void readFile();
	void print();
	//void writeOnFile(string newInputA,string newInputB);
	void writeOnFile(int newInputA, string newInputB);

	vector<int> scoreData;
	vector<string> nameData;	
private:
	fstream file;

};