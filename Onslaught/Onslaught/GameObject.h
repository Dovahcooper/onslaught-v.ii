#pragma once

#include "Mesh.h"
#include "ShaderProgram.h"
#include <GLM/glm.hpp>
#include <GLM\gtx\transform.hpp>
#include <GLM\gtc\type_ptr.hpp>
#include <iostream>
#include <memory>
#include <vector>
#include "Light.h"
#include "Material.h"
#include "animateMorph.h"


//create a vector for the child
//have a function that pushes to that vector
//have stuff for local and world space

//so we create functions for adding and removing children
//then in update we do stuff regarding local and world space....


enum class TextureColour {
	Green, Blue, Red, Arena
};

class GameObject
{
public:
	GameObject();
	~GameObject();

	//create struct for the bounding boxes of GameObjects
	struct AABB
	{
		//position
		float x, y;
		//dimensions
		float width, height;
		//velocity
		float veloX, veloY;
	};

	virtual void oldInitializeObject(std::string fileLocation);
	virtual void oldInitializeObject2(std::string fileLocation);
	virtual void initializeObject(Mesh* meshObject, ShaderProgram * newPhong, bool active = false);
	virtual void initializeObject(std::string fileLocation, TextureColour _color = TextureColour::Red);
	virtual void update(float deltaTime, bool FKoverride = false);
	virtual void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);
	virtual void drawUI(glm::mat4 cameraTransform, glm::mat4 cameraOrtho);
	virtual void drawFromLight(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection);

	void drawChildren(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

	void addChild(GameObject* newChild);
	void removeChild(GameObject* rip);

	virtual void setPosition(glm::vec3 newPosition); //the setters
	void setRotationAngleX(float temp);
	void setRotationAngleY(float temp);
	void setRotationAngleZ(float temp);
	void setRotation(glm::vec3 rotation, float deltaTime);
	void setRotation(glm::vec3 rotation);
	void setScale(float temp); //uniform scale
	void setSpecificScale(float x = 1, float y = 1, float z = 1); //scaling specific values on specific axis

	void setMaterial(std::string diffuse, std::string specular);
	void setMaterial(Material *newMat);

	//void MorphTarget(std::string fileLocation);

	glm::vec3 getPosition(); // the getters
	float getRotationAngleX();
	float getRotationAngleY();
	float getRotationAngleZ();
	glm::mat4 getObjectScale();

	glm::mat4 getLocalMatrix();
	glm::mat4 getLocalToWorldMatrix();
	glm::vec3 getWorldPosition();
	glm::mat4 getWorldRotation();

	std::vector<GameObject*> getChildren();
	GameObject* getChild(int i);
	GameObject* getParent();

	float getScaleX();
	float getScaleY();
	float getScaleZ();

	//int NUM_POINT_LIGHTS;

	//Mesh object;
	//ShaderProgram passThrough;
	glm::mat4 objectTransform;

	ShaderProgram* shader, *lightShader;;
	Material* mat;
	ShaderProgram passThrough;

	int height;
	int width;
	int AABB;
	int NUM_ITERATIONS;
	int toggleTexture;
	float radius;
	Mesh* Object;
	animateMorph morph;
	bool shouldAnimate = false;
	bool animateBackwards = false;
	glm::mat4 localToWorldMatrix;
	glm::mat4 localMatrix;

	// Forward kinematics
	GameObject* parent; // Pointer to parent
	std::vector<GameObject*> children; // Pointers to children
									   //	std::shared_ptr<Mesh> mesh;

protected:
	glm::vec3 localPosition;
	glm::mat4 localRotation;
	glm::mat4 objectScale;
	float rotX, rotY, rotZ;
	float scaleX, scaleY, scaleZ;
	//Mesh* Object;
	//Mesh* Object;
private:



};