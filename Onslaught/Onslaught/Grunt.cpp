#include "Grunt.h"
#include <iostream>

Grunt::Grunt(Mesh *temp)
	:Enemy(temp)
{
	velocity = glm::vec3(0, 0, 0);
	maxForce = 3;
	max_velocity = 20.0f;
	max_seek = 10.0f;
	mass = 1;
	seperationVector;
	areaRadius = 1.5;
	target = glm::vec3(0, 0, 0);
	directionFaced = glm::vec3(0, 0, 0);
	timer = 0;
	health = 30;
	enemyRadius = 1.5;
	desiredSeperation = enemyRadius;

	_enemyType = _Grunt;

	gruntPosition[0] = (glm::vec3(9.5, 9, 0));
	gruntPosition[1] = glm::vec3(-9.5, -9, 0);
	gruntPosition[2] = glm::vec3(-9.5, 9, 0);
	gruntPosition[3] = glm::vec3(9.5, -9, 0);
	gruntPosition[4] = glm::vec3(0, 12.5, 0);
	gruntPosition[5] = glm::vec3(0, -12.5, 0);
	gruntPosition[6] = glm::vec3(-12.5, 0, 0);
	gruntPosition[7] = glm::vec3(12.5, 0, 0);

}

Grunt::~Grunt()
{
	Enemy::~Enemy();
}

glm::vec3 Grunt::computeSeperation(std::vector <Enemy*> theEnemies, int index)
{
	int total_enemies = theEnemies.size(); //we need to iterate through all the grunts
	int neighbourCount = 0; //we use this to 

	for (int i = index; i < total_enemies; i++)
	{
		if (i != index && gridIndex == theEnemies[i]->gridIndex) //making sure we're not seperating from itself
		{
			float lineBetween = sqrt(pow(getPosition().y - theEnemies[i]->getPosition().y, 2) + pow(getPosition().x - theEnemies[i]->getPosition().x, 2)); //THIS IS DISTANCE BETWEEN

			if (lineBetween < (desiredSeperation + theEnemies[i]->enemyRadius)) //If another object falls within this object's range
			{
				//printf("%f", desiredSeperation);
				glm::vec3 difference = (glm::vec3(theEnemies[i]->getPosition().x - getPosition().x, theEnemies[i]->getPosition().y - getPosition().y, 0)); //we get the difference
				seperationVector += difference; //add it to the force
				neighbourCount++; //increment the number of 'neighbours'
			}
		}
	}

	if (neighbourCount == 0)
	{
		return glm::vec3(0); //if there are no neighbours, nothing happens
	}

	seperationVector /= neighbourCount; //why are we doing this
	seperationVector *= -1; //this is the offset
							//seperationVector = glm::normalize(seperationVector); //this is what keeps them apart //do we need this???

	steering = seperationVector - velocity; // to update it...?

	if (glm::length(steering) > max_velocity) //this is clamping the value
		steering = glm::normalize(steering) * max_velocity;

	steering = steering / mass; //the mass affects how much it moves

	return steering; //we return the force
}

glm::vec3 Grunt::enemyArrive()
{
	//float distance = glm::length(target); //the distance is for arrive
	float distance = glm::distance(getPosition(), target);
	target = glm::normalize(target); //normalizing leads to the direction

	if (distance < areaRadius) //if it falls within the area
	{
		float t = distance / areaRadius; //this is the t value--the interval
		float arriveInfluence = glm::mix(0.f, max_seek, t); //it lerps to that point

		target *= arriveInfluence; //this influences how strongly it slows down
	}
	else
		target *= max_seek;

	steering = target - velocity;

	//the mass affects how it moves
	steering = steering / mass;

	return steering;
}

void Grunt::enemyAttack()
{
	float distance = glm::length(target);
	target = glm::normalize(target);

	if (distance < 12 && timer > 1)
	{
		attacking = true;
		firing = true;
		createBullets(target.x, target.y);
		timer = 0;
	}
	else
		firing = false;
}

void Grunt::collision(Player * thePlayer)
{
	Enemy::collision(thePlayer);

	if (Alive == true)
	{
		for (int i = 0; i < thePlayer->playerBullets.size(); i++)
		{
			float distance = glm::distance(getPosition(), thePlayer->playerBullets[i]->getPosition());

			if (distance < 1.3f)
			{
				//Alive = false;
				thePlayer->deleteBullets(i);
				setMaterial(damageTexture);
				dTimer = 0.4;
				health -= thePlayer->bulletDamage;

				if (health <= 0 && tutorial == false)
					thePlayer->score += (5 * thePlayer->scoreMultiplayer);
			}
		}
	}
}

void Grunt::initialize(Mesh* newMesh, Material* newMaterial, int waveStates, int spawnPoint, bool champion)
{
	if (waveStates == 2)
	{
		Spawn = true;
		Alive = false;
		Object = newMesh;
		setMaterial(newMaterial);
		SpawnIndex = spawnPoint;

		setPosition(gruntPosition[spawnPoint]);
	}
	else
		Enemy::initialize(newMesh, newMaterial, spawnPoint, champion);

	velocity = glm::vec3(0, 0, 0);
	maxForce = 2;
	max_velocity = 30.0f;
	max_seek = 10.0f;
	mass = 1;
	seperationVector;
	areaRadius = 9;
	target = glm::vec3(0, 0, 0);
	health = 30;
}

void Grunt::update(float deltaTime, int &numAlive, PowerUp *powerUp)
{
	Enemy::update(deltaTime, numAlive, powerUp);
}

void Grunt::draw(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	Enemy::draw(cameraTransform, cameraProjection, pointLights, directionalLight);
}

/*when should enemy bullets be created....?
when should they be deleted...?
at what intervals?
~Atiya Nova~ ~Constant Vigilance~
THINGS TO DO FOR GAME
-TWEAK ENEMY:
SPAWNING
WAVES
FIRING
ROTATION
SPEED

-WORK ON:
MORPH TARGETS
WIN AND LOSE STATES
COLLISIONS FOR BULLETS FROM ENEMIES
*/