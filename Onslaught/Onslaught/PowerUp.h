#pragma once
#include "GameObject.h"
#include "Player.h"
//kill me

enum PowerUpType
{
	healthP = 0, //health powerup
	ScoreP
};

class PowerUp : public GameObject
{
public:
	PowerUp();
	~PowerUp();

	void initialize(glm::vec3 thePosition, PowerUpType type, Mesh* body, Material* texture);
	void Collision(Player *thePlayer, int &index, int max);

	void update(float deltaTime);
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

	bool Alive;
	int powerupChance;
	float timer;
	bool playSound;
	PowerUpType _type;

private:
};