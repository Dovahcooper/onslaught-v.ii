#include "Boss.h"

Boss::Boss(Mesh * temp, Mesh *weaponBody, Material *mat, ShaderProgram *phong)
	:Enemy(temp)
{
	velocity = glm::vec3(0, 0, 0);
	maxForce = 3;
	max_velocity = 20.0f;
	max_seek = 5.0f;
	mass = 4;
	seperationVector;
	target = glm::vec3(0, 0, 0);
	directionFaced = glm::vec3(0, 0, 0);
	timer = 0;
	health = 600;
	_enemyType = _Boss;
	activated = false;
	_type = false;
	bossRotTimer = 0;
	areaRadius = 15;
	enemyRadius = 15;

	//y and z are flipped here for some reason
	boss_weakPoint.setPosition(glm::vec3(0, 4, 4));
	boss_weakPoint.setScale(0.5);

	for (int i = 0; i < 2; i++)
	{
		regularWeapons.push_back(new Grunt(temp));
		regularWeapons[i]->initializeObject(weaponBody, phong);
		regularWeapons[i]->setRotationAngleX(90);
		regularWeapons[i]->setScale(1);
		regularWeapons[i]->enemyTexture = mat;
		regularWeapons[i]->damageTexture = mat;
		regularWeapons[i]->setMaterial(mat);
		regularWeapons[i]->setBulletTexture(mat);

		avengerWeapons.push_back(new Avengers(temp));
		avengerWeapons[i]->initializeObject(weaponBody, phong);
		avengerWeapons[i]->setRotationAngleX(90);
		avengerWeapons[i]->setScale(1);
		avengerWeapons[i]->enemyTexture = mat;
		avengerWeapons[i]->damageTexture = mat;
		avengerWeapons[i]->setMaterial(mat);
		avengerWeapons[i]->setBulletTexture(mat);

		sprinklerWeapons.push_back(new Sprinkler(temp));
		sprinklerWeapons[i]->initializeObject(weaponBody, phong);
		sprinklerWeapons[i]->setRotationAngleX(90);
		sprinklerWeapons[i]->setScale(1);
		sprinklerWeapons[i]->enemyTexture = mat;
		sprinklerWeapons[i]->damageTexture = mat;
		sprinklerWeapons[i]->setMaterial(mat);
		sprinklerWeapons[i]->setBulletTexture(mat);
	}
}

Boss::~Boss()
{
	delete bossWeapons[0], bossWeapons[1], regularWeapons[0], regularWeapons[1];
}

glm::vec3 Boss::enemyArrive()
{
	//float distance = glm::length(target); //the distance is for arrive
	float distance = glm::distance(getPosition(), target);
	target = glm::normalize(target); //normalizing leads to the direction

	if (distance < areaRadius) //if it falls within the area
	{
		float t = distance / areaRadius; //this is the t value--the interval
		float arriveInfluence = glm::mix(0.f, max_seek, t); //it lerps to that point

		target *= arriveInfluence; //this influences how strongly it slows down
	}
	else
		target *= max_seek;

	steering = target - velocity;

	//the mass affects how it moves
	steering = steering / mass;

	return steering;
}

void Boss::calculateTarget(Player * player1, Player * player2)
{
	if (bossWeapons.size() > 0)
	{
		if (bossWeapons[0]->Alive == true)
			bossWeapons[0]->calculateTarget(player1, player2);

		if (bossWeapons[1]->Alive == true)
			bossWeapons[1]->calculateTarget(player1, player2);
	}
}

void Boss::enemyAttack()
{
	if (_type == 2)
	{
		if (bossWeapons[0]->Alive == true)
			((Grunt*)bossWeapons[0])->enemyAttack();

		if (bossWeapons[1]->Alive == true)
			((Grunt*)bossWeapons[1])->enemyAttack();
	}

	if (_type == 4)
	{
		if (bossWeapons[0]->Alive == true)
			((Avengers*)bossWeapons[0])->enemyAttack();

		if (bossWeapons[1]->Alive == true)
			((Avengers*)bossWeapons[1])->enemyAttack();
	}

	if (_type == 6)
	{
		if (bossWeapons[0]->Alive == true)
			((Sprinkler*)bossWeapons[0])->enemyAttack();

		if (bossWeapons[1]->Alive == true)
			((Sprinkler*)bossWeapons[1])->enemyAttack();
	}
}

void Boss::avengerUpdate(float deltaTime, std::vector<Enemy*> theEnemies, int & numAlive, int i)
{
	for (int j = 0; j < bossWeapons.size(); j++)
	{
		if (((Avengers*)bossWeapons[j])->explosion == false)
		{
			glm::vec3 sep = bossWeapons[j]->computeSeperation(theEnemies, i);
			glm::vec3 arrival = bossWeapons[j]->enemyArrive(); //storing all the values we need in temp variables

			bossWeapons[j]->acceleration = ((2.5f * sep) + (1.5f * arrival));//we need to update the acceleration (Newton's law)
			bossWeapons[j]->velocity = bossWeapons[j]->velocity + (bossWeapons[j]->acceleration * deltaTime); //we need to update velocity (Newton's law)

			glm::vec3 newPosition = theEnemies[i]->getPosition() + (bossWeapons[j]->velocity * deltaTime); //THEN we update position (again, newton's law)

			bossWeapons[j]->setPosition(newPosition); //setting the new position lawd help me	
		}
		else
			((Avengers*)bossWeapons[j])->explode(theEnemies, numAlive);
	}
}

//make it so that it's only the back part that gets hurt for the boss itself
//otherwise, do the collisions with the children, which you declared as a vector within this class
void Boss::collision(Player * thePlayer, GameObject *theHealthBar)
{
	//they die if it collides
	if (bossWeapons.size() > 0)
	{
		bossWeapons[0]->collision(thePlayer);
		bossWeapons[1]->collision(thePlayer);
	}

	for (int i = 0; i < thePlayer->playerBullets.size(); i++)
	{
		float distance1 = glm::distance(glm::vec3(boss_weakPoint.getWorldPosition().x, boss_weakPoint.getWorldPosition().y, 0), thePlayer->playerBullets[i]->getPosition());
		float distance2 = glm::distance(glm::vec3(getPosition().x, getPosition().y, 0), thePlayer->playerBullets[i]->getPosition());

		if (distance2 < 4)
		{
			if (distance1 < 2.f)
			{
				thePlayer->deleteBullets(i);
				health -= thePlayer->bulletDamage;
				theHealthBar->setSpecificScale(health, 5, getScaleZ());
				dTimer = 0.4;
				setMaterial(damageTexture);
			}
			else
			{
				thePlayer->deleteBullets(i);
				health -= thePlayer->bulletDamage*0.5;
				theHealthBar->setSpecificScale(health, 5, getScaleZ());
			}

			if (health <= 0)
				thePlayer->score += (100 * thePlayer->scoreMultiplayer);

		}
	}
}

//set up everything for the boss. The spawn point should be the center, I think.
void Boss::initialize(Mesh * newMesh, Material * newMaterial, int type)
{
	Spawn = true;
	Alive = false;

	Object = newMesh;
	setMaterial(newMaterial);

	if (type == 2) //regular
	{
		bossWeapons.push_back(regularWeapons[0]);
		bossWeapons.push_back(regularWeapons[1]);
	}

	else if (type == 4) //avenger
	{
		bossWeapons.push_back(avengerWeapons[0]);
		bossWeapons.push_back(avengerWeapons[1]);
	}
	else if (type == 6) //boss
	{
		bossWeapons.push_back(sprinklerWeapons[0]);
		bossWeapons.push_back(sprinklerWeapons[1]);
	}

	_type = type;

	bossWeapons[0]->setPosition(glm::vec3(4.5, 0, 0));
	bossWeapons[1]->setPosition(glm::vec3(-4.5, 0, 0));
	bossWeapons[0]->setBulletTexture(newMaterial);
	bossWeapons[1]->setBulletTexture(newMaterial);
	bossWeapons[0]->setRotation(glm::vec3(0, 90, 0));
	bossWeapons[1]->setRotation(glm::vec3(0, 90, 0));
	bossWeapons[0]->setScale(1);
	bossWeapons[1]->setScale(1);

	setPosition(glm::vec3(0, 0, 0));

	velocity = glm::vec3(0, 0, 0);
	maxForce = 2;
	max_velocity = 30.0f;
	max_seek = 10.0f;
	mass = 1;
	seperationVector;
	areaRadius = 9;
	target = glm::vec3(0, 0, 0);
	health = 600;

	bossWeapons[0]->initialize(newMesh, newMaterial, 0, true);
	bossWeapons[1]->initialize(newMesh, newMaterial, 0, true);

	activated = true;

	addChild(&boss_weakPoint);

}

void Boss::update(float deltaTime, int & numAlive, PowerUp *powerUp)
{
	Enemy::update(deltaTime, numAlive, powerUp);

	setRotationAngleY((getRotationAngleY() + 20 * deltaTime));

	boss_weakPoint.update(deltaTime, numAlive);

	if (health > 0 && bossWeapons.size() > 0)
	{
		bossWeapons[0]->localToWorldMatrix = getLocalToWorldMatrix() * bossWeapons[0]->localMatrix;
		bossWeapons[0]->update(deltaTime, numAlive, NULL, true);
		bossWeapons[0]->updateBullets(deltaTime);

		bossWeapons[1]->localToWorldMatrix = getLocalToWorldMatrix() * bossWeapons[1]->localMatrix;
		bossWeapons[1]->update(deltaTime, numAlive, NULL, true);
		bossWeapons[1]->updateBullets(deltaTime);
	}

	if (health <= 0 && Alive == true)
	{
		Alive = false;
		Spawn = false;

		bossWeapons[0]->Alive = false;
		bossWeapons[1]->Alive = false;

		removeChild(&boss_weakPoint);

		bossWeapons.pop_back();
		bossWeapons.pop_back();
	}
}

void Boss::draw(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	if (Alive == true)
	{
		Enemy::draw(cameraTransform, cameraProjection, pointLights, directionalLight);
		//boss_weakPoint.draw(cameraTransform, cameraProjection, pointLights, directionalLight);

		for (int i = 0; i < bossWeapons.size(); i++)
		{
			bossWeapons[i]->draw(cameraTransform, cameraProjection, pointLights, directionalLight);
		}
	}
}
