
#define _CRT_SECURE_NO_WARNINGS
#include "Mesh.h"
#include <vector>
#include <fstream>
#include <iostream>

#define BUFFER_OFFSET(i) ((char*)0 + (i))

struct MeshFace
{
	MeshFace()
	{
		vertices[0] = vertices[1] = vertices[2] = 0;
		textureUVs[0] = textureUVs[1] = textureUVs[2] = 0;
		normals[0] = normals[1] = normals[2] = 0;
	}
	MeshFace(unsigned int v1, unsigned int v2, unsigned int v3,
		unsigned int t1, unsigned int t2, unsigned int t3,
		unsigned int n1, unsigned int n2, unsigned int n3)
	{
		vertices[0] = v1;
		vertices[1] = v2;
		vertices[2] = v3;

		textureUVs[0] = t1;
		textureUVs[1] = t2;
		textureUVs[2] = t3;

		normals[0] = n1;
		normals[1] = n2;
		normals[2] = n3;
	}

	unsigned int vertices[3];
	unsigned int textureUVs[3];
	unsigned int normals[3];
};

Mesh::Mesh()
{

}

Mesh::~Mesh()
{

}

void Mesh::draw()
{
	vbo.draw();
}

bool Mesh::loadFromFile(const std::string &file)
{
	std::ifstream input;
	input.open(file);

	std::string inputString;

	//index / face data
	std::vector<MeshFace> faceData;

	while (std::getline(input, inputString))
	{
		if (inputString[0] == '#')
		{
			///this is a comment, skip this shieeet
			continue;
		}
		else if (inputString[0] == 'v')
		{
			if (inputString[1] == ' ')
			{
				///vertex data
				glm::vec3 temp;
				std::sscanf(inputString.c_str(), "v %f %f %f",
					&temp.x, &temp.y, &temp.z);
				vertexData.push_back(temp);
			}
			else if (inputString[1] == 't')
			{
				///UV data
				glm::vec2 temp;
				std::sscanf(inputString.c_str(), "vt %f %f",
					&temp.x, &temp.y);
				textureData.push_back(temp);
			}
			else if (inputString[1] == 'n')
			{
				///normal data
				glm::vec3 temp;
				std::sscanf(inputString.c_str(), "vn %f %f %f",
					&temp.x, &temp.y, &temp.z);
				normalData.push_back(temp);
			}
		}
		else if (inputString[0] == 'f')
		{
			///face data
			MeshFace temp;

			std::sscanf(inputString.c_str(), "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textureUVs[0], &temp.normals[0],
				&temp.vertices[1], &temp.textureUVs[1], &temp.normals[1],
				&temp.vertices[2], &temp.textureUVs[2], &temp.normals[2]);

			faceData.push_back(temp);


		}
		else
		{
			std::cerr << "Line not recognized, skipping: "
				<< std::endl;
			continue;
		}
	}
	input.close();

	//unpack .obj data
	unpackedVertexData.reserve(vertexData.size());
	unpackedTextureData.reserve(textureData.size());
	unpackedNormalData.reserve(normalData.size());

	for (unsigned int i = 0; i < faceData.size(); i++)
	{
		for (unsigned int j = 0; j < 3; j++)
		{
			unpackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].x);
			unpackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].y);
			unpackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].z);


			unpackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].x);
			unpackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].y);


			unpackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].x);
			unpackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].y);
			unpackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].z);
		}
	}

	for (unsigned i = 0; i < faceData.size(); i++)
	{
		//tangent source referenced: http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/#tangent-and-bitangent

		//gather vertex data for face
		vert1 = vertexData[faceData[i].vertices[0] - 1];
		vert2 = vertexData[faceData[i].vertices[1] - 1];
		vert3 = vertexData[faceData[i].vertices[2] - 1];

		//gather texture data for face
		tex1 = textureData[faceData[i].textureUVs[0] - 1];
		tex2 = textureData[faceData[i].textureUVs[1] - 1];
		tex3 = textureData[faceData[i].textureUVs[2] - 1];

		edge1 = vert2 - vert1;
		edge2 = vert3 - vert1;

		subtex1 = tex2 - tex1;
		subtex2 = tex3 - tex1;

		float tangentCalc = 1.0f / (subtex1.x * subtex2.y - subtex1.y * subtex2.x);
		tangent = tangentCalc * (edge1 * subtex2.y - edge2 * subtex1.y);
		biTangent = tangentCalc* (edge2 * subtex1.x - edge1 * subtex2.x);

		for (unsigned j = 0; j < 3; j++)
		{
			unpackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].x);
			unpackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].y);
			unpackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].z);

			unpackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].x);
			unpackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].y);

			unpackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].x);
			unpackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].y);
			unpackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].z);

			unpackedTangentData.push_back(tangent.x);
			unpackedTangentData.push_back(tangent.y);
			unpackedTangentData.push_back(tangent.z);
			
			unpackedBiTangentData.push_back(biTangent.x);
			unpackedBiTangentData.push_back(biTangent.y);
			unpackedBiTangentData.push_back(biTangent.z);
		}
	}

	numFaces = faceData.size();

	numVertices = numFaces * 3;

	// send data to openGL
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vboVertices);
	glGenBuffers(1, &vboUVs);
	glGenBuffers(1, &vboNormals);
	glGenBuffers(1, &vboTangents);
	glGenBuffers(1, &vboBiTangent);

	glBindVertexArray(vao);

	glEnableVertexAttribArray(0);  //Vertices
	glEnableVertexAttribArray(1); //UVs
	glEnableVertexAttribArray(2);//Normies
	glEnableVertexAttribArray(3); //Tangents
	glEnableVertexAttribArray(4); //BiTangents

	//vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboVertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unpackedVertexData.size(),
		&unpackedVertexData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE,
		sizeof(float) * 3, BUFFER_OFFSET(0));


	glBindBuffer(GL_ARRAY_BUFFER, vboUVs);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unpackedTextureData.size(),
		&unpackedTextureData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE,
		sizeof(float) * 2, BUFFER_OFFSET(0));


	glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unpackedNormalData.size(),
		&unpackedNormalData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 3, GL_FLOAT, GL_FALSE,
		sizeof(float) * 3, BUFFER_OFFSET(0));

	//need to check for similar vertices later on <- return to
	glBindBuffer(GL_ARRAY_BUFFER, vboTangents);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* unpackedTangentData.size(),
		&unpackedTangentData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)3, 3, GL_FLOAT, GL_FALSE,
		sizeof(float) * 3, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, vboBiTangent);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* unpackedBiTangentData.size(),
		&unpackedBiTangentData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)4, 3, GL_FLOAT, GL_FALSE,
		sizeof(float) * 3, BUFFER_OFFSET(0));

	//clean-up
	glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	glBindVertexArray(GL_NONE);

	return true;


}

void Mesh::unload()
{
	glDeleteBuffers(1, &vboNormals);
	glDeleteBuffers(1, &vboUVs);
	glDeleteBuffers(1, &vboVertices);

	glDeleteVertexArrays(1, &vao);

	vboNormals = NULL;
	vboUVs = NULL;
	vboVertices = NULL;
	vao = NULL;

	numFaces = 0;
	numVertices = 0;
}

unsigned int Mesh::getNumFaces()
{
	return numFaces;
}

unsigned int Mesh::getNumVertices()
{
	return numVertices;
}

void Mesh::createVBO()
{
	unsigned int numTris = (unsigned int)(vertexData.size()) / 3;

	if (vertexData.size() > 0)
	{
		AttributeDescriptor verts;
		verts.attributeLocation = AttributeLocations::VERTEX;
		verts.attributeName = "vertex";
		verts.data = &vertexData[0];
		verts.elementSize = sizeof(float);
		verts.elementType = GL_FLOAT;
		verts.numElements = numTris * 3 * 3;
		verts.numElementsPerAttrib = 3;
		vbo.addAttributeArray(verts);
	}
	if (textureData.size() > 0)
	{
		AttributeDescriptor texCoord;
		texCoord.attributeLocation = AttributeLocations::TEX_COORD;
		texCoord.attributeName = "uv";
		texCoord.data = &textureData[0];
		texCoord.elementSize = sizeof(float);
		texCoord.elementType = GL_FLOAT;
		texCoord.numElements = numTris * 3 * 2;
		texCoord.numElementsPerAttrib = 2;
		vbo.addAttributeArray(texCoord);
	}
	if (normalData.size() > 0)
	{
		AttributeDescriptor norms;
		norms.attributeLocation = AttributeLocations::NORMAL;
		norms.attributeName = "normal";
		norms.data = &normalData[0];
		norms.elementSize = sizeof(float);
		norms.elementType = GL_FLOAT;
		norms.numElements = numTris * 3 * 3;
		norms.numElementsPerAttrib = 3;
		vbo.addAttributeArray(norms);
	}

	vbo.createVBO(GL_STATIC_DRAW);
}