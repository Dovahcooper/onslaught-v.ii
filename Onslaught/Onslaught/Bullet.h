#pragma once
#include "GameObject.h"

class Bullet : public GameObject
{
public:
	Bullet(float _dx, float _dy, glm::vec3 pos, Mesh* bulletMesh, ShaderProgram * phong, Material *mat, float newVelocity);
	~Bullet();

	void update(float dt);

	void setPosition(float &dt);

private:
	float velocity;
	glm::vec3 direction;
	glm::vec3 velocityVec;
};