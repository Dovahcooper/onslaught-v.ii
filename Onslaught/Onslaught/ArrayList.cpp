#include "ArrayList.h"

template<class elemType>
void arrayListType<elemType>::ArrayListType()
{
	length = 0;
}

/*template<class elemType>
void arrayListType<elemType>::~ArrayListType()
{
	length = 0;
}*/

template<class elemType>
bool arrayListType<elemType>::isEmpty()
{
	return (length == 0);
}

template<class elemType>
bool arrayListType<elemType>::isFull()
{
	return (length == maxSize);
}

template<class elemType>
int arrayListType<elemType>::listSize()
{
	return length;
}

template<class elemType>
int arrayListType<elemType>::maxListSize()
{
	return maxSize;
}

template<class elemType>
int arrayListType<elemType>::seqSearch(const elemType & item)
{
	int loc;
	bool found = false;
	for (loc = 0; loc < length; loc++)
	{
		if (list[loc] == item)
		{
			found = true;
			break;
		}
	}
	if (found) return loc;
	else return-1;
}

template<class elemType>
void arrayListType<elemType>::insertAt(int location, const elemType & insertItem)
{
	if (location < 0 || location >= maxSize)
	{
		printf("Out of range\n");
	}
	else
	{
		if (length >= maxSize) //if the list is full
			printf("Can't insert anymore\n");
		else
		{
			for (int i = length; i > location; i--)
			{
				list[i] = list[i - 1]; //this may be incorrect idkkkkk
				list[location] = insertItem;
				length++;
			}
		}
	}
}

template<class elemType>
void arrayListType<elemType>::insertEnd(const elemType & insertItem)
{
	if (length >= maxSize) //if the list is full
		printf("Sry the list is full\n");
	else
	{
		list[length] = insertItem; //inserts at the end
		length++;
	}
}

template<class elemType>
void arrayListType<elemType>::retrieveAt(int location, elemType & retItem)
{
	if (location < 0 || location >= length)
	{
		printf("Sry but out of range\n");
	}
	else
	{
		retItem = list[location];
	}
}

template<class elemType>
void arrayListType<elemType>::removeAt(int location)
{
	if (location < 0 || location >= length)
	{
		printf("This is out of range sry\n");
	}
	else
	{
		for (int i = location; i < length - 1; i++)
			list[i] = list[i + 1];
		length--;
	}
}

template<class elemType>
void arrayListType<elemType>::remove(const elemType & removeItem)
{
	int loc;
	if (length == 0)
		printf("This is empty so we cannot delete\n");
	else
	{
		loc = seqSearch(removeItem);
		if (loc != 1)
			removeAt(loc);
		else
			printf("not in the else\n");
	}
}