#include "Player.h"
#include <AnimationMath.h>

#define PI 3.141592654


//no longer loading in meshes during the constructor--however, we can load in the regular mesh for the screen
//but dont use the filepath, use pointers 
//instead of the old way load the actual meshes from the load meshes function
//things like the colour can still be set tho, except instead of passing through texture colour maybe pass in the material pointer
//load in the body mesh and then the turret mesh in the load meshes function like normal
//it'll be passing in a 'turret type' and 'body type' as well, and you can use that to determine the stats
//the types are: basic, speed, heavy for both the turrets and the bodies
Player::Player(int &_index, Material *texture, Material *body)
	: currentFrame(0),
	nextFrame(1)
{
	playerTurret = new GameObject();
	playerTurret->setScale(0.5);
	playerTurret->setRotationAngleY(90);
	playerTurret->setRotationAngleX(90);
	playerTurret->setMaterial(texture);

	normal = new Material();
	damage = new Material();

	bulletMat = new Material();

	splinePos[0] = glm::vec3(28, -5, 0);
	splinePos[1] = glm::vec3(19, 16, 0);
	splinePos[2] = glm::vec3(0, 24, 0);
	splinePos[3] = glm::vec3(-19, 16, 0);
	splinePos[4] = glm::vec3(-28, -5, 0);
	splinePos[5] = glm::vec3(-19, -18, 0);
	splinePos[6] = glm::vec3(0, -29, 0);
	splinePos[7] = glm::vec3(19, -18, 0);
	splinePos[8] = splinePos[0];

	bulletMat = texture;
	normal = body;
	setMaterial(body);

	time = 0.f;

	setRotationAngleX(90);
	setRotationAngleY(90);
	setRotationAngleZ(0);

	rotation = glm::vec3(90.f, 90.f, 0.f);
	velocity = glm::vec3(0.0f, 0.0f, 0.f);
	acceleration = 0.f;
	maxVelocity = 0.f;

	health = 100;
	Alive = true;

	height = 50;
	width = 50;
	radius = 2.f;

	index = _index;
	fireRate = 0.f;

	bulletMesh = new Mesh();
	dTimer = 0;

	bulletMesh->loadFromFile("meshes/bullet.obj");

	bulletVelocity = 20.0f;
	bulletScale = 1;

	scoreMultiplayer = 1;
	scoreTimer = 0;
	maxHealth = 0;

	hit = false;
	firing = false;
	isMoving = false;

	invincibilityFrames = 0;
	yPressed = false;
}

Player::~Player()
{
	//	bulletMesh->unload;
	//phong->unload;
	delete bulletMesh, bulletMat;
}

void Player::update(float deltaTime, Input::XBoxInput &controller, GameStates &gameStates)
{
	if (scoreTimer > 0)
	{
		scoreTimer -= 1 * deltaTime;
		if (scoreTimer <= 0)
		{
			scoreTimer = 0;
			scoreMultiplayer = 1;
		}
	}
	//int p0, p1, p2, p3;

	if (invincibilityFrames > 0)
		invincibilityFrames -= deltaTime;

	if (invincibilityFrames < 0)
		invincibilityFrames = 0;

	GameObject::update(deltaTime);
	playerTurret->setPosition(glm::vec3(getPosition().x, getPosition().y, 0.1));
	playerTurret->update(deltaTime);

	//catmullTime += deltaTime;

	controller.GetSticks(index, lStick, rStick);

	if (gameStates != (GameStates)5)
		Player::setPosition(lStick, deltaTime);

	if (rStick.xAxis != 0 || rStick.yAxis != 0)
	{
		Player::setRotation(rStick.xAxis, rStick.yAxis);
		time += deltaTime;
	}
	if (rStick.xAxis == 0 && rStick.yAxis == 0)
	{
		time = 0.f;
		firing = false;
	}
	if (gameStates == (GameStates)5 && controller.GetButton(index, Input::Button::A))
	{
		gameStates == (GameStates)6;
	}
#ifdef _DEBUG
	int state = (int)gameStates;
	if (controller.GetButton(index, Input::Button::Y) == true && yPressed == false)
	{
		yPressed = true;
	}
	else if (!controller.GetButton(index, Input::Button::Y) && yPressed == true)
	{
		if (state < 6)
		{
			state++;
			gameStates = GameStates(state);
		}
		yPressed = false;
	}
#endif
	setRotationAngleX(rotation.x);
	setRotationAngleY(rotation.y);
	setRotationAngleZ(rotation.z);

	if (health <= 0)
	{
		Alive = false;
		hit = false;
		dTimer = 0;
	}
}

void Player::draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight)
{
	GameObject::draw(cameraTransform, cameraProjection, pointLights, directionalLight);

	for (int i = 0; i < playerBullets.size(); i++)
	{
		playerBullets[i]->draw(cameraTransform, cameraProjection, pointLights, directionalLight);
	}

	playerTurret->draw(cameraTransform, cameraProjection, pointLights, directionalLight);
}

void Player::menuUpdate(float deltaTime)
{
	int p0, p1, p2, p3;

	GameObject::update(deltaTime);
	playerTurret->update(deltaTime);

	catmullTime += deltaTime;

	if (catmullTime >= 1.f)
	{
		catmullTime = 0.f;
		currentFrame++;
		if (currentFrame == 8)
		{
			currentFrame = 0;
		}
		nextFrame = currentFrame + 1;
	}

	if (currentFrame == 0)
	{
		p0 = currentFrame;
		p1 = currentFrame;
		p2 = nextFrame;
		p3 = nextFrame + 1;
	}
	else if (currentFrame == 7)
	{
		p0 = currentFrame - 1;
		p1 = currentFrame;
		p2 = nextFrame;
		p3 = 1;
	}
	else
	{
		p0 = currentFrame - 1;
		p1 = currentFrame;
		p2 = nextFrame;
		p3 = nextFrame + 1;
	}


	GameObject::setPosition(Math::catmullUMP(splinePos[p0], splinePos[p1],
		splinePos[p2], splinePos[p3], catmullTime));

	float angle;
	glm::vec3 origin(0.f, 0.f, 0.f);

	glm::vec3 temp = origin - getPosition();

	angle = acos(temp.x / glm::length(temp));

	setRotationAngleX(90.f);

	if (temp.y > 0)
	{
		setRotationAngleY(0.f + glm::degrees(angle));
		rotation = glm::vec3(90.f, 0.f + glm::degrees(angle), 0.f);
	}
	else
	{
		setRotationAngleY(0.f - glm::degrees(angle));
		rotation = glm::vec3(90.f, 0.f - glm::degrees(angle), 0.f);
	}
	setRotationAngleZ(0.f);
	playerTurret->setRotation(glm::vec3(getRotationAngleX(), getRotationAngleY(), getRotationAngleZ()), deltaTime);
	playerTurret->setPosition(getPosition());
}

void Player::createBullets(float &_x, float &_y)
{
	playerBullets.push_back(new Bullet(_x, _y, getPosition(), bulletMesh, shader, bulletMat, bulletVelocity));
	playerBullets.back()->setScale(bulletScale);
}

void Player::deleteBullets(int index)
{
	delete playerBullets[index];
	playerBullets.erase((playerBullets.begin() + index));
	//delete playerBullets[index];
}

void Player::updateBullets(float deltaTime)
{
	for (int i = 0; i < playerBullets.size(); i++)
	{
		playerBullets[i]->update(deltaTime);
		if (playerBullets[i]->getPosition().x > 30 || playerBullets[i]->getPosition().x < -30 || playerBullets[i]->getPosition().y < -30 || playerBullets[i]->getPosition().y > 30) deleteBullets(i);
	}
}

void Player::setPosition(Input::Stick &lStik, float &dt)
{
	glm::vec3 velocityVec(0.f, 0.f, 0.f);

	float magnitude = glm::length(glm::vec3(lStik.xAxis, lStik.yAxis, 0));

	float angle = acos((lStik.xAxis / magnitude) / 1);

	if (lStik.xAxis != 0 || lStik.yAxis != 0)
	{
		isMoving = true;
		if (lStik.yAxis < 0) // this controls the player movement
		{
			velocityVec = glm::vec3(velocity.x * cos(angle), -1 * velocity.y * sin(angle), 0);
			test = 1;
		}
		else
		{
			velocityVec = glm::vec3(velocity.x * cos(angle), velocity.y * sin(angle), 0);
			test = 2;
		}

		//if (lastAngle <= angle)
		//{
		if (velocity.x < maxVelocity)
			velocity += acceleration * dt;
		//}
		/*else
		{
		if (velocity.x > 0)
		velocity -= acceleration * dt;
		}*/

		lastAngle = angle;
	}
	else
	{
		isMoving = false;
		if (velocity.x > 0)
		{
			velocity -= acceleration * (dt * 4);
			if (test == 1)
			{
				velocityVec = glm::vec3(velocity.x * cos(lastAngle), -1 * velocity.y * sin(lastAngle), 0);
			}
			else if (test == 2)
			{
				velocityVec = glm::vec3(velocity.x * cos(lastAngle), velocity.y * sin(lastAngle), 0);
			}
		}
	}

	GameObject::setPosition(getPosition() + (velocityVec * dt));
}

void Player::setRotation(float &_x, float &_y)
{
	float angle = acos(_x / glm::length(glm::vec2(_x, _y)) / 1);

	float x, y;

	x = _x / glm::length(glm::vec2(_x, _y));
	y = _y / glm::length(glm::vec2(_x, _y));

	if (time >= (1 / fireRate) || time == 0.f)
	{
		time = 0.f;
		createBullets(x, y);
		firing = true;
	}

	//if it's just createbullets, it's frozen until you release
	//only greater that 0.5 means that it doesnt always shoot out bullets

	if (_y < 0) // this is the object mesh rotating
	{
		playerTurret->setRotationAngleX(90);
		playerTurret->setRotationAngleY(-90 - glm::degrees(angle));
		playerTurret->setRotationAngleZ(0);
		rotation = glm::vec3(90.f, -90, 0);
	}
	else
	{
		playerTurret->setRotationAngleX(90);
		playerTurret->setRotationAngleY(-90 + glm::degrees(angle));
		playerTurret->setRotationAngleZ(0);
		rotation = glm::vec3(90.f, -90, 0);
	}
}

void Player::damageTaken(float temp)
{
	health -= temp;
}

void Player::setDamage(Material * temp)
{
	damage = temp;
}

void Player::loadMeshes(Mesh * body, Mesh * turret, CustomizationType _body, CustomizationType _turret, ShaderProgram * shader, Material *matBody, Material *matTurret)
{
	initializeObject(body, shader);
	playerTurret->initializeObject(turret, shader);
	playerTurret->setMaterial(matTurret);
	normal = matBody;
	setMaterial(matBody);

	if (_body == CustomizationType::Basic)
	{
		acceleration = 11.5f;
		maxVelocity = 20.f;
		health = 100;
		maxHealth = 100;
	}
	else if (_body == CustomizationType::Fast)
	{
		acceleration = 15.f;
		maxVelocity = 25.f;
		health = 70;
		maxHealth = 70;
	}
	else if (_body == CustomizationType::Heavy)
	{
		acceleration = 8.f;
		maxVelocity = 15.f;
		health = 130;
		maxHealth = 130;
	}

	if (_turret == CustomizationType::Basic)
	{
		bulletVelocity = 35.f;
		bulletDamage = 10.f;
		bulletScale = 12;
		fireRate = 5;
	}
	else if (_turret == CustomizationType::Fast)
	{
		bulletVelocity = 35.f;
		bulletDamage = 5.f;
		bulletScale = 10;
		fireRate = 8;
	}
	else if (_turret == CustomizationType::Heavy)
	{
		bulletVelocity = 35.f;
		bulletDamage = 15.f;
		bulletScale = 14;
		fireRate = 2;
	}
}

void Player::reset(glm::vec3 resetPosition)
{
	playerTurret->setScale(0.5);
	playerTurret->setRotationAngleY(90);
	playerTurret->setRotationAngleX(90);

	playerBullets.clear();

	time = 0.f;
	dTimer = 0;
	firing = false;

	setRotationAngleX(90);
	setRotationAngleY(90);
	setRotationAngleZ(0);

	rotation = glm::vec3(90.f, 90.f, 0.f);
	velocity = glm::vec3(0.0f, 0.0f, 0.f);
	acceleration = 11.f;

	health = 100;
	Alive = true;

	height = 50;
	width = 50;
	radius = 2.f;

	score = 0;
	morph.currentFrame = 0;
	morph.nextFrame = 1;

	scoreMultiplayer = 1;
	scoreTimer = 0;

	bulletVelocity = 20.0f;
	GameObject::setPosition(resetPosition);
}

bool Player::getAlive()
{
	return Alive;
}

void Player::setHealth(float temp)
{
	health = temp;
}

void Player::setAlive(bool temp)
{
	Alive = temp;
}

float Player::getHealth()
{
	return health;
}

