#include "Material.h"
#include <iostream>

Material::Material()
{

}

Material::Material(std::string diffusePath, std::string specularPath, float specularExp, glm::vec3 &hue)
	: specularExponent(specularExp), hue(hue)
{
	loadTexture(TextureType::Diffuse, diffusePath);
	loadTexture(TextureType::Specular, specularPath);
}

Material::~Material()
{

}

void Material::loadTexture(TextureType type, std::string texFile, bool _flip)
{
	switch (type)
	{
	case Diffuse:
		if (!diffuse.load(texFile, _flip))
		{
			system("pause");
			exit(0);
		}
		break;
	case Specular:
		break;
	case Normal:
		if (!normal.load(texFile, _flip))
		{
			system("pause");
			exit(0);
		}
		break;
	default:
	{
		std::cout << "Error-Texture type not found" << type << std::endl;
		system("pause");
		exit(0);
	}
	break;
	}
}