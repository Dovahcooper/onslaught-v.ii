//Designed by Matthew Holt 100622906

#include "SoundChannel.h"

//initializes the velocity and position before the glut loop
SoundSystem::SoundSystem()
{
	playing = false;
	channelPos = { 0.f, 0.f, -1.f };
	channelVel = { 0.f, 0.f, 0.f };
}

SoundSystem::~SoundSystem()
{
	delete channel, sound;
}

//updates the playing status as well as resets the 3D attributes every frame
void SoundSystem::update(float dt, FMOD::System &sys, bool _playing)
{
	playing = _playing;

	setPosition(channelPosition + (channelVelocity * dt));

	result = channel->set3DAttributes(&channelPos, &channelVel);
	FmodErrorCheck(result);

	//setRolloff(_roll);
}
//sets the position using a glm vector and translates it into an
//FMOD vector for use in the attributes of the sound
void SoundSystem::setPosition(glm::vec3 &position)
{
	FMOD_VECTOR temp = { position.x, position.y, -position.z };
	channelPos = temp;
	channelPosition = position;
}
glm::vec3 SoundSystem::getPosition()
{
	return channelPosition;
}
//sets the velocity for a little bit of doppler
//during runtime. Panning does not create a doppler
//effect
void SoundSystem::setVelocity(glm::vec3 &vel)
{
	FMOD_VECTOR temp = { vel.x, vel.y, -vel.z };
	channelVel = temp;
	channelVelocity = vel;
}
glm::vec3 SoundSystem::getVelocity()
{
	return channelVelocity;
}
//sets the channel volume so as to be heard without
//requiring an excessive computer volume
void SoundSystem::setVolume(float volume)
{
	channel->setVolume(volume);
}

//sets the rolloff values between linear rolloff and
//Inverse Rollof, which I assume is the logarithmic one we
//are supposed to be using here
void SoundSystem::setRolloff(FMOD_MODE _rolloff)
{
	result = sound->setMode(_rolloff);
	FmodErrorCheck(result);
}

void SoundSystem::setMode(FMOD_MODE _mode)
{
	result = sound->setMode(_mode);
	FmodErrorCheck(result);
}

void SoundSystem::setFMODMode(FMOD_MODE c)
{
	
	result = channel->setMode(c);
	FmodErrorCheck(result);
	
}

//loads the sound from a user specified file as well as sets the maximum
//and minimum distance it can be heard at. Returns a regular boolean value
//which will break the program if the loading fails
bool SoundSystem::createSound(std::string soundFile, FMOD::System &system)
{
	result = system.createSound(soundFile.c_str(), FMOD_3D, 0, &sound);
	FmodErrorCheck(result);
	if (result != FMOD_OK) return false;

	return true;
}

void SoundSystem::setPlaying(bool temp)
{
	playing = temp;
}

bool SoundSystem::getPlaying()
{
	return playing;
}
