#pragma once
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <string>


class ShaderProgram
{
public:
	ShaderProgram();
	~ShaderProgram();

	//loads vertex and fragment shaders
	bool load(const std::string &vertFile, const std::string &fragFile);
	bool loadGeo(const std::string &vertFile, const std::string &fragFile, const std::string &geoFile);
	///checks if shaders loaded
	bool isLoaded() const;
	///unload Shader
	void unload();
	//links shaders
	bool linkProgram();

	//use the shader
	void bind();
	///stop using shader / detach
	void unbind();

	//adds an attribute to the program
	///requires relinkin before openGL will register the change
	void addAttribute(unsigned int index, const std::string &attribName);
	///returns -1 if attribute doesn't exist
	int getAttribLocation(const std::string &attribName);

	//retrieve unifor m location
	///returns -1 if not exist
	int getUniformLocation(const std::string &uniformName);

	//send uniform data to shaders
	void sendUniform(const std::string &uniformName, int integer);
	///just overloading for different data types
	void sendUniform(const std::string &uniformName, unsigned int unsignedInteger);
	void sendUniform(const std::string &uniformName, float scalar);
	void sendUniform(const std::string &uniformName, const glm::vec2 &vector);
	void sendUniform(const std::string &uniformName, const glm::vec3 &vector);
	void sendUniform(const std::string &uniformName, const glm::vec4 &vector);
	///sending Matrices, rather than individual data types
	void sendUniformMat3(const std::string &uniformName, float *matrix, bool transpose);
	void sendUniformMat4(const std::string &uniformName, float *matrix, bool transpose);

protected:

private:
	bool loaded = false;
	GLuint vertexShader = 0;
	GLuint geoShader = 0;
	GLuint fragShader = 0;
	GLuint program = 0;

	std::string readFile(const std::string &fileName) const;
	
	bool compileShader(GLuint shader) const;

	void outputShaderLog(GLuint shader) const;

	void outputProgramLog() const;

};