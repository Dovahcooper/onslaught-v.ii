#include "Timer.h"
#include <GL/glut.h>

Timer::Timer()
{
	previousTime = glutGet(GLUT_ELAPSED_TIME);
	tick();
}

Timer::~Timer()
{

}
//update the timer
float Timer::tick()
{
	currentTime = glutGet(GLUT_ELAPSED_TIME);
	elapsedTime = currentTime - previousTime;
	previousTime = currentTime;
	return elapsedTime;
}

//return delta time in milliseconds
float Timer::getElapsedTimeMS()
{
	return elapsedTime;
}

//return delta time in seconds
float Timer::getElapsedTimeSeconds()
{
	return elapsedTime / 1000.f;
}
//gets current time
float Timer::getCurrentTime()
{
	return currentTime;
}