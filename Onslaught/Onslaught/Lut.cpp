
#include "Lut.h"

Lut::Lut()
{
}

Lut::~Lut()
{
	unbind();
}

void Lut::load(string fileName)
{
	string name = LUTpath + fileName;
	ifstream LUTfile(name.c_str());

	while (!LUTfile.eof()) 
	{
		string LUTline;
		getline(LUTfile, LUTline);
		if (LUTline.empty())
			continue;
		glm::vec3 line;
		if (sscanf_s(LUTline.c_str(), "%f %f %f", &line.x, &line.y, &line.z) == 3) LUT.push_back(line);
	}

	glEnable(GL_TEXTURE_3D);
	glGenTextures(1, &texture3D);
	glBindTexture(GL_TEXTURE_3D, texture3D);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, LUTsize, LUTsize, LUTsize, 0, GL_RGB, GL_FLOAT, &LUT[0]);
}

void Lut::setLutSize(float s)
{
	LUTsize = s;
}

void Lut::bind()
{
	if (!isBinded)
	{

	isBinded = true;
	glBindTexture(GL_TEXTURE_3D, texture3D);

	}
	
}

void Lut::unbind()
{
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
	isBinded = false;
}
