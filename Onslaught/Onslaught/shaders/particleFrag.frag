#version 420

struct Material
{
	sampler2D diffuse;
	sampler2D specular;

	vec3 hue;

	float specularExponent;
};

uniform Material material;


in vec3 normalG;
in vec3 texCoordG;
in vec4 colourG;
	
// Multiple render targets!
layout(location = 0) out vec4 FragColor;

uniform vec3 partColour;

void main()
{
	//float mixNum = sin(blend);
	vec4 color1 = texture(material.diffuse, texCoordG.xy);
	vec3 color2 = vec3(0.0, 1.0, 0.0);
	//FragColor = vec4(mix(color1, color2, mixNum), 1.0);
	FragColor = color1;
}