//Tangent reference: http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/#tangent-and-bitangent
#version 420

#define NUM_POINT_LIGHTS 8

struct PointLight
{
	vec4 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constantAttenuation; 
	float linearAttenuation;
	float quadraticAttenuation;

};

struct DirectionalLight
{
	vec4 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct Material
{
	sampler2D diffuse;
	sampler2D specular;
	sampler2D normal;

	vec3 hue;

	float specularExponent;
};

uniform int NUM_ITERATIONS;

uniform PointLight pointLights[NUM_POINT_LIGHTS];

uniform DirectionalLight directionalLight;

uniform Material material;

uniform sampler2D uTex;

uniform int textureToggle;

in vec3 position;
in vec2 texCoord;
in vec3 normal;
in vec3 tangent;
in vec3 biTangent;

out vec4 outColor;

vec3 calculatePointLight(PointLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular);
vec3 calculateDirectionalLight(DirectionalLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular);

void main()
{
	//try camToTangent here
	mat3 camToTangent = mat3(tangent.x, biTangent.x, normal.x,
								tangent.y, biTangent.y, normal.y,
								tangent.z, biTangent.z, normal.z);

	vec3 norm = normalize(normal);
	vec3 normal = normalize((texture(material.normal, texCoord).rgb * 2 - 1) * camToTangent);
	vec4 diffuse = texture(material.diffuse, texCoord) * textureToggle;
	vec4 specular = texture (material.specular, texCoord) * textureToggle;

	outColor.rgb = calculateDirectionalLight(directionalLight, norm, diffuse, specular);

	for (int i = 0; i < NUM_ITERATIONS; i++)
	{
		outColor.rgb += calculatePointLight(pointLights[i], normal, diffuse, specular);
	}

	outColor.rgb *= material.hue;
	outColor.a = diffuse.a;
}

vec3 calculatePointLight(PointLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular)
{
	vec3 lightVec = light.position.xyz - position;
	float dist = length(lightVec);
	vec3 lightDir = lightVec / dist;
	
	//the light contributes to this surface
	float attenuation = 1.0/(light.constantAttenuation + (light.linearAttenuation*dist) + (light.quadraticAttenuation*dist*dist));
	
	vec3 ambient = attenuation * light.ambient;

	//calculate the diffuse contribution
	float NdotL = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = light.diffuse * NdotL * attenuation * materialDiffuse.rgb;

	//Blinn phong half vector
	vec3 reflectDir = reflect(-lightDir, norm);
	float VdotR = max(dot(normalize(-position), reflectDir), 0.0);
	vec3 specular = light.specular * pow(VdotR, material.specularExponent) * attenuation * light.specular * materialSpecular.rgb;

	return ambient + diffuse + specular;
}

vec3 calculateDirectionalLight(DirectionalLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular)
{
	vec3 lightDir = normalize(-light.direction).xyz;

	//the light contributes to this surface
	//directional lights are omnipresent so we can't calculate the distance. Thus there is no attenuation
	
	vec3 ambient = light.ambient;

	//calculate the diffuse contribution
	float NdotL = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = light.diffuse * NdotL * materialDiffuse.rgb;

	//Blinn phong half vector
	vec3 reflectDir = reflect(-lightDir, norm);
	float VdotR = max(dot(normalize(-position), reflectDir), 0.0);
	vec3 specular = light.specular * pow(VdotR, material.specularExponent) * light.specular * materialSpecular.rgb;

	return ambient + diffuse + specular;
}