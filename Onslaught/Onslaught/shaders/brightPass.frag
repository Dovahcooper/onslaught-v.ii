#version 420

layout(binding = 0) uniform sampler2D u_tex;

uniform float u_threshold;

in VertexData
{
    vec3 normal;
    vec3 texCoord;
    vec4 colour;
    vec3 posEye;
} vIn;

layout(location = 0) out vec4 fragColour;

vec4 pixelShader()
{
    vec4 col = texture(u_tex, vIn.texCoord.xy);

    return vec4(clamp(((col - u_threshold) / (1 - u_threshold)), 0, 1));
}

void main()
{
    fragColour = pixelShader();
}