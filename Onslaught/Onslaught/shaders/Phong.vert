#version 420

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uLightMat;

layout(location = 0) in vec3 in_vert;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec3 in_tangent;
layout(location = 4) in vec3 in_biTangent;

out vec3 position;
out vec3 normal;
out vec2 texCoord;
out vec4 shadowPosition;

void main()
{
	texCoord = in_uv;
	//all calculations are done in view space

	normal = mat3(uView) * mat3(uModel) * in_normal;

	vec4 viewSpacePos = uView * uModel * vec4(in_vert,1.0f);
	position = viewSpacePos.xyz; //swizzling to take the xyz
	shadowPosition = uLightMat * uModel * vec4(in_vert, 1.0f);
	gl_Position = uProj * viewSpacePos;
}