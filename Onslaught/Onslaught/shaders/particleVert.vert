#version 420

layout(location = 0) in vec3 in_vert;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec3 in_uv;
layout(location = 3) in float in_colour;

//uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

out vec3 normal;
out vec3 texCoord;
out vec4 colour;

vec3 position;

void main() 
{
	vec4 viewSpacePos = uView * vec4(in_vert,1.0f);
	position = viewSpacePos.xyz; //swizzling to take the xyz
	//gl_Position = uProj * viewSpacePos; //not needed if using geometry shader I believe
	gl_Position = vec4(in_vert, 1.0);
}