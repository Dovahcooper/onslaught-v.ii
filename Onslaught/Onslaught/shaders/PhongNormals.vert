//Tangent reference: http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/#tangent-and-bitangent
#version 420

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

layout(location = 0) in vec3 in_vert;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec3 in_tangent;
layout(location = 4) in vec3 in_biTangent;

out vec3 position;
out vec3 normal;
out vec2 texCoord;
out vec3 tangent;
out vec3 biTangent;

void main()
{

	mat3 camToTangent = mat3(tangent.x, biTangent.x, normal.x,
								tangent.y, biTangent.y, normal.y,
								tangent.z, biTangent.z, normal.z);
	texCoord = in_uv;
	//all calculations are done in view space

	normal = mat3(uView) * mat3(uModel) * in_normal;

	tangent = mat3(uView) * mat3(uModel) * in_tangent;
	biTangent = mat3(uView) * mat3(uModel) * in_biTangent;

	mat3 tanMat = transpose(mat3(tangent, biTangent, normal));
							
	vec3 tangentEye = tanMat * mat3(uView) * in_vert;
	vec4 viewSpacePos = uView * uModel * vec4(in_vert,1.0f);
	position = viewSpacePos.xyz; //swizzling to take the xyz
	gl_Position = uProj * viewSpacePos;
}