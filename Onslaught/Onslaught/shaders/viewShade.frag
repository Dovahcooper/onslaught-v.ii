#version 420

struct Material
{
	sampler2D diffuse;
	sampler2D specular;

	vec3 hue;

	float specularExponent;
};

uniform Material material;

uniform sampler2D uTex;

in vec3 position;
in vec2 texCoord;
in vec3 normal;

out vec4 outColor;

void main()
{
	vec3 norm = normalize(normal);
	vec4 diffuse = texture(material.diffuse, texCoord);
	vec4 specular = texture (material.specular, texCoord);

	outColor.rgb = diffuse.rgb;
	outColor.a = diffuse.a;
}