#version 420

#define NUM_POINT_LIGHTS 8

struct PointLight
{
	vec4 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constantAttenuation; 
	float linearAttenuation;
	float quadraticAttenuation;

};

struct DirectionalLight
{
	vec4 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct Material
{
	sampler2D diffuse;
	sampler2D specular;

	vec3 hue;

	float specularExponent;
};

uniform int NUM_ITERATIONS;

uniform PointLight pointLights[NUM_POINT_LIGHTS];

uniform DirectionalLight directionalLight;

uniform Material material;

layout (binding = 4) uniform sampler2D depthTexture;

uniform int textureToggle;

in vec3 position;
in vec2 texCoord;
in vec3 normal;
in vec4 shadowPosition;

out vec4 outColor;

vec3 calculatePointLight(PointLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular);
vec3 calculateDirectionalLight(DirectionalLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular);

float calculateShadowFrag(vec4 shadowPos)
{
	
	vec3 pC = shadowPos.xyz / shadowPos.w;
	
	//transform to texture coordinates
    pC = pC * 0.5 + 0.5;
     
    //depth component of the shadow map
    float ourDepth = pC.z;

	//initialize our shadow, texelsize and bias
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(depthTexture, 0);
	float bias = 0.035; //for shadow acne

	//perform blur with kernel
    for(int x = -2; x <= 2; ++x)
    {
        for(int y = -2; y <= 2; ++y)
        {
            float checkDepth = texture(depthTexture, pC.xy + vec2(x, y) * texelSize).r; 
            shadow += ourDepth - bias > checkDepth  ? 1.0 : 0.0;        
        }    
    }
	//normalize
    shadow /= 18.0;

	//check if out of frustum range
	if (pC.z > 1.0)
		shadow = 0.0;

    return shadow;
}

void main()
{
	vec3 norm = normalize(normal);
	vec4 diffuse = texture(material.diffuse, texCoord) *textureToggle;
	vec4 specular = texture (material.specular, texCoord)*textureToggle;

	outColor.rgb = calculateDirectionalLight(directionalLight, norm, diffuse, specular);

	for (int i = 0; i < NUM_ITERATIONS; i++)
	{
		outColor.rgb += calculatePointLight(pointLights[i], norm, diffuse, specular);
	}

	outColor.rgb *= material.hue;
	outColor.a = diffuse.a;
}

vec3 calculatePointLight(PointLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular)
{
	vec3 lightVec = light.position.xyz - position;
	float dist = length(lightVec);
	vec3 lightDir = lightVec / dist;
	
	//the light contributes to this surface
	float attenuation = 1.0/(light.constantAttenuation + (light.linearAttenuation*dist) + (light.quadraticAttenuation*dist*dist));
	
	vec3 ambient = attenuation * light.ambient;

	//calculate the diffuse contribution
	float NdotL = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = light.diffuse * NdotL * attenuation * materialDiffuse.rgb;

	//Blinn phong half vector
	vec3 reflectDir = reflect(-lightDir, norm);
	float VdotR = max(dot(normalize(-position), reflectDir), 0.0);
	vec3 specular = light.specular * pow(VdotR, material.specularExponent) * attenuation * light.specular * materialSpecular.rgb;

	return ambient + diffuse + specular;
}

vec3 calculateDirectionalLight(DirectionalLight light, vec3 norm, vec4 materialDiffuse, vec4 materialSpecular)
{
	vec3 lightDir = normalize(-light.direction).xyz;

	//the light contributes to this surface
	//directional lights are omnipresent so we can't calculate the distance. Thus there is no attenuation
	
	vec3 ambient = light.ambient * materialDiffuse.rgb;

	//calculate the diffuse contribution
	float NdotL = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = light.diffuse * NdotL * materialDiffuse.rgb;

	//Blinn phong half vector
	vec3 reflectDir = reflect(-lightDir, norm);
	float VdotR = max(dot(normalize(-position), reflectDir), 0.0);
	vec3 specular = light.specular * pow(VdotR, material.specularExponent) * light.specular * materialSpecular.rgb;

	float shadow = calculateShadowFrag(shadowPosition);  
	return ambient + ((1.0 - shadow) * (diffuse + specular));
}