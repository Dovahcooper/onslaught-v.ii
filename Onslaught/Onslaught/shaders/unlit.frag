#version 420

layout(binding = 0) uniform sampler2D u_tex;

in VertexData
{
	vec3 normal;
	vec3 texCoord;
	vec4 colour;
	vec3 posEye;
} vIn;

layout(location = 0) out vec4 fragColour;

void main()
{
	vec4 colour = texture(u_tex, vIn.texCoord.xy);
	fragColour.rgb = colour.rgb/* + vIn.colour.rgb*/;
	fragColour.a = 1.0;

	//fragColour = vec4(1.0);
}