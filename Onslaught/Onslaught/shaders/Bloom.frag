#version 420

layout(binding = 0) uniform sampler2D u_scene;
layout(binding = 1) uniform sampler2D u_blur;

in VertexData
{
    vec3 normal;
    vec3 texCoord;
    vec4 colour;
    vec3 posEye;
} vIn;

layout(location = 0) out vec4 fragColour;

void main()
{
    vec3 scene = texture(u_scene, vIn.texCoord.xy).rgb;
    vec3 blur = texture(u_blur, vIn.texCoord.xy).rgb;

    fragColour.rgb = mix(scene, blur, 0.27);
    //fragColour.rgb = scene + blur;
    //fragColour.rgb = vec3(1.0, 0.0, 0.0);
    fragColour.a = 1.0;
}