#version 420

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

//uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

in vec3[] normal;
in vec3[] texCoord;
in vec4[] colour;

out vec3 normalG;
out vec3 texCoordG;
out vec4 colourG;

void createQuadParticles(vec4 point, float qSize)
{
	float pointOffset = qSize * 0.5;

	vec4 pointPos = uView * point;

	vec4 v1 = vec4(pointPos.xy + vec2(-pointOffset, pointOffset), pointPos.z, 1.0);
	vec4 v2 = vec4(pointPos.xy + vec2(-pointOffset, -pointOffset), pointPos.z, 1.0);
	vec4 v3 = vec4(pointPos.xy + vec2(pointOffset, pointOffset), pointPos.z, 1.0);
	vec4 v4 = vec4(pointPos.xy + vec2(pointOffset, -pointOffset), pointPos.z, 1.0);

	gl_Position = uProj * v1;
	texCoordG.xy = vec2(0.0, 1.0);
	colourG = colour[0];
	EmitVertex();

	gl_Position = uProj * v2;
	texCoordG.xy = vec2(0.0, 0.0);
	colourG = colour[0];
	EmitVertex();

	gl_Position = uProj * v3;
	texCoordG.xy = vec2(1.0, 1.0);
	colourG = colour[0];
	EmitVertex();

	gl_Position = uProj * v4;
	texCoordG.xy = vec2(1.0, 0.0);
	colourG = colour[0];
	EmitVertex();

	EndPrimitive();
}

void main()
{
	createQuadParticles(gl_in[0].gl_Position, 1.0);
}