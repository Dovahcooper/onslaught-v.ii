#version 420

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

layout(location = 0) in vec3 in_vert;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec3 in_normal;

out vec3 position;
out vec3 normal;
out vec2 texCoord;

void main()
{
	texCoord = in_uv;
	//all calculations are done in view space

	normal = mat3(uView) * mat3(uModel) * in_normal;

	vec4 viewSpacePos = uView * uModel * vec4(in_vert,1.0f);
	position = viewSpacePos.xyz; //swizzling to take the xyz
	gl_Position = uProj * viewSpacePos;
	//normal = (uModel * vec4(in_normal, 0.f)).xyz;
	//gl_Position = uProj * uView * uModel * vec4(in_vert, 1.0f);
}