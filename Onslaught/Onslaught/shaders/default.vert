#version 420

uniform mat4 u_mvp;
uniform mat4 u_mv;
uniform mat4 u_model;
uniform vec4 u_colour;

layout(location = 0) in vec3 in_vert;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_uv;

out VertexData
{
    vec3 normal;
    vec3 texCoord;
    vec4 colour;
    vec3 posEye;
} vOut;

void main()
{
    vOut.texCoord = vec3(in_uv, 1.0);
    vOut.colour = vec4(0.0, 0.0, 0.0, 1.0);
    vOut.normal = (u_mv * vec4(in_normal, 0.0)).xyz;

    vec4 posEyeSpace = (u_mv * u_model * vec4(in_vert, 1.0));

    vOut.posEye = posEyeSpace.xyz;

    gl_Position = u_mvp * vec4(in_vert, 1.0);
}