#version 420

layout(binding = 0) uniform sampler2D u_tex;

uniform float u_horizontalTexel;

in VertexData
{
    vec3 normal;
    vec3 texCoord;
    vec4 colour;
    vec3 posEye;
} vIn;

layout(location = 0) out vec4 fragColour;

vec3 blur();

void main()
{
    fragColour.rgb = blur();
    fragColour.a = 1.0;
}

vec3 blur()
{
    float weights[5];
    vec2 texCoordinates[5];

    weights[0] = weights[4] = 0.102637;
    weights[1] = weights[3] = 0.238998;
    weights[3] = 0.31637;

    texCoordinates[0] = vec2(vIn.texCoord.x + (2 * u_horizontalTexel), vIn.texCoord.y);
    texCoordinates[1] = vec2(vIn.texCoord.x + u_horizontalTexel, vIn.texCoord.y);
    texCoordinates[2] = vec2(vIn.texCoord.x, vIn.texCoord.y); 
    texCoordinates[3] = vec2(vIn.texCoord.x - u_horizontalTexel, vIn.texCoord.y);
    texCoordinates[4] = vec2(vIn.texCoord.x - (2 * u_horizontalTexel), vIn.texCoord.y);

    vec3 blurred = vec3(0.0);

    blurred += texture(u_tex, texCoordinates[0]).rgb * weights[0];
    blurred += texture(u_tex, texCoordinates[1]).rgb * weights[1];
    blurred += texture(u_tex, texCoordinates[2]).rgb * weights[2];
    blurred += texture(u_tex, texCoordinates[3]).rgb * weights[3];
    blurred += texture(u_tex, texCoordinates[4]).rgb * weights[4];

    return blurred / ((weights[0] * 2) + (weights[1] * 2) + weights[3]);
}