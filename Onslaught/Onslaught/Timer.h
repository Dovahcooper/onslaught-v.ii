
#pragma once

class Timer
{
	//keeps track of elapsed time to use for things like physics
public:
	Timer();
	~Timer();


	float tick();
	float getElapsedTimeMS();
	float getElapsedTimeSeconds();
	float getCurrentTime();

private:
	float currentTime, previousTime, elapsedTime;

};