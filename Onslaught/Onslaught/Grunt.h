#pragma once
#include "Enemy.h"

class Grunt : public Enemy
{
public:
	Grunt(Mesh *temp);
	~Grunt();

	//AABB gruntBB;
	//void enemyAI(Player *thePlayer);

	/* Computer Seperation
	Needs the Enemy wave, and the index (i)

	Checks to see if enemy is too close to other enemies
	Then seperates them
	Returns the force that needs to be applied to the acceleration
	*/
	glm::vec3 computeSeperation(std::vector <Enemy*> enemy_units, int index);

	/* Enemy Arrive + Seek
	Both needs to be combines

	Needs the target (the player)

	Computes path to player
	Follows that path
	modifies the velocity depending on how close it is
	So that it can stop moving
	Returns the force that needs to be applied to the acceleration
	*/
	glm::vec3 enemyArrive();

	void enemyAttack();

	//getters and setters for velocity, acceleration, and max force

	void collision(Player *thePlayer);

	void initialize(Mesh* newMesh, Material* newMaterial, int waveStates = 0, int spawnPoint = 0, bool champion = false);
	void update(float deltaTime, int &numAlive, PowerUp *powerUp);
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

	glm::vec3 gruntPosition[8];
	/*	glm::vec3 velocity;
	glm::vec3 acceleration;
	float maxForce;
	bool Alive;
	bool attacking = false;

	Material *enemyTexture;
	Material *damageTexture;*/

private:
};
