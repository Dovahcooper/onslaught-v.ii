#pragma once
#include "Enemy.h"

class Sprinkler : public Enemy
{
public:
	Sprinkler(Mesh *temp);
	~Sprinkler();

	glm::vec3 computeSeperation(std::vector <Enemy*> theEnemies, int index);

	glm::vec3 enemyArrive();

	void enemyAttack();

	//getters and setters for velocity, acceleration, and max force

	void collision(Player *thePlayer);

	void initialize(Mesh* newMesh, Material* newMaterial, int spawnPoint, bool champion = false);
	void update(float deltaTime, int &numAlive, PowerUp *powerUp);
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

private:
	float secondTimer;
	float reviveTimer;
};

