#pragma once
#include <glm/glm.hpp>

class Light //just a storage class for all of the stuff
{

public:
	Light();
	~Light();

	glm::vec4 positionOrDirection;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	float specularExponent;

	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;

private:

};