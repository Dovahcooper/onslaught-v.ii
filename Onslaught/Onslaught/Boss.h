#pragma once
#include "Sprinkler.h"
#include "Grunt.h"
#include "Avengers.h"

class Boss : public Enemy
{
public:
	Boss(Mesh *temp, Mesh *weaponBody, Material *mat, ShaderProgram *phong);
	~Boss();

	glm::vec3 enemyArrive();

	void calculateTarget(Player *player1, Player *player2);

	void enemyAttack();

	void avengerUpdate(float deltaTime, std::vector<Enemy*> theEnemies, int &numAlive, int i);

	//getters and setters for velocity, acceleration, and max force

	void collision(Player *thePlayer, GameObject *theHealthBar);

	void initialize(Mesh* newMesh, Material* newMaterial, int type);
	void update(float deltaTime, int &numAlive, PowerUp *powerUp);
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

	std::vector<Enemy*> bossWeapons;
	std::vector<Grunt*> regularWeapons;
	std::vector<Avengers*> avengerWeapons;
	std::vector<Sprinkler*> sprinklerWeapons;
	GameObject boss_weakPoint;
	bool activated;
	int _type;
	float bossRotTimer;

private:
	float s_spawn1, s_spawn2; //the timers for the sprinklers
};
