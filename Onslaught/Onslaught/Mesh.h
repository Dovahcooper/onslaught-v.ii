#pragma once
#include <string>
#include <GL\glew.h>
#include <vector>
#include "VertexBufferObject.h"
#include "glm/glm.hpp"

class Mesh
{
public:
	Mesh();
	~Mesh();

	//load a mesh and send it to OpenGL
	bool loadFromFile(const std::string &file);

	//Release data from opengl VRAM
	void unload();

	void draw();

	unsigned int getNumFaces();
	unsigned int getNumVertices();

	void createVBO();

	//openGL buffers and objects
	GLuint vboVertices = 0;
	GLuint vboUVs = 0;
	GLuint vboNormals = 0;
	GLuint vao = 0;
	GLuint vboTangents = 0;
	GLuint vboBiTangent = 0;

	VertexBufferObject vbo;

	std::vector<glm::vec3> vertexData;
	std::vector<glm::vec2> textureData;
	std::vector<glm::vec3> normalData;

	///openGL ready data
	std::vector<float> unpackedVertexData;
	std::vector<float> unpackedTextureData;
	std::vector<float> unpackedNormalData;
	std::vector<float> unpackedTangentData;
	std::vector<float> unpackedBiTangentData;

	unsigned int numFaces = 0;
	unsigned int numVertices = 0;

private:
	//tangent data
	glm::vec3 vert1, vert2, vert3;
	glm::vec2 tex1, tex2, tex3;
	glm::vec3 edge1, edge2;
	glm::vec2 subtex1, subtex2;
	glm::vec3 tangent, biTangent;

};