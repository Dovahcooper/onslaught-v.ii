#pragma once

#include <GL/glew.h>
#include <string>

//for different texture types

enum TextureType
{
	Diffuse,
	Specular,
	Normal
};

class Texture
{
public:
	Texture();
	~Texture();

	bool load(const std::string &file, bool _invert = false);
	void unload();

	void bind();
	void bind(GLenum active_tex);
	void unbind();
	void unbind(GLenum active_tex);

	//the handle to the tex object
	GLuint texObj = 0;
private:
};