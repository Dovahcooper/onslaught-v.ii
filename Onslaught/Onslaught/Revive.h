#pragma once
#include "Enemy.h"

class Revive : public Enemy
{
public:
	Revive(Mesh *temp);
	~Revive();

	glm::vec3 computeSeperation(std::vector <Enemy*> enemy_units, int index);

	glm::vec3 enemyArrive();

	void enemyAttack();

	bool collision(Player *thePlayer, Player *deadPlayer, int &numAlive, int &index, int max);

	void initialize(Mesh* newMesh, Material* newMaterial, glm::vec3 newPosition, bool champion = false);
	void update(float deltaTime, int &numAlive);
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

private:
};