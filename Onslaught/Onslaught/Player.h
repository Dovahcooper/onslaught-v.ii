#pragma once

#include <string>
#include <vector>
#include "GameObject.h"
#include "controller.h"
#include "Bullet.h"

enum GameStates;

enum class CustomizationType {
	Basic = 0,
	Fast = 1,
	Heavy = 2 //for the bodies, and for the turrets
};

class Player : public GameObject
{
public:
	Player(int &_index, Material *texture, Material *body);
	~Player();

	void update(float deltaTime, Input::XBoxInput &controller, GameStates &gameStates);
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);
	void menuUpdate(float deltaTime);

	void createBullets(float &_x, float &_y);
	void deleteBullets(int index);
	void updateBullets(float deltaTime);

	void setPosition(Input::Stick &lStik, float &dt);
	void setRotation(float &_x, float &_y);

	void damageTaken(float temp);
	void setDamage(Material* temp);

	void loadMeshes(Mesh* body, Mesh*turret, CustomizationType _body, CustomizationType _turret, ShaderProgram * shader, Material *matBody, Material *matTurret);
	void reset(glm::vec3 resetPosition);

	bool getAlive();
	bool hit;
	bool firing;

	bool isMoving;

	void setHealth(float temp);
	void setAlive(bool temp);
	float getHealth();

	std::vector<Bullet*> playerBullets;

	glm::vec3 velocity;
	float acceleration;
	float maxVelocity;
	float bulletVelocity;
	float bulletDamage;

	float test;
	float lastAngle;

	Material *damage;
	Material *normal;
	int score;
	int scoreMultiplayer;
	int maxHealth;
	float scoreTimer;
	float dTimer; //for the healthbars changing colour // may move this later
	GameObject *playerTurret;
	int invincibilityFrames;

private:
	int index;
	Input::Stick rStick;
	Input::Stick lStick;
	glm::vec3 rotation;

	glm::vec3 splinePos[9];

	float time;
	float catmullTime;

	int currentFrame;
	int nextFrame;
	int bulletScale;

	float health;
	float fireRate;
	bool Alive;
	Mesh* bulletMesh;
	Material *bulletMat;

	bool yPressed;
};