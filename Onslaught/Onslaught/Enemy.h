#pragma once
#include "GameObject.h"
#include "Player.h"
#include <vector>
#include "Bullet.h"
#include "PowerUp.h"

enum EnemyType
{
	_Grunt = 0,
	_Avengers,
	_Sprinklers,
	_Boss,
	_Revive
};

class Enemy : public GameObject
{
public:
	Enemy(Mesh *temp);
	~Enemy();

	void calculateTarget(Player *player1, Player *player2);

	virtual glm::vec3 computeSeperation(std::vector <Enemy*> enemies, int index);

	virtual glm::vec3 enemyArrive();

	virtual void enemyAttack();

	virtual void collision(Player *thePlayer);

	virtual void tutorialCollision(Player *thePlayer);

	virtual void changeAppearange(Mesh* newMesh, Material* newMaterial);

	void deleteAllBullets();

	virtual void initialize(Mesh* newMesh, Material* newMaterial, int spawnPoint, bool champion);
	virtual void update(float deltaTime, int &numAlive, PowerUp* powerUp, bool FKoverride = false);
	virtual void updateBullets(float deltaTime);
	virtual void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection, std::vector<Light> &pointLights, Light &directionalLight);

	virtual void checkSpawn(Mesh* newMesh, Material* newMaterial, bool champion = false);

	void createBullets(float &_x, float &_y);
	void deleteBullets(int index);
	void setBulletTexture(Material *bullet);
	void setDamageTexture(Material *mat);

	void setHealth(float temp);

	float getTimer();
	void setTimer(float temp);
	float getHealth();
	int getBulletSize();

	glm::vec3 velocity;
	glm::vec3 acceleration;
	float maxForce;
	bool Alive;
	bool Spawn;
	bool attacking;

	Material *enemyTexture;
	Material *damageTexture;
	Material *bulletTexture;

	EnemyType _enemyType;
	float SpawnIndex;
	//	float SpawnIndex2;
	int gridIndex;
	float spawnTimer;
	bool tutorial;
	bool blnTutorialCollision;
	glm::vec3 spawnPosition[8];
	bool firing;
	float areaRadius;
	float enemyRadius;
	float desiredSeperation;

protected:
	float max_seek;
	float max_velocity;
	glm::vec3 steering;
	float mass;
	glm::vec3 seperationVector;
	glm::vec3 target;
	glm::vec3 directionFaced;
	glm::vec3 playerPos;
	int shotCounter;

	std::vector<Bullet *> enemyBullet;

	//what we need
	Mesh* bulletMesh;
	float timer;
	float health;
	//EnemyBullets *enemyBullet;
	float dTimer;
};