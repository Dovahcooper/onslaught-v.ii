#pragma once
//Brent Cowan Jan. 22, 2018

//This pre-processor definition add the lib file in code instead of using the project settings.
//This makes adding your code to other projects easier, but it may not work on other operating
//systems or compilers.
//#pragma comment (lib, "../lib/Win32/Debug/fmod_vc.lib")

#include <Windows.h>
#include <math.h>
#include "FMOD/FMOD/fmod.hpp"
#include "FMOD/FMOD/fmod_errors.h"
#include <glm/vec3.hpp>
#include <iostream> //cin & cout

using namespace std;

void FmodErrorCheck(FMOD_RESULT result);

struct Listener
{
	FMOD_VECTOR pos = { 0.0f, 0.0f, -1.0f };
	FMOD_VECTOR forward = { 0.0f, 0.0f, 1.0f };
	FMOD_VECTOR up = { 0.0f, 1.0f, 0.0f };
	FMOD_VECTOR vel = { 0.0f, 0.0f, 0.0f };
};

class SoundEngine
{
private:
	bool init;
	FMOD_RESULT result;

public:
	SoundEngine();
	~SoundEngine();
	bool Init();
	void CleanUp();

	void SoundEngine::update();

	//sets listener position vector using vec3
	void setListenerPosition(glm::vec3 listener_pos);
	//sets listener forwards vector using vec3
	void setListenerForwards(glm::vec3 listener_forward);
	//sets listener up vector using vec3
	void setListenerUp(glm::vec3 listener_up);
	//sets listener Velocity vector using a vec3
	void setListenerVelocity(glm::vec3 listener_vel);

	FMOD::System    *system;
	unsigned int     version;
	void            *extradriverdata = 0;
	Listener         listener;
};