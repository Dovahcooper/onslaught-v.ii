#include "Avengers.h"

Avengers::Avengers(Mesh * temp)
	:Enemy(temp)
{
	velocity = glm::vec3(0, 0, 0);
	maxForce = 3;
	max_velocity = 20.0f;
	max_seek = 5.0f;
	mass = 1;
	seperationVector;
	target = glm::vec3(0, 0, 0);
	directionFaced = glm::vec3(0, 0, 0);
	timer = 0;
	health = 50;
	secondTimer = 0;
	explosion = false;
	_enemyType = _Avengers;
	explSound = false;
	areaRadius = 1.5;
	enemyRadius = 1.5;
	desiredSeperation = enemyRadius;
}

Avengers::~Avengers()
{
}

glm::vec3 Avengers::computeSeperation(std::vector <Enemy*> theEnemies, int index)
{
	int total_grunts = theEnemies.size(); //we need to iterate through all the grunts
	int neighbourCount = 0; //we use this to 

	for (int i = index; i < total_grunts; i++)
	{
		if (i != index&& gridIndex == theEnemies[i]->gridIndex) //making sure we're not seperating from itself
		{
			float lineBetween = sqrt(pow(getPosition().y - theEnemies[i]->getPosition().y, 2) + pow(getPosition().x - theEnemies[i]->getPosition().x, 2)); //THIS IS DISTANCE BETWEEN

			if (lineBetween < (desiredSeperation + theEnemies[i]->enemyRadius)) //If another object falls within this object's range
			{
				//printf("%f", desiredSeperation);
				glm::vec3 difference = (glm::vec3(theEnemies[i]->getPosition().x - getPosition().x, theEnemies[i]->getPosition().y - getPosition().y, 0)); //we get the difference
																																						   //difference = glm::normalize(difference); //normalize it to get direction //idontknowifweneedthis?????
																																						   //difference /= lineBetween; //divide it--the average
				seperationVector += difference; //add it to the force
				neighbourCount++; //increment the number of 'neighbours'
			}
		}
	}

	if (neighbourCount == 0)
	{
		return glm::vec3(0); //if there are no neighbours, nothing happens
	}

	seperationVector /= neighbourCount; //why are we doing this
	seperationVector *= -1;

	steering = seperationVector - velocity; // to update it...?

	if (glm::length(steering) > max_velocity) //this is clamping the value
		steering = glm::normalize(steering) * max_velocity;

	steering = steering / mass; //the mass affects how much it moves

	return steering; //we return the force
}

glm::vec3 Avengers::enemyArrive()
{
	float distance = glm::length(target); //the distance is for arrive
	target = glm::normalize(target); //normalizing leads to the direction

	target *= max_seek;

	steering = target - velocity;

	//the mass affects how it moves
	steering = steering / mass;

	return steering;
}

void Avengers::enemyAttack()
{
	target = glm::normalize(target);

	if (timer > 2 && explosion == false)
	{
		attacking = true;
	}

	if (attacking == true)
	{
		secondTimer += 0.5;
		if (fmod(secondTimer, 2) == 0)
		{
			createBullets(target.x, target.y);
			firing = true;
		}

		if (secondTimer > 6)
		{
			attacking = false;
			timer = 0;
			secondTimer = 0;
		}
	}
}

void Avengers::collision(Player * thePlayer)
{
	Enemy::collision(thePlayer);

	float distance = glm::length(thePlayer->getPosition());
	float playerDistance = glm::distance(getPosition(), thePlayer->getPosition());

	if (Alive == true)
	{
		for (int i = 0; i < thePlayer->playerBullets.size(); i++)
		{
			float distance = glm::distance(getPosition(), thePlayer->playerBullets[i]->getPosition());

			if (distance < 1.3f)
			{
				thePlayer->deleteBullets(i);

				if (tutorial == true && blnTutorialCollision == false)
				{
					blnTutorialCollision = true;
				}
				else
				{
					setMaterial(damageTexture);
					dTimer = 0.4;
					health -= thePlayer->bulletDamage;

					if (health <= 0 && tutorial == false)
						thePlayer->score += (10 * thePlayer->scoreMultiplayer);
				}
			}
		}

		if (explosion == true && playerDistance < 3)
		{
			if (tutorial == false && thePlayer->getAlive() == true)
			{
				thePlayer->damageTaken(10);
				thePlayer->hit = true;
			}
		}
	}

}

void Avengers::initialize(Mesh* newMesh, Material* newMaterial, int spawnPoint, bool champion)
{
	Enemy::initialize(newMesh, newMaterial, spawnPoint, champion);
	//	setMaterial(enemyTexture);

	velocity = glm::vec3(0, 0, 0);
	maxForce = 2;
	max_velocity = 30.0f;
	max_seek = 10.0f;
	mass = 1;
	seperationVector;
	areaRadius = 9;
	target = glm::vec3(0, 0, 0);
	health = 50;

	Alive = false;
	Spawn = true;
	explosion = false;
}

void Avengers::update(float deltaTime, int & numAlive)
{
	Enemy::update(deltaTime, numAlive, NULL);

	//the avenger steadily loses health and explodes when the health is at 0
	//if the player is caught within the explosion's range they die

	if (Alive == true)
	{
		health -= 3 * deltaTime;

		if (health <= 0 && explosion == false)
		{
			explosion = true;
			timer = 0;
			secondTimer = 0;
			explSound = true;
		}
	}

	if (explosion == true && Alive == true)
	{
		timer += deltaTime;

		if (timer >= 2)
		{
			Alive = false;
			if (tutorial == false)
				numAlive--;
			Spawn = false;
			explosion = false; // this may cause problems
			tutorial = false;
		}
	}
}

void Avengers::draw(glm::mat4 & cameraTransform, glm::mat4 & cameraProjection, std::vector<Light>& pointLights, Light & directionalLight)
{
	Enemy::draw(cameraTransform, cameraProjection, pointLights, directionalLight);
}

void Avengers::explode(std::vector<Enemy*> theEnemies, int &numAlive)
{
	for (int i = 0; i < theEnemies.size(); i++)
	{
		if (gridIndex == theEnemies[i]->gridIndex && theEnemies[i]->Alive == true)
		{
			if (theEnemies[i]->_enemyType != _Boss && theEnemies[i]->_enemyType != _Revive)
			{
				theEnemies[i]->Alive = false;
				numAlive--;
			}
			else if (theEnemies[i]->_enemyType != _Revive)
			{
				theEnemies[i]->setHealth(theEnemies[i]->getHealth() - 10);
			}
		}
	}
}
