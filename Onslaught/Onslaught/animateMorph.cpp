#include "animateMorph.h"

#define BUFFER_OFFSET(i) ((char*)0 + (i))

animateMorph::animateMorph()
{

}

animateMorph::~animateMorph()
{

}

void animateMorph::loadMorphTarget()
{
	for (int i = 0; i < morphMesh.size(); i++)
	{

		if (i == morphMesh.size() - 1)
		{
			glBindVertexArray(morphMesh[i]->vao);

			glEnableVertexAttribArray(3);  //morph Vertices
			glEnableVertexAttribArray(4);  //morph Normals

			glBindBuffer(GL_ARRAY_BUFFER, morphMesh[0]->vboVertices);
			glVertexAttribPointer((GLuint)3, 3, GL_FLOAT, GL_FALSE,
				sizeof(float) * 3, BUFFER_OFFSET(0));

			glBindBuffer(GL_ARRAY_BUFFER, morphMesh[0]->vboNormals);
			glVertexAttribPointer((GLuint)4, 3, GL_FLOAT, GL_FALSE,
				sizeof(float) * 3, BUFFER_OFFSET(0));
		}
		else
		{
			glBindVertexArray(morphMesh[i]->vao);

			glEnableVertexAttribArray(3);  //morph Vertices
			glEnableVertexAttribArray(4);  //morph Normals

			glBindBuffer(GL_ARRAY_BUFFER, morphMesh[i + 1]->vboVertices);
			glVertexAttribPointer((GLuint)3, 3, GL_FLOAT, GL_FALSE,
				sizeof(float) * 3, BUFFER_OFFSET(0));

			glBindBuffer(GL_ARRAY_BUFFER, morphMesh[i + 1]->vboNormals);
			glVertexAttribPointer((GLuint)4, 3, GL_FLOAT, GL_FALSE,
				sizeof(float) * 3, BUFFER_OFFSET(0));
		}
	}
	
}