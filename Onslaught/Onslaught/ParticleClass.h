#pragma once

#include "ShaderProgram.h"
#include "glm/vec3.hpp"
#include <memory>
#include "Material.h"
#include "Texture.h"
class ParticleClass
{
	//particle structure
	struct Particle
	{
		glm::vec3* positions;
		glm::vec3* velocities;
		glm::vec3* accelerations;
		float*	remainingLives;
		float* masses;
	} particles;

public:
	ParticleClass();
	~ParticleClass();

	// Initialize memory for particles
	void initialize(unsigned int numParticles, ShaderProgram *newShader, Texture *newTexture);

	// playback control
	inline void play() { playing = true; }
	inline void pause() { playing = false; }

	// Updates each particle in system
	void update(float dt);
	void updateSpawn(float dt);

	void setMaterial(std::string diffuse);

	// Draws particles to screen
	void draw(glm::mat4 &cameraTransform, glm::mat4 &cameraProjection);

	void freeMemory();

	// Emitter position in world space
	// aka where the particles spawn
	glm::vec3 initialPosition;

	// locomotion properties
	// Ranges store the min (x) and max(y) bound of random values for the property
	glm::vec3 lifeRange;
	glm::vec3 initialForceMin;
	glm::vec3 initialForceMax;

	GLuint vao = 0;
	GLuint vbo = 0;
	ShaderProgram *shader;

	float timeKeep;
	glm::vec3 partColour;
	bool playing;	// false is update is paused
	bool reset = false; //resetting particles
	bool doExplode = false;

	Material* material;
	Texture* partTexture;

private:
	//glm::vec3 localPosition;
	//glm::mat4 localMatrix = glm::mat4(1.0);	//not needed, but keeping here in case
	unsigned int numParticles; // Number of particles passed into initialize()
	bool allocated; // false if memory not allocated
	int angle = 0;
};
